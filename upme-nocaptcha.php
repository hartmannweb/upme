<?php

if( !defined( 'ABSPATH' ) ) exit;


UPME_NoCaptcha();
   
    class UPME_NoCaptcha{
    
        private static $instance;
        public $upnr_options;

        public static function instance() {
            
            if ( ! isset( self::$instance ) && ! ( self::$instance instanceof UPME_NoCaptcha ) ) {
                self::$instance = new UPME_NoCaptcha();
                self::$instance->init();
                
                

                add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
                
            }
            return self::$instance;
        }

        public function init() {
            add_filter('upme_captcha_plugins_list',array($this,'upnr_captcha_plugins_list'),10,2);
            add_filter('upme_captcha_load_nocaptcharecaptcha',array($this,'load_nocaptcharecaptcha'),10,2);
            add_filter('upme_captcha_check_nocaptcharecaptcha',array($this,'validate_nocaptcharecaptcha'),10,2);
            
        }
        
        public function upnr_captcha_plugins_list($captcha_plugins,$captcha_plugins_params){
            $captcha_plugins['nocaptcharecaptcha'] = __('No Captcha reCaptcha', 'upnr');
            return $captcha_plugins;
        }
        
        public function load_nocaptcharecaptcha($captcha_html,$captcha_html_params){
            global $upme,$upme_captcha_loader;

            // Getting the Public Key to load reCaptcha
            $public_key = '';
            $public_key = $upme->get_option('recaptcha_public_key');

            if($public_key != '')
            {
                $captcha_code = '';

                // Loading the theme configured in admin.
                //$recaptcha_theme = $upme->get_option('recaptcha_theme');
                $recaptcha_theme = 'upme';

                if($recaptcha_theme == 'upme')
                {
                    $theme_code = "<script type=\"text/javascript\"> var RecaptchaOptions = {    theme : 'custom',lang: 'en',    custom_theme_widget: 'recaptcha_widget' };</script>";
                    $captcha_code = $this->load_custom_nocaptcharecaptcha($public_key);
                }
                else
                {
                    $theme_code = "<script type=\"text/javascript\">var RecaptchaOptions = {theme : '".$recaptcha_theme."', lang:'en'};</script>";
                    if(is_ssl()){
                        $captcha_code = recaptcha_get_html($public_key, null, true);
                    }else{
                        $captcha_code = recaptcha_get_html($public_key, null);
                    }

                }

                return $theme_code.$captcha_code;
            }
            else
            {
                // No public key is not set in admin. So loading no captcha HTML. 
                return $upme_captcha_loader->load_no_captcha_html();
            }
        }
        
        public function load_custom_nocaptcharecaptcha($public_key='')
        {
            wp_register_script('upnr_nocaptcha_script', 'https://www.google.com/recaptcha/api.js' );
            wp_enqueue_script('upnr_nocaptcha_script');

            return '<div class="g-recaptcha" data-sitekey="' . $public_key . '"></div>';

        }
        
        public function load_nocaptcha_recaptcha_class(){
            require_once UPNR_PLUGIN_DIR . 'ReCaptcha/Response.php';
            require_once UPNR_PLUGIN_DIR . 'ReCaptcha/RequestParameters.php';
            require_once UPNR_PLUGIN_DIR . 'ReCaptcha/RequestMethod.php';
            require_once UPNR_PLUGIN_DIR . 'ReCaptcha/RequestMethod/Post.php';
            require_once UPNR_PLUGIN_DIR . 'ReCaptcha/RequestMethod/Socket.php';
            require_once UPNR_PLUGIN_DIR . 'ReCaptcha/RequestMethod/SocketPost.php';
            require_once UPNR_PLUGIN_DIR . 'ReCaptcha/ReCaptcha.php';
        }
        
        public function validate_nocaptcharecaptcha($status,$captcha_check_params)
        {
            global $upme;
            $this->load_nocaptcha_recaptcha_class();

            // Getting the Private Key to validate reCaptcha
            $private_key = '';
            $private_key = $upme->get_option('recaptcha_private_key');


            if($private_key != '')
            {
                if (upme_is_in_post('g-recaptcha-response'))
                {
                    $recaptcha = new \ReCaptcha\ReCaptcha($private_key);
                    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
                    if ($resp->isSuccess())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }    
            }
            else
            {
                // Private key is not set in admin
                return true;
            }
        }

        public function load_textdomain() {
            
        }
        
    }

// Plugin version
if ( ! defined( 'UPNR_VERSION' ) ) {
    define( 'UPNR_VERSION', '1.0' );
}

// Plugin Folder Path
if ( ! defined( 'UPNR_PLUGIN_DIR' ) ) {
    define( 'UPNR_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Plugin Folder URL
if ( ! defined( 'UPNR_PLUGIN_URL' ) ) {
    define( 'UPNR_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

function UPME_NoCaptcha() {
    global $upnr;
	$upnr = UPME_NoCaptcha::instance();
}