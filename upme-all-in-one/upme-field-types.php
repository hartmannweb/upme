<?php

/**
 * Include the files required for custom field types
 */
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-file-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-wp-editor-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-text-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-embedly-card-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-date-field.php';

require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-google-maps-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-list-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-chosen-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-user-badge-field.php';
require_once UAIO_PLUGIN_DIR.'classes/fields/class-uaio-mailchimp-field.php';

require_once UAIO_PLUGIN_DIR.'classes/class-uaio-field-type-manager.php';
require_once UAIO_PLUGIN_DIR.'classes/class-uaio-file-extension-manager.php';
require_once UAIO_PLUGIN_DIR.'classes/class-uaio-file-manager.php';


