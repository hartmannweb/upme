function uaio_initialize_google_map_field() {

     var options = {
      types: ['(cities)'],

     };
 
    jQuery('.uaio-google-map').each(function(){
      var input1 = document.getElementById(jQuery(this).attr('id'));
 	  var autocomplete = new google.maps.places.Autocomplete(input1, options);
    });
    
}

jQuery(document).ready(function(){
    uaio_initialize_google_map_field();
    //uaio_initialize_google_map();
    
    if(uaioCommon.maps.length != 0){
        
        for (var key in uaioCommon.maps) {
            if(uaioCommon.maps[key] != ''){
                google.maps.event.addDomListener(window, 'load', uaio_initialize_google_map(key,uaioCommon.maps[key]));
            }
            
        }
        
    }
});
//google.maps.event.addDomListener(window, 'load', uaio_initialize_google_map_field);



function uaio_initialize_google_map(map_id,address) {
  
            console.log(address);
            var myOptions = {
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
            };
    
            var address_sigle = address;
            var map = new google.maps.Map(document.getElementById(map_id), myOptions);
 
            google.maps.event.addListener(map, 'zoom_changed', function() {
            zoomChangeBoundsListener = 
                google.maps.event.addListener(map, 'bounds_changed', function(event) {
                    if (this.getZoom() > 8 && this.initialZoom == true) {
                        // Change max/min zoom here
                        this.setZoom(8);
                        this.initialZoom = false;
                    }
                    google.maps.event.removeListener(zoomChangeBoundsListener);
                });
            });
        
    
    
            var geocoder = new google.maps.Geocoder();

            var markerBounds = new google.maps.LatLngBounds();
            
            geocoder.geocode( { 'address': address_sigle}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                    markerBounds.extend(results[0].geometry.location);
                    map.initialZoom = true;
                    map.fitBounds(markerBounds);
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
}

function uaio_geocoder(address){
	geocoder_map = new google.maps.Geocoder(); 
        
        geocoder_map.geocode( { 'address': address}, function(results, status) {
            
            if (status == google.maps.GeocoderStatus.OK) {
                marker_new = new Array();
                marker_new[0] = results[0].formatted_address;
                marker_new[1] = results[0].geometry.location.k;
                marker_new[2] = results[0].geometry.location.B;

                return marker_new;

            }
        });

}