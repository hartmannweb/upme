jQuery(document).ready(function($) {

    /**
     * Enable datepickers on UPME date fields
     *
     */
	if (jQuery.fn.datepicker) {
	    $( ".uaio_past_date" ).datepicker({ minDate: "-50Y", maxDate: 0 });
		$( ".uaio_future_date" ).datepicker({ minDate: 0, maxDate: "+50Y" });
	}

    /**
     * Reset values of date fields
     */
    jQuery(".uaio_past_date_reset").click(function(){
        var picker = jQuery(this).parent().find('.uaio_past_date');
        jQuery.datepicker._clearDate(picker);
    });

    jQuery(".uaio_future_date_reset").click(function(){
        var picker = jQuery(this).parent().find('.uaio_future_date');
        jQuery.datepicker._clearDate(picker);
    });

    
    jQuery('form').submit(function(e){
        // e.preventDefault();
        jQuery('.upme-chosen-multiple').each(function(){      
            if(jQuery(this).val() == null){ 

               jQuery(this).val(jQuery(this).find("option:first").val());
            }
        });
        //e.preventDefault();
    });
    
    jQuery("#uaio-registration-field-btn").click(function(){
        jQuery('#uaio-registration-field-err').hide();
        if(jQuery('#uaio-registration-fields').val() == '0'){
            jQuery('#uaio-registration-field-err').html(uaioAdmin.regFieldEmpty).show();
        }else{
            //$('select option:selected').text();
            var err = 0;
            jQuery('#uaio-reg-fields-list li').each(function(){
                if(jQuery(this).attr('data-meta') == jQuery('#uaio-registration-fields').val()){
                    err = 1;
                    jQuery('#uaio-registration-field-err').html(uaioAdmin.fieldAdded).show();
                }
            });
            
            if(err == 0){
                var meta = '';
                
                jQuery('#uaio-reg-fields-list').append('<li data-meta="' + jQuery('#uaio-registration-fields').val() + '">' + jQuery('#uaio-registration-fields option:selected').text() + '<span class="uaio-reg-field-delete"><i class="upme-icon upme-icon-remove"></i></span></li>');
                jQuery('#uaio-reg-fields-list li').each(function(){
                    meta += jQuery(this).attr('data-meta') + ',';                          
                 });
     
                jQuery('#uaio-registration-fields-hidden').val(meta);
            }
            
        }
        
    });
    
    jQuery('#uaio-reg-fields-list').on('click','.uaio-reg-field-delete',function(){
        jQuery(this).parent().remove();
        
        var meta = '';
        jQuery('#uaio-reg-fields-list li').each(function(){
            meta += jQuery(this).attr('data-meta') + ',';                          
         });

         jQuery('#uaio-registration-fields-hidden').val(meta);
        
    });
    
    jQuery('#uaio-profile-view-type').change(function(){
        if(jQuery(this).val() == 'fields'){
            jQuery('#uaio-profile-fields').parent().parent().show();
        }else{
            jQuery('#uaio-profile-fields').parent().parent().hide();
        }
        
    });
    
    jQuery('#uaio-profile-grouping-type').change(function(){
        if(jQuery(this).val() == 'users'){
            jQuery('#uaio-listing-users').parent().parent().show();
        }else{
            jQuery('#uaio-listing-users').parent().parent().hide();
        }
        
    });
    
    jQuery('body').on('click','.uaio_mailchimp_btn',function(){
        var user_id = jQuery(this).attr('data-user');
        var meta = jQuery(this).attr('data-meta');
        var subscribe_btn = jQuery(this);
        
        subscribe_btn.val(uaioAdmin.Messages.Processing);
        
        jQuery.post(
                uaioAdmin.AdminAjax,
                {
                    'action': 'uaio_process_mailchimp_subscription',
                    'user_id':   user_id,
                    'meta':   meta,
                },
                function(response){
                     if(response.status == 'success'){
                        if(response.type == 'subscribe'){
                            subscribe_btn.val(uaioAdmin.Messages.ClickToUnsubscribe);
                        }else{
                            subscribe_btn.val(uaioAdmin.Messages.ClickToSubscribe);
                        }
                        
                    }else{
                        if(response.type == 'subscribe'){
                            subscribe_btn.val(uaioAdmin.Messages.ClickToSubscribe);
                        }else{
                            subscribe_btn.val(uaioAdmin.Messages.ClickToUnsubscribe);
                        }
                    }
                }
                    

                ,"json");
    });
    
});