<?php 
    global $uaio_settings_data; 
    extract($uaio_settings_data);
 
?>

<form method="post" action="">
<table class="form-table uaio-settings-list">
                <tr>
                    <th><label for=""><?php echo __('Google Maps API Key','uaio'); ?></label></th>
                    <td style="width:500px;">
                        <input type="text" name="uaio_general[google_map_api_key]"  value="<?php echo $google_map_api_key; ?>" /><br/>
                        <div class='uaio-settings-help'><?php _e('This setting is used get your account API Key to access google map.','uaio'); ?></div>
                    </td>
                    
                </tr>
                        
                
    <input type="hidden" name="uaio_general[private_mod]"  value="1" />                        
    <input type="hidden" name="uaio_tab" value="<?php echo $tab; ?>" />    
</table>

    <?php submit_button(); ?>
</form>