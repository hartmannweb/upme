<?php 
    global $upme_admin,$registration_forms_data;
    extract($registration_forms_data);
?>
    <div class="uaio-settings-form ">
        <table class="form-table">
              
            <tr>
                <td colspan="2"><div id="uaio-registration-field-err" style="display:none"></div></td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Registration Form Before Head ','uaio'); ?></label></th>
                <td>
                    <?php wp_editor( $reg_content_before_head, 'uaio-reg-form-before-head');  ?>
                    <i original-title="<?php _e('Add custom content before the head section of registration form.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Registration Form Head ','uaio'); ?></label></th>
                <td>
                    <?php wp_editor( $reg_content_head, 'uaio-reg-form-head');  ?>
                    <i original-title="<?php _e('Add custom content to the head section of registration form.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Registration Form After Head ','uaio'); ?></label></th>
                <td>
                    <?php wp_editor( $reg_content_after_head, 'uaio-reg-form-after-head');  ?>
                    <i original-title="<?php _e('Add custom content after the head section of registration form.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Registration Form After Fields','uaio'); ?></label></th>
                <td>
                    <?php wp_editor( $reg_content_after_fields, 'uaio-reg-form-after-fields');  ?>
                    <i original-title="<?php _e('Add custom content after the fields section of registration form.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Registration Form Header Title Message','uaio'); ?></label></th>
                <td>
                    <?php wp_editor( $reg_content_header_title_message, 'uaio-reg-form-header-title-message');  ?>
                    <i original-title="<?php _e('Add a custom message to the registration form header title..','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            
        </table>
    </div>
