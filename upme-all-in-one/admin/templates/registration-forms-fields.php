<?php 
    global $upme_admin,$registration_forms_data;
    extract($registration_forms_data);
?>
    <div class="uaio-settings-form ">
        <table class="form-table">
              
            <tr>
                <td colspan="2"><div id="uaio-registration-field-err" style="display:none"></div></td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Registration Fields ','uaio'); ?></label></th>
                <td>
                    <select name="uaio-registration-fields" id="uaio-registration-fields" >
                    <option value="0" ><?php _e('Choose Fields','uaio') ?></option>
                    <?php foreach($reg_fields as $k=>$v){ ?>
                    <option value="<?php echo $k; ?>"  ><?php echo $v; ?></option>       
                    <?php } ?>
                    </select>
                    <input type="hidden" name="uaio-registration-fields-hidden" id="uaio-registration-fields-hidden" value="" />
                    <input type="hidden" name="uaio-registration-fields-status-hidden" id="uaio-registration-fields-status-hidden" value="" />
                    <input type="button" class="button button-primary button-large" name="uaio-registration-field-btn" id="uaio-registration-field-btn" value="<?php _e('Add Field','uaio'); ?>" />
                    <i original-title="<?php _e('Add the necessary fields to the registration form.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ul id="uaio-reg-fields-list">
                        <?php foreach($reg_form_fields as $key => $field){ 
                                if($field != ''){
                        ?>
                        <li data-meta="<?php echo $field; ?>"><?php echo $reg_fields[$field]; ?><span class="uaio-reg-field-delete"><i class="upme-icon upme-icon-remove"></i></span></li>
                        <?php } } ?>
                    </ul>
                </td>
            </tr>

            
            
            
        </table>
    </div>
