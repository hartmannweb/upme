<?php 
    global $upme_admin,$user_profile_data;
    extract($user_profile_data);
?>
    <div class="uaio-settings-form ">
        
        <?php echo wp_nonce_field( basename( __FILE__ ), 'uaio_user_profiles_fields_nonce' ); ?>
        
        <table class="form-table">
            <tr>
                <th><label class="uaio-form-label"><?php _e('User Profile Page Name','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-user-profile-name" id="uaio-user-profile-name" value="<?php echo $profile_form_name; ?>"   />
                    <i original-title="<?php _e('Add specific name to profile form to load different fields and features on different user profile forms. If not specified, this will add a dynamic random string as the name.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('User','uaio'); ?></label></th>
                <td>

                    <select name="uaio-profile-user" id="uaio-profile-user" class="chosen-admin_setting" >
                        <option value="default" <?php selected( 'default', $profile_user); ?> ><?php _e('Display Profile Based on URL','uaio'); ?></option>
                    <?php foreach($user_profiles as $k => $user_profile){ 
                            $name = trim(get_user_meta($user_profile->ID,'first_name',true) . ' ' . get_user_meta($user_profile->ID,'last_name',true));
                            $name = ($name == '') ? $user_profile->data->user_login : $name;
                    ?>
                        <option value="<?php echo $user_profile->ID; ?>" <?php selected( $user_profile->ID, $profile_user); ?> ><?php echo $name; ?></option>
                    <?php } ?>     
                        
                    </select>
                    <i original-title="<?php _e('Select a user to display or use page URL to load the user.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr> 
            <tr>
                <th><label class="uaio-form-label"><?php _e('Profile View Type','uaio'); ?></label></th>
                <td>
                    <select name="uaio-profile-view-type" id="uaio-profile-view-type" class="chosen-admin_setting" >                   
                        
                        <option value="default" <?php selected( 'default', $profile_view_type); ?> ><?php _e('Display Full Profile','uaio'); ?></option>
                        <option value="compact" <?php selected( 'compact', $profile_view_type); ?> ><?php _e('Display Compact Profile','uaio'); ?></option>
                        <option value="fields" <?php selected( 'fields', $profile_view_type); ?> ><?php _e('Display Selected Fields on Profile','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Select the type of view profile. Full profile displays all fields. Compact profile only displays the profile header. Selected fields allows you to select the filds to be displayed in profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <?php $profile_field_display_status = ($profile_view_type == 'fields') ? '' : 'display:none'; ?>
            <tr style="<?php echo $profile_field_display_status; ?>">
                <th><label class="uaio-form-label"><?php _e('Profile Fields','uaio'); ?></label></th>
                <td>
                    <select placeholder="Select Fields" multiple name="uaio-profile-fields[]" id="uaio-profile-fields" class="chosen-admin_setting" >
                        
                    <?php foreach($user_profile_fields as $k => $user_profile_field){  
                            if($user_profile_field['meta'] != 'user_pass' && $user_profile_field['meta'] != 'user_pass_confirm'){ 
                                $profile_fields = (array) $profile_fields;
                                
                                $uaio_profile_fields_sel = '';
                                if(in_array($user_profile_field['meta'], $profile_fields)){
                                    $uaio_profile_fields_sel = ' selected=selected ';
                                }
                    ?>
                            <option value="<?php echo $user_profile_field['meta']; ?>" <?php echo $uaio_profile_fields_sel; ?> ><?php echo $user_profile_field['name']; ?></option>

                    
                    <?php }} ?>     
                        
                    </select>
                    <i original-title="<?php _e('Select profile fields to be displayed inside the profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display User ID','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-user-id" id="uaio-display-user-id" class="chosen-admin_setting" >                   
                        
                        <option value="no" <?php selected( 'no', $display_user_id); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $display_user_id); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used to show/hide user ID on profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Profile Status','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-profile-status" id="uaio-display-profile-status" class="chosen-admin_setting" >
                        <option value="no" <?php selected( 'no', $display_profile_status); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $display_profile_status); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used to show/hide profile ACTIVE/INACTIVE status on profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Profile Stats','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-profile-stats" id="uaio-display-profile-stats" class="chosen-admin_setting" >                        
                        <option value="yes" <?php selected( 'yes', $display_profile_stats); ?> ><?php _e('Yes','uaio'); ?></option>
                        <option value="no" <?php selected( 'no', $display_profile_stats); ?> ><?php _e('No','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used to show/hide the user profile stats inside the View Profile section.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Social Bar','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-social-bar" id="uaio-display-social-bar" class="chosen-admin_setting" >                        
                        <option value="yes" <?php selected( 'yes', $display_social_bar); ?> ><?php _e('Yes','uaio'); ?></option>
                        <option value="no" <?php selected( 'no', $display_social_bar); ?> ><?php _e('No','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used for displaying/hiding social icons on profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Use In Sidebar','uaio'); ?></label></th>
                <td>
                    <select name="uaio-use-in-sidebar" id="uaio-use-in-sidebar" class="chosen-admin_setting" >
                        <option value="no" <?php selected( 'no', $use_in_sidebar); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $use_in_sidebar); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used for displaying profile in sidebar with proper styles.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display User Role','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-user-role" id="uaio-display-user-role" class="chosen-admin_setting" >
                        <option value="no" <?php selected( 'no', $display_user_role); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $display_user_role); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used for displaying the user role inside the profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Logout Redidirect URL','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-logout-redirect" id="uaio-logout-redirect" value="<?php echo $logout_redirect; ?>"   />
                    <i original-title="<?php _e('Used for specifying the redirect URL after the logout button is clicked from the profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
        </table>
    </div>
