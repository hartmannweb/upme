<?php 
    global $upme_admin,$user_listing_data;
    extract($user_listing_data);
?>
    <div class="uaio-settings-form ">
        
        <?php echo wp_nonce_field( basename( __FILE__ ), 'uaio_user_listing_fields_nonce' ); ?>
        
        <table class="form-table">
            <tr>
                <th><label class="uaio-form-label"><?php _e('User Listing Name','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-user-listing-name" id="uaio-user-listing-name" value="<?php echo $user_listing_name; ?>"   />
                    <i original-title="<?php _e('Add specific name to member list to load different fields and features on different user listings. If not specified, this will add a dynamic random string as the name.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            

            <tr>
                <th><label class="uaio-form-label"><?php _e('Profile Grouping Type','uaio'); ?></label></th>
                <td>
                    <select name="uaio-profile-grouping-type" id="uaio-profile-grouping-type" class="chosen-admin_setting" >                   
                        
                        <option value="all" <?php selected( 'all', $profile_grouping_type); ?> ><?php _e('Group All Users','uaio'); ?></option>
                        <option value="users" <?php selected( 'users', $profile_grouping_type); ?> ><?php _e('Group Selected Users','uaio'); ?></option>
                        
                    </select>
                    <i original-title="<?php _e('Select the grouping type for member list. Group All Users displays all active users in the site. Group Selected Users allows you to select the users to be displayed in member list.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <?php $listing_users_display_status = ($profile_grouping_type == 'users') ? '' : 'display:none'; ?>
            <tr style="<?php echo $listing_users_display_status; ?>">
                <th><label class="uaio-form-label"><?php _e('Users','uaio'); ?></label></th>
                <td>

                    <select name="uaio-listing-users[]" id="uaio-listing-users" multiple class="chosen-admin_setting" >
                        
                    <?php foreach($user_profiles as $k => $user_profile){ 
                            $name = trim(get_user_meta($user_profile->ID,'first_name',true) . ' ' . get_user_meta($user_profile->ID,'last_name',true));
                            $name = ($name == '') ? $user_profile->data->user_login : $name;
        
                            $uaio_profiles_sel = '';
                            if(in_array($user_profile->ID, $listing_users)){
                                $uaio_profiles_sel = ' selected=selected ';
                            }
                    ?>
                        <option value="<?php echo $user_profile->ID; ?>" <?php echo $uaio_profiles_sel; ?> ><?php echo $name; ?></option>
                    <?php } ?>     
                        
                    </select>
                    <i original-title="<?php _e('Select users to display on the list.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr> 

            <tr>
                <th><label class="uaio-form-label"><?php _e('Group Meta Key','uaio'); ?></label></th>
                <td>

                    <select name="uaio-listing-meta-key" id="uaio-listing-meta-key" class="chosen-admin_setting" >
                        <option value="0" <?php selected('0' , $listing_meta_key); ?> ><?php echo __('Select Meta Key','upme'); ?></option>
                    <?php 
                        
                        
                        foreach($user_profile_fields as $k => $user_profile_field){  
                            if(isset($user_profile_field['meta']) && $user_profile_field['meta'] != 'user_pass' && $user_profile_field['meta'] != 'user_pass_confirm'){ 
                                $profile_fields = (array) $profile_fields;
                    ?>
                            <option value="<?php echo $user_profile_field['meta']; ?>" <?php selected($user_profile_field['meta'] , $listing_meta_key); ?> ><?php echo $user_profile_field['name']; ?></option>

                    
                    <?php }} ?>     
                        
                    </select>
                    <i original-title="<?php _e('Custom field key to group the users for the user list.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Group Meta Value','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-listing-meta-value" id="uaio-listing-meta-value" value="<?php echo $listing_meta_value; ?>"   />
                    
                    <i original-title="<?php _e('Custom field value to group the users for the user list.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Profile View Type','uaio'); ?></label></th>
                <td>
                    <select name="uaio-profile-view-type" id="uaio-profile-view-type" class="chosen-admin_setting" >                   
                        
                        <option value="default" <?php selected( 'default', $profile_view_type); ?> ><?php _e('Display Full Profile','uaio'); ?></option>
                        <option value="compact" <?php selected( 'compact', $profile_view_type); ?> ><?php _e('Display Compact Profile','uaio'); ?></option>
                        <option value="fields" <?php selected( 'fields', $profile_view_type); ?> ><?php _e('Display Selected Fields on Profile','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Select the type of view profile. Full profile displays all fields. Compact profile only displays the profile header. Selected fields allows you to select the fields to be displayed in profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <?php $profile_field_display_status = ($profile_view_type == 'fields') ? '' : 'display:none'; ?>
            <tr style="<?php echo $profile_field_display_status; ?>">
                <th><label class="uaio-form-label"><?php _e('Profile Fields','uaio'); ?></label></th>
                <td>
                    <select placeholder="Select Fields" multiple name="uaio-profile-fields[]" id="uaio-profile-fields" class="chosen-admin_setting" >
                        
                    <?php foreach($user_profile_fields as $k => $user_profile_field){  
                            if(isset($user_profile_field['meta']) && $user_profile_field['meta'] != 'user_pass' && $user_profile_field['meta'] != 'user_pass_confirm'){ 
                                $profile_fields = (array) $profile_fields;
                                
                                $uaio_profile_fields_sel = '';
                                if(in_array($user_profile_field['meta'], $profile_fields)){
                                    $uaio_profile_fields_sel = ' selected=selected ';
                                }
                    ?>
                            <option value="<?php echo $user_profile_field['meta']; ?>" <?php echo $uaio_profile_fields_sel; ?> ><?php echo $user_profile_field['name']; ?></option>

                    
                    <?php }} ?>     
                        
                    </select>
                    <i original-title="<?php _e('Select profile fields to be displayed inside the profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            
            
            
            
            
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('User Roles','uaio'); ?></label></th>
                <td>
                    <select name="uaio-user-roles[]" id="uaio-user-roles"  multiple class="chosen-admin_setting" >                   
                        
                        <?php foreach($user_roles as $k => $user_role){  
                                
                                $uaio_roles_sel = '';
                                if(in_array($k, $listing_user_roles)){
                                    $uaio_roles_sel = ' selected=selected ';
                                }
                    ?>
                            <option value="<?php echo $k; ?>" <?php echo $uaio_roles_sel; ?> ><?php echo $user_role; ?></option>

                    
                    <?php } ?>
                        
                    </select>
                    <i original-title="<?php _e('Hides/Shows user list and only shows search results.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Orderby Field','uaio'); ?></label></th>
                <td>

                    <select name="uaio-order-by" id="uaio-order-by" class="chosen-admin_setting" >
                        <option value="0" <?php selected('0' , $order_by); ?> ><?php echo __('Select Orderby Field','upme'); ?></option>
                    <?php 
                        
                        $user_predefined_fields = array('ID' => 'ID', 'login' => 'Username', 'nicename' => 'Display Name', 'email' => 'Email', 'url' => 'URL', 'registered' => 'Registered Date', 'post_count' => 'Post Count' );
                        foreach($user_predefined_fields as $k => $user_predefined_field){  
                            
                               
                    ?>
                            <option value="<?php echo $k; ?>" <?php selected($k , $order_by); ?> ><?php echo $user_predefined_field; ?></option>

                    
                    <?php } 
                        
                        foreach($user_profile_fields as $k => $user_profile_field){  
                            if(isset($user_profile_field['meta']) && $user_profile_field['meta'] != 'user_pass' && $user_profile_field['meta'] != 'user_pass_confirm'){ 
                                
                    ?>
                            <option value="<?php echo $user_profile_field['meta']; ?>" <?php selected($user_profile_field['meta'] , $order_by); ?> ><?php echo $user_profile_field['name']; ?></option>

                    
                    <?php }} ?>     
                        
                    </select>
                    <i original-title="<?php _e('Define a custom field or predefined option to order the results in user listing.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Hide Admins','uaio'); ?></label></th>
                <td>
                    <select name="uaio-hide-admins" id="uaio-hide-admins" class="chosen-admin_setting" >
                        <option value="no" <?php selected( 'no', $hide_admins); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $hide_admins); ?> ><?php _e('Yes','uaio'); ?></option>                     </select>
                    
                    <i original-title="<?php _e('Display/Hide administrators from the member list.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            
            
            
        </table>
    </div>
