<?php 
    global $upme_admin,$profile_tabs_data;
    extract($profile_tabs_data);
?>
    <div class="uaio-settings-form ">
        <table class="form-table">

            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Status','uaio'); ?></label></th>
                <td>
                    <select name="uaio-tab-display-status" id="uaio-tab-display-status" >
                        <option value="0" <?php selected( '0', $uaio_tab_display_status); ?> ><?php _e('Display to Public','uaio'); ?></option>
                        <option value="1" <?php selected( '1', $uaio_tab_display_status); ?> ><?php _e('Display to All Logged-In Users','uaio'); ?></option>
                        <option value="2" <?php selected( '2', $uaio_tab_display_status); ?> ><?php _e('Display to Profile Owner','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Select the type of users who are allowed to view the tab and its contents','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>   
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Tab Status','uaio'); ?></label></th>
                <td>
                    <select name="uaio-tab-status" id="uaio-tab-status" >
                        <option value="1" <?php selected( '1', $uaio_tab_status); ?> ><?php _e('Enabled','uaio'); ?></option>
                        <option value="0" <?php selected( '0', $uaio_tab_status); ?> ><?php _e('Disabled','uaio'); ?></option>
                        
                    </select>
                    
                    <i original-title="<?php _e('Tab Status','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>

            <tr>
                <th><label class="uaio-form-label"><?php _e('Tab Icon','uaio'); ?></label></th>
                <td>
                    <label class="upme-icons"><input type="radio"  <?php echo ('0' == $uaio_icon)? 'checked=checked' : ''; ?> name="uaio-icon"
                        value="0" /> <?php _e('None','upme'); ?> </label> 
                    
                    <?php 
                        $x = 0; 
                        foreach($upme_admin->fontawesome as $icon) { 
                            $x++;
                            $checked = '';
                            if($icon == $uaio_icon){
                                $checked = ' checked=checked ';
                            }
                            
                    ?>
                        <label class="upme-icons"><input <?php echo $checked; ?> type="radio" name="uaio-icon"
                                value="<?php echo $icon; ?>" /><i
                                class="upme-icon upme-icon-<?php echo $icon; ?> upme-tooltip3"
                                title="<?php echo $icon; ?>"></i> 
                            </label>
 
                             
                    
                    <?php } ?>
                    <i original-title="<?php _e('Display icon of the tab inside the profile tabs section.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
                
            </tr>
            
            
        </table>
    </div>
