<?php 
    global $upme_admin,$registration_forms_data;
    extract($registration_forms_data);
?>
    <div class="uaio-settings-form ">
        
        <?php echo wp_nonce_field( basename( __FILE__ ), 'uaio_register_form_fields_nonce' ); ?>
        
        <table class="form-table">
            <tr>
                <th><label class="uaio-form-label"><?php _e('Registration Form Name','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-registration-form-name" id="uaio-registration-form-name" value="<?php echo $uaio_registration_form_name; ?>"   />
                    <i original-title="<?php _e('Add specific name to registration form to load different fields on different registration forms. If not specified, this will add a dynamic random string as the name.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Redirect URL','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-registration-redirect" id="uaio-registration-redirect" value="<?php echo $uaio_redirect_url; ?>"   />
                    <i original-title="<?php _e('Users are redirected to the specified URL after completing registration.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>   
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Captcha ','uaio'); ?></label></th>
                <td>
                    <select name="uaio-registration-captcha" id="uaio-registration-captcha" >
                        <option value="0" <?php selected( '0', $uaio_captcha); ?> ><?php _e('Disabled','uaio'); ?></option>
                        <option value="recaptcha" <?php selected( 'recaptcha', $uaio_captcha); ?> ><?php _e('reCaptcha','uaio'); ?></option>
                        <option value="funcaptcha" <?php selected( 'funcaptcha', $uaio_captcha); ?> ><?php _e('FunCaptcha','uaio'); ?></option>
                        <option value="captchabestwebsoft" <?php selected( 'captchabestwebsoft', $uaio_captcha); ?> ><?php _e('Captcha','uaio'); ?></option>
                        
                    </select>
                    
                    <i original-title="<?php _e('Show the registration form without captcha,
	even if a captcha is set in UPME settings.You can specify the captcha to be used with the following','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('User Role ','uaio'); ?></label></th>
                <td>
                    
                    <select name="uaio-registration-user-role" id="uaio-registration-user-role" >
                        <option value="0" <?php selected( '0', $uaio_user_role); ?> ><?php _e('Default Role','uaio') ?></option>
                        <?php foreach($user_roles as $k=>$v){ ?>
                        <option value="<?php echo $k; ?>" <?php selected( $k, $uaio_user_role); ?> ><?php echo $v; ?></option>       
                        <?php } ?>   
                    </select>
                    
                    <i original-title="<?php _e('Add specific to the registration form. Once user_role attribute is added, all users registred with this form will get the defined user role instead of default user role.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>

            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Login ','uaio'); ?></label></th>
                <td>
                    <select name="uaio-registration-display-login" id="uaio-registration-display-login" >
                        <option value="0" <?php selected( '0', $uaio_display_login); ?> ><?php _e('Disabled','uaio'); ?></option>
                        <option value="1" <?php selected( '1', $uaio_display_login); ?> ><?php _e('Enabled','uaio'); ?></option>
                        
                        
                    </select>
                    
                    <i original-title="<?php _e('Displays the login link on registration form for already registered users.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            
        </table>
    </div>
