<?php 
    global $upme_admin,$user_listing_data;
    extract($user_listing_data);
?>
    <div class="uaio-settings-form ">

        
        <table class="form-table">
            
            
            
            

            
            
            
            
            
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Order Results','uaio'); ?></label></th>
                <td>
                    <select name="uaio-order-results" id="uaio-order-results" class="chosen-admin_setting" >                   
                        
                        <option value="asc" <?php selected( 'asc', $order_results); ?> ><?php _e('ASC','uaio'); ?></option>
                        <option value="desc" <?php selected( 'desc', $order_results); ?> ><?php _e('DESC','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Defines the Sort/order of the profiles displayed.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Hide Until Search','uaio'); ?></label></th>
                <td>
                    <select name="uaio-hide-until-search" id="uaio-hide-until-search" class="chosen-admin_setting" >                   
                        
                        <option value="no" <?php selected( 'no', $hide_until_search); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $hide_until_search); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Hides/Shows user list and only shows search results.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Users Per Page','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-users-per-page" id="uaio-users-per-page" value="<?php echo $users_per_page; ?>"   />
                    
                    <i original-title="<?php _e('Number of user profiles to be displayed inside a single page.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            
            
            
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Limit Results','uaio'); ?></label></th>
                <td>
                    <select name="uaio-limit-results" id="uaio-limit-results" class="chosen-admin_setting" >                                        <option value="no" <?php selected( 'no', $limit_results); ?> ><?php _e('No','uaio'); ?></option>
                         <option value="yes" <?php selected( 'yes', $limit_results); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    
                    <i original-title="<?php _e('Limit the member list to fixed number of users without pagination.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Open Profiles In New Window','uaio'); ?></label></th>
                <td>
                    <select name="uaio-new-window" id="uaio-new-window" class="chosen-admin_setting" >                                        <option value="no" <?php selected( 'no', $new_window); ?> ><?php _e('No','uaio'); ?></option>
                         <option value="yes" <?php selected( 'yes', $new_window); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    
                    <i original-title="<?php _e('Open profiles in a new window from the members list.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Open Profiles In Modal Window','uaio'); ?></label></th>
                <td>
                    <select name="uaio-modal-window" id="uaio-modal-window" class="chosen-admin_setting" >                                        <option value="no" <?php selected( 'no', $modal_window); ?> ><?php _e('No','uaio'); ?></option>
                         <option value="yes" <?php selected( 'yes', $modal_window); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    
                    <i original-title="<?php _e('Open profiles in a modal window from a member list with compact view.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Logout Redidirect URL','uaio'); ?></label></th>
                <td>
                    <input type="text" name="uaio-logout-redirect" id="uaio-logout-redirect" value="<?php echo $logout_redirect; ?>"   />
                    <i original-title="<?php _e('Used for specifying the redirect URL after the logout button is clicked from the profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
        </table>
    </div>
