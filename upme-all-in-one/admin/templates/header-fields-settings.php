<?php 
    global $uaio_settings_data; 
    extract($uaio_settings_data);


    $header_fields = $header_fields_list;
    $tab_fields = $tab_fields_list;

    $filtered_fields = array('user_pic','user_pass','user_pass_confirm');

    $profile_fields = get_option('upme_profile_fields');

?>

<form method="post" action="">
<table class="form-table">
    
                <tr>
                    <th><label for=""><?php _e('Profile Header Fields','uaio'); ?></label></th>
                    <td style="width:500px;">
                    <select  name="uaio_header_fields[header_fields_list][]" multiple class="chosen-admin_setting">
                        <option <?php echo in_array('0',$header_fields)? 'selected' : ''; ?> value="0" ></option>
                        <?php foreach($profile_fields as $k => $field){
                            if($field['type'] == 'usermeta' && !in_array($field['meta'],$filtered_fields)){
                                $selected = '';
                                if(in_array($field['meta'],$header_fields)){
                                    $selected = 'selected';
                                }
                        ?>
                            <option <?php echo $selected; ?>  value="<?php echo $field['meta']; ?>" ><?php echo $field['name']; ?></option>
                        <?php
                            }            
                        }
                        ?>
                    </select>
                    
                </td>
                </tr>
                <tr>
                    <th><label for=""><?php _e('Header Field Display Type','uaio'); ?></label></th>
                    <td style="width:500px;">
                    <select  name="uaio_header_fields[header_fields_display_type]" class="chosen-admin_setting">
                        <option <?php echo selected('0',$header_fields_display_type); ?> value="0" ><?php _e('Display with Label','uaio'); ?></option>
                        <option <?php echo selected('1',$header_fields_display_type); ?> value="1" ><?php _e('Display without Label','uaio'); ?></option>
                        
                    </select>
                    
                </td>
                </tr>
                <tr>
                    <th><label for=""><?php _e('Display Header Fields In Compact View','uaio'); ?></label></th>
                    <td style="width:500px;">
                    <select  name="uaio_header_fields[header_fields_compact_view_status]" class="chosen-admin_setting">
                        <option <?php echo selected('0',$header_fields_compact_view_status); ?> value="0" ><?php _e('No','uaio'); ?></option>
                        <option <?php echo selected('1',$header_fields_compact_view_status); ?> value="1" ><?php _e('Yes','uaio'); ?></option>
                        
                    </select>
                    
                </td>
                </tr>
                <tr>
                    <th><label for=""><?php _e('Profile Tab Fields','uaio'); ?></label></th>
                    <td style="width:500px;">
                    <select  name="uaio_header_fields[tab_fields_list][]" multiple class="chosen-admin_setting">
                        <option <?php echo in_array('0',$tab_fields)? 'selected' : ''; ?> value="0" ></option>
                        <?php foreach($profile_fields as $k => $field){
                            if($field['type'] == 'usermeta' && !in_array($field['meta'],$filtered_fields)){
                                $selected = '';
                                if(in_array($field['meta'],$tab_fields)){
                                    $selected = 'selected';
                                }
                        ?>
                            <option <?php echo $selected; ?>  value="<?php echo $field['meta']; ?>" ><?php echo $field['name']; ?></option>
                        <?php
                            }            
                        }
                        ?>
                    </select>

                    </td>
                </tr>
    
    <input type="hidden" name="uaio_tab" value="<?php echo $tab; ?>" />

    
</table>
    <?php submit_button(); ?>
</form>