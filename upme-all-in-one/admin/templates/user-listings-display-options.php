<?php 
    global $upme_admin,$user_listing_data;
    extract($user_listing_data);
?>
    <div class="uaio-settings-form ">
        
        
        <table class="form-table">          
            <tr>
                <th><label class="uaio-form-label"><?php _e('Show Result Count','uaio'); ?></label></th>
                <td>
                    <select name="uaio-result-count" id="uaio-result-count" class="chosen-admin_setting" > 
                        <option value="no" <?php selected( 'no', $result_count); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $result_count); ?> ><?php _e('Yes','uaio'); ?></option>                     </select>
                    
                    <i original-title="<?php _e('Display/Hide number of results generated from the user list.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr> 
            <tr>
                <th><label class="uaio-form-label"><?php _e('Number of Columns','uaio'); ?></label></th>
                <td>
                    <select name="uaio-list-width" id="uaio-list-width" class="chosen-admin_setting" >                   
                        
                        <option value="0" <?php selected( '0', $list_width); ?> ><?php _e('Please Select','uaio'); ?></option>
                        <option value="2" <?php selected( '2', $list_width); ?> ><?php _e('2 Columns','uaio'); ?></option>
                        <option value="3" <?php selected( '3', $list_width); ?> ><?php _e('3 Columns','uaio'); ?></option>
                    </select>
                    
                    <i original-title="<?php _e('Number of user profiles to be displayed inside a single page.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display User ID','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-user-id" id="uaio-display-user-id" class="chosen-admin_setting" >                   
                        
                        <option value="no" <?php selected( 'no', $display_user_id); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $display_user_id); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used to show/hide user Id on profiles in user listing.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Profile Status','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-profile-status" id="uaio-display-profile-status" class="chosen-admin_setting" >
                        <option value="no" <?php selected( 'no', $display_profile_status); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $display_profile_status); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used to show/hide the user profile status inside the View Profile section.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Profile Stats','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-profile-stats" id="uaio-display-profile-stats" class="chosen-admin_setting" >                        
                        <option value="yes" <?php selected( 'yes', $display_profile_stats); ?> ><?php _e('Yes','uaio'); ?></option>
                        <option value="no" <?php selected( 'no', $display_profile_stats); ?> ><?php _e('No','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used to show/hide the user profile stats inside the View Profile section.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display Social Bar','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-social-bar" id="uaio-display-social-bar" class="chosen-admin_setting" >                        
                        <option value="yes" <?php selected( 'yes', $display_social_bar); ?> ><?php _e('Yes','uaio'); ?></option>
                        <option value="no" <?php selected( 'no', $display_social_bar); ?> ><?php _e('No','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used for displaying/hiding social icons on profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
            <tr>
                <th><label class="uaio-form-label"><?php _e('Display User Role','uaio'); ?></label></th>
                <td>
                    <select name="uaio-display-user-role" id="uaio-display-user-role" class="chosen-admin_setting" >
                        <option value="no" <?php selected( 'no', $display_user_role); ?> ><?php _e('No','uaio'); ?></option>
                        <option value="yes" <?php selected( 'yes', $display_user_role); ?> ><?php _e('Yes','uaio'); ?></option>
                    </select>
                    <i original-title="<?php _e('Used for displaying the user role inside the profile.','uaio'); ?>" class="upme-icon upme-icon-question-circle upme-tooltip2 option-help"></i>
                </td>
            </tr>
            
        </table>
    </div>
