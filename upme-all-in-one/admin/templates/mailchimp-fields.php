<?php 
    global $uaio_settings_data; 
    extract($uaio_settings_data);


//Array
//(
//    [status] => error
//    [code] => 104
//    [name] => Invalid_ApiKey
//    [error] => Invalid MailChimp API Key: dewfew
//)
    $profile_fields = get_option('upme_profile_fields');
// name , meta  field
    
    $uaio_options = get_option('uaio_options');

    $api_key =  isset($uaio_options['mailchimp']['api_key']) ? $uaio_options['mailchimp']['api_key'] : 'invalid-us1';
    $api_key = (strpos($api_key,'-us1')) ? $api_key : $api_key . '-us1';
//echo "<pre>";print_r($uaio_options);exit;
    $MailChimp = new \Drewm\MailChimp($api_key);
    $result = $MailChimp->call('lists/list', array());
    //echo "<pre>";print_r($result);exit;

    $mailchimp_field_lists = get_option('uaio_mailchimp_field_lists');
?>

<form method="post" action="">
<table class="form-table">
                <?php foreach($profile_fields as $field){
                        extract($field);
                        if($field == 'mailchimp'){
                ?>
                <tr>
                    <th><label for=""><?php echo $name; ?></label></th>
                    <td style="width:500px;">
                        <select  name="uaio_mailchimp_fields[<?php echo $meta; ?>]"  class="chosen-admin_setting">
                            <option <?php echo (isset($mailchimp_field_lists[$meta]) && '0' == $mailchimp_field_lists[$meta]) ? 'selected' : ''; ?> value="0" ><?php _e('Select','uaio'); ?></option>
                        <?php 
                            if(!isset($result['error']) && isset($result['data'])){ 
                                foreach($result['data'] as $mlist){
                                    $selected = '';
                                    
                                    if(isset($mailchimp_field_lists[$meta]) && $mlist['id'] == $mailchimp_field_lists[$meta]){
                                        $selected = ' selected ';
                                    }
                        ?>                       
                                <option <?php echo $selected; ?>  value="<?php echo $mlist['id']; ?>" ><?php echo $mlist['name']; ?></option>

                        <?php }} ?>
                        </select>
                </td>
                </tr>
                <?php }} ?>
    
    <input type="hidden" name="uaio_tab" value="<?php echo $tab; ?>" />

    
</table>

    <?php submit_button(); ?>
</form>