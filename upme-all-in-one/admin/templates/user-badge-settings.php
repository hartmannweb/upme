<?php 
    global $uaio_settings_data; 
    extract($uaio_settings_data);

    $badges_query = new WP_Query( array( 'post_status' => 'publish', 'post_type'=> UAIO_USER_BADGES_POST_TYPE ,'posts_per_page' => '-1' ) );
        
    $loop = array();
    if ( $badges_query->have_posts() ) :
        while ($badges_query->have_posts()) : $badges_query->the_post();
            array_push($loop, array('key' => get_the_ID(), 'value' => get_the_title()));
        endwhile;
    endif;

    $profile_fields = get_option('upme_profile_fields');
//echo "<pre>";print_r($field_badges);exit;
?>

<form method="post" action="">
<table class="form-table">
    
    <?php foreach($profile_fields as $key => $field ){ 
            if(isset($field['field']) && $field['field'] == 'user_badge'){
                $fieldmeta = isset($field['meta']) ? $field['meta'] : '';
                $field_badges_meta = (isset($field_badges[$fieldmeta]) && ($field_badges[$fieldmeta] != '')) ? $field_badges[$fieldmeta] : array();
    ?>
    
                <tr>
                    <th><label for=""><?php echo $field['name']; ?></label></th>
                    <td>
                        <select multiple name="uaio_badge[field_badges][<?php echo $fieldmeta; ?>][]" class="chosen-admin_setting">
                            
                            <option value="0" <?php echo in_array('0', $field_badges_meta) ? 'selected' : ''; ?> ></option>
                            <?php
                                foreach($loop as $k=>$v){
                                    $selected = '';
                                    $meta = $field['meta'];

                                    if(is_array($field_badges_meta) && in_array($v['key'], $field_badges_meta)){
                                        $selected = ' selected ';
                                    }
                            ?>
                                <option value="<?php echo $v['key'];?>" <?php echo $selected; ?> ><?php echo $v['value'];?></option>
                            <?php                        
                                }
                            ?>
                        </select>

                        <span class="description"><?php echo __('Select the badges for this field.','uaio');?></span>
                    </td>
                </tr>
    
    <?php }} ?>
<!--
    <tr>
        <th><label for=""><?php echo __('Default Badge','uaio'); ?></label></th>
        <td>
            <select name="uaio_badge[default_badge]" class="chosen-admin_setting">
                <option value="0" <?php echo selected($default_badge,'0',true); ?> ></option>
                <?php
                    foreach($loop as $k=>$v){
                ?>
                    <option value="<?php echo $v['key'];?>" <?php echo selected($default_badge,$v['key'],true); ?> ><?php echo $v['value'];?></option>
                <?php                        
                    }
                ?>
            </select>
            
            <span class="description"><?php echo __('Select the default badge for this site.','uaio');?></span>
        </td>
    </tr>
    <tr>
        <th><label for=""><?php echo __('Apply Default Badge to All Users','uaio'); ?></label></th>
        <td>
            <input type="submit" name="uaio_apply_default_badge" value="<?php _e('Apply Default Badge','uaio');?>" />
            
            
        </td>
    </tr>
-->
    
    
    
    
    <input type="hidden" name="uaio_tab" value="<?php echo $tab; ?>" />

    
</table>
    <?php submit_button(); ?>
</form>