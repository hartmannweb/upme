<?php

class UAIO_User_Listings{
    
    public $uaio;

    public function __construct(){
        add_action('init',array($this,'register_user_listing'));
        add_action( 'admin_init', array($this,'add_user_listing_meta_box' ));
        add_action( 'save_post', array($this,'save_user_listing_fields'),10,2);   
        
        add_filter('manage_edit-' . UAIO_USER_USER_LISTING_POST_TYPE . '_columns', array($this,'user_listing_custom_columns'));
        add_action( 'manage_' . UAIO_USER_USER_LISTING_POST_TYPE . '_posts_custom_column', array( $this,'user_listing_custom_column_values'), 10, 2 );
       
        add_shortcode('uaio_user_listing', array($this,'process_user_listing_shortcode'));
    }
    
    public function register_user_listing(){
  
        register_post_type( UAIO_USER_USER_LISTING_POST_TYPE,
            array(
                'labels' => array(
                    'name'              => __('User Listings','uaio'),
                    'singular_name'     => __('User Listing','uaio'),
                    'add_new'           => __('Add New','uaio'),
                    'add_new_item'      => __('Add New User Listing','uaio'),
                    'edit'              => __('Edit','uaio'),
                    'edit_item'         => __('Edit User Listing','uaio'),
                    'new_item'          => __('New User Listing','uaio'),
                    'view'              => __('View','uaio'),
                    'view_item'         => __('View User Listing','uaio'),
                    'search_items'      => __('Search User Listing','uaio'),
                    'not_found'         => __('No User Listings found','uaio'),
                    'not_found_in_trash' => __('No User Listings found in Trash','uaio'),
                ),

                'public' => true,
                'menu_position' => 100,
                'supports' => array( 'title'),
                'has_archive' => true
            )
        );

    }
    
    public function add_user_listing_meta_box(){
        add_meta_box( 'user_listing_meta_box', __('User Listing Filtering Options','uaio'), array($this,'display_user_listing_info'), UAIO_USER_USER_LISTING_POST_TYPE, 'normal', 'high');
        
        add_meta_box( 'user_listing_meta_box2', __('User Listing General Options','uaio'), array($this,'display_user_listing_general_options'), UAIO_USER_USER_LISTING_POST_TYPE, 'normal', 'high');
        
        add_meta_box( 'user_listing_meta_box3', __('User Listing Display Options','uaio'), array($this,'display_user_listing_display_options'), UAIO_USER_USER_LISTING_POST_TYPE, 'normal', 'high');
    }
    
    public function display_user_listing_info($profile_form){
        global $user_listing_data,$uaio,$upme_roles;
        ob_start();    
        
        $user_listing_data['user_listing_name']     =    get_post_meta( $profile_form->ID, '_uaio_user_listing_name',  true );
        $user_listing_data['profile_view_type']     =    get_post_meta( $profile_form->ID, '_uaio_profile_view_type',  true );
        $user_listing_data['profile_fields']        =    get_post_meta( $profile_form->ID, '_uaio_profile_fields',  true );
        
        
        $user_listing_data['listing_users']         =    (array) get_post_meta( $profile_form->ID, '_uaio_listing_users', true );
        $user_listing_data['profile_grouping_type'] =    get_post_meta( $profile_form->ID, '_uaio_profile_grouping_type', true );        
        $user_listing_data['listing_meta_key']      =    get_post_meta( $profile_form->ID, '_uaio_listing_meta_key', true ); 
        $user_listing_data['listing_meta_value']    =    get_post_meta( $profile_form->ID, '_uaio_listing_meta_value', true );
        
        $user_listing_data['user_profiles']         = $uaio->users->get_active_users();
        $user_listing_data['user_profile_fields']   = get_option('upme_profile_fields');
        $user_listing_data['user_roles']            = $upme_roles->upme_get_available_user_roles();        
        $user_listing_data['listing_user_roles']    =    (array) get_post_meta( $profile_form->ID, '_uaio_user_roles', true );
        $user_listing_data['hide_admins']           =    get_post_meta( $profile_form->ID, '_uaio_hide_admins', true );
        $user_listing_data['order_by']              =    get_post_meta( $profile_form->ID, '_uaio_order_by', true );
        $uaio->template_loader->get_template_part('user-listings');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    public function display_user_listing_display_options($profile_form){
        global $user_listing_data,$uaio,$upme_roles;
        ob_start();    
        
        
        $user_listing_data['display_user_id']            =    get_post_meta( $profile_form->ID, '_uaio_display_user_id', true );
        $user_listing_data['display_profile_status']     =    get_post_meta( $profile_form->ID, '_uaio_display_profile_status', true );
        $user_listing_data['display_profile_stats']      =    get_post_meta( $profile_form->ID, '_uaio_display_profile_stats', true );
        $user_listing_data['display_social_bar']         =    get_post_meta( $profile_form->ID, '_uaio_display_social_bar', true );
        $user_listing_data['display_user_role']          =    get_post_meta( $profile_form->ID, '_uaio_display_user_role', true );
        $user_listing_data['list_width']                =    get_post_meta( $profile_form->ID, '_uaio_list_width', true );        
        $user_listing_data['result_count']              =    get_post_meta( $profile_form->ID, '_uaio_result_count', true );
        
        $uaio->template_loader->get_template_part('user-listings-display-options');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    public function display_user_listing_general_options($profile_form){
        global $user_listing_data,$uaio,$upme_roles;
        ob_start();    
        
        $user_listing_data['hide_until_search']      =    get_post_meta( $profile_form->ID, '_uaio_hide_until_search', true );
        $user_listing_data['order_results']      =    get_post_meta( $profile_form->ID, '_uaio_order_results', true );        
        $user_listing_data['users_per_page']      =    get_post_meta( $profile_form->ID, '_uaio_users_per_page', true );      
        $user_listing_data['limit_results']      =    get_post_meta( $profile_form->ID, '_uaio_limit_results', true );
        $user_listing_data['new_window']      =    get_post_meta( $profile_form->ID, '_uaio_new_window', true );
        $user_listing_data['modal_window']      =    get_post_meta( $profile_form->ID, '_uaio_modal_window', true );
        $user_listing_data['logout_redirect']            =    get_post_meta( $profile_form->ID, '_uaio_profile_logout_redirect', true );
        
        
        $uaio->template_loader->get_template_part('user-listings-general-options');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    
    
    public function save_user_listing_fields($listing_id, $listing) {
        
        $is_autosave = wp_is_post_autosave( $listing_id );
        $is_revision = wp_is_post_revision( $listing_id );
        $is_valid_nonce = ( isset( $_POST[ 'uaio_user_profiles_fields_nonce' ] ) && wp_verify_nonce( $_POST[ 'uaio_user_profiles_fields_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
        // Exits script depending on save status
        if ( !$is_valid_nonce ) {
            return;
        }
        
        if ( !current_user_can( 'manage_options') )
            return $listing_id;

        
        
        $user_listing_name      = isset( $_POST['uaio-user-listing-name'] ) ? $_POST['uaio-user-listing-name'] : '';
        $profile_view_type      = isset( $_POST['uaio-profile-view-type'] ) ? $_POST['uaio-profile-view-type'] : '';
        $profile_fields         = isset( $_POST['uaio-profile-fields'] ) ? $_POST['uaio-profile-fields'] : '';
        $display_user_id        = isset( $_POST['uaio-display-user-id'] ) ? $_POST['uaio-display-user-id'] : '';
        $display_profile_status = isset( $_POST['uaio-display-profile-status'] ) ? $_POST['uaio-display-profile-status'] : '';
        $display_profile_stats  = isset( $_POST['uaio-display-profile-stats'] ) ? $_POST['uaio-display-profile-stats'] : '';
        $display_social_bar     = isset( $_POST['uaio-display-social-bar'] ) ? $_POST['uaio-display-social-bar'] : '';
        $display_user_role      = isset( $_POST['uaio-display-user-role'] ) ? $_POST['uaio-display-user-role'] : '';
        $logout_redirect        = isset( $_POST['uaio-logout-redirect'] ) ? $_POST['uaio-logout-redirect'] : '';
        $listing_users          = isset( $_POST['uaio-listing-users'] ) ? $_POST['uaio-listing-users'] : '';
        $profile_grouping_type  = isset( $_POST['uaio-profile-grouping-type'] ) ? $_POST['uaio-profile-grouping-type'] : '';
        $listing_meta_key       = isset( $_POST['uaio-listing-meta-key'] ) ? $_POST['uaio-listing-meta-key'] : '';
        $listing_meta_value     = isset( $_POST['uaio-listing-meta-value'] ) ? $_POST['uaio-listing-meta-value'] : '';
        $hide_until_search      = isset( $_POST['uaio-hide-until-search'] ) ? $_POST['uaio-hide-until-search'] : '';
        $order_results          = isset( $_POST['uaio-order-results'] ) ? $_POST['uaio-order-results'] : '';
        $order_by               = isset( $_POST['uaio-order-by'] ) ? $_POST['uaio-order-by'] : '';
        
        $listing_user_roles     = isset( $_POST['uaio-user-roles'] ) ? $_POST['uaio-user-roles'] : '';
        $users_per_page         = isset( $_POST['uaio-users-per-page'] ) ? $_POST['uaio-users-per-page'] : '';
        $list_width             = isset( $_POST['uaio-list-width'] ) ? $_POST['uaio-list-width'] : '';
        
        $limit_results          = isset( $_POST['uaio-limit-results'] ) ? $_POST['uaio-limit-results'] : '';
        $hide_admins            = isset( $_POST['uaio-hide-admins'] ) ? $_POST['uaio-hide-admins'] : '';
        $result_count           = isset( $_POST['uaio-result-count'] ) ? $_POST['uaio-result-count'] : '';
        
        $new_window             = isset( $_POST['uaio-new-window'] ) ? $_POST['uaio-new-window'] : '';
        $modal_window           = isset( $_POST['uaio-modal-window'] ) ? $_POST['uaio-modal-window'] : '';


        if($user_listing_name == ''){
            $user_listing_name = wp_generate_password(12,false);
        }
        
        if ( $listing->post_type == UAIO_USER_USER_LISTING_POST_TYPE ) {
            //echo "<pre>";print_r($profile_fields);exit;
            update_post_meta( $listing_id, '_uaio_user_listing_name',  $user_listing_name );
            update_post_meta( $listing_id, '_uaio_profile_view_type',  $profile_view_type );
            update_post_meta( $listing_id, '_uaio_profile_fields',  $profile_fields );
            update_post_meta( $listing_id, '_uaio_display_user_id', $display_user_id );
            update_post_meta( $listing_id, '_uaio_display_profile_status', $display_profile_status );
            update_post_meta( $listing_id, '_uaio_display_profile_stats', $display_profile_stats );
            update_post_meta( $listing_id, '_uaio_display_social_bar', $display_social_bar );
            update_post_meta( $listing_id, '_uaio_display_user_role', $display_user_role );
            update_post_meta( $listing_id, '_uaio_profile_logout_redirect', $logout_redirect );
            update_post_meta( $listing_id, '_uaio_listing_users', $listing_users );
            update_post_meta( $listing_id, '_uaio_profile_grouping_type', $profile_grouping_type );
            update_post_meta( $listing_id, '_uaio_listing_meta_key', $listing_meta_key );
            update_post_meta( $listing_id, '_uaio_listing_meta_value', $listing_meta_value );
            update_post_meta( $listing_id, '_uaio_hide_until_search', $hide_until_search );
            update_post_meta( $listing_id, '_uaio_order_results', $order_results );
            update_post_meta( $listing_id, '_uaio_user_roles', $listing_user_roles );
            update_post_meta( $listing_id, '_uaio_users_per_page', $users_per_page );
            update_post_meta( $listing_id, '_uaio_list_width', $list_width );
            update_post_meta( $listing_id, '_uaio_limit_results', $limit_results );
            update_post_meta( $listing_id, '_uaio_hide_admins', $hide_admins );
            update_post_meta( $listing_id, '_uaio_order_by', $order_by );
            update_post_meta( $listing_id, '_uaio_result_count', $result_count );
            update_post_meta( $listing_id, '_uaio_new_window', $new_window );
            update_post_meta( $listing_id, '_uaio_modal_window', $modal_window );
            
        }
    }
    
    function user_listing_custom_columns( $columns ) {

        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Title','uaio'),
            'uaio_user_listing_shortcode' => __( 'User Listing Shortcode','uaio' ),
            'date' => __( 'Dates','uaio' )
        );

        return $columns;
    }

    function user_listing_custom_column_values($column, $post_id ) {
        global $post;

	    switch( $column ) {

		  /* If displaying the 'duration' column. */
		  case 'uaio_user_listing_shortcode' :
                echo "[uaio_user_listing id='". $post_id ."' ]";
                break;

		

		  /* Just break out of the switch statement for everything else. */
		  default :
			break;
	   }
        
    }
    
    public function process_user_listing_shortcode($atts,$content){
        extract(shortcode_atts(array(
		'id'	=>	'0',
     	), $atts));
        
        
        $user_listing_name     =    get_post_meta( $id, '_uaio_user_listing_name',  true );
        $profile_view_type     =    get_post_meta( $id, '_uaio_profile_view_type',  true );
        $profile_fields        =    get_post_meta( $id, '_uaio_profile_fields',  true );       
        $listing_users         =    (array) get_post_meta( $id, '_uaio_listing_users', true );
        $profile_grouping_type =    get_post_meta( $id, '_uaio_profile_grouping_type', true );        
        $listing_meta_key      =    get_post_meta( $id, '_uaio_listing_meta_key', true ); 
        $listing_meta_value    =    get_post_meta( $id, '_uaio_listing_meta_value', true );
        
        $listing_user_roles    =    get_post_meta( $id, '_uaio_user_roles', true );
        $hide_admins           =    get_post_meta( $id, '_uaio_hide_admins', true );
    
        $display_user_id            =    get_post_meta( $id, '_uaio_display_user_id', true );
        $display_profile_status     =    get_post_meta( $id, '_uaio_display_profile_status', true );
        $display_profile_stats      =    get_post_meta( $id, '_uaio_display_profile_stats', true );
        $display_social_bar         =    get_post_meta( $id, '_uaio_display_social_bar', true );
        $display_user_role          =    get_post_meta( $id, '_uaio_display_user_role', true );
        
        $list_width                 =    get_post_meta( $id, '_uaio_list_width', true );        
        $result_count               =    get_post_meta( $id, '_uaio_result_count', true );
        
        $hide_until_search          =    get_post_meta( $id, '_uaio_hide_until_search', true );
        $hide_until_search          =    ($hide_until_search == 'yes') ? true :  false;
        
        $order_results              =    get_post_meta( $id, '_uaio_order_results', true );   
        $order_by                   =    get_post_meta( $id, '_uaio_order_by', true ); 

        $users_per_page             =    get_post_meta( $id, '_uaio_users_per_page', true );      
        $limit_results              =    get_post_meta( $id, '_uaio_limit_results', true );
        $new_window                 =    get_post_meta( $id, '_uaio_new_window', true );
        $modal_window               =    get_post_meta( $id, '_uaio_modal_window', true );
        $logout_redirect            =    get_post_meta( $id, '_uaio_profile_logout_redirect', true );
        

        $sh_attributes = '';
        
        $sh_attributes  .= ' name="' . $user_listing_name . '" ';
        
        
        if($profile_grouping_type == 'all'){
            $sh_attributes  .= ' group="all" ';
        }else{
            if(is_array($listing_users) && count($listing_users) > 0){
                $sh_attributes  .= ' group="' . implode(',',$listing_users) . '" ';
            }else{
                $sh_attributes  .= ' group="all" ';
            }            
        }        
        
        if($profile_view_type == 'compact'){
            $sh_attributes  .= ' view="compact" ';
        }else if($profile_view_type == 'fields'){
            $profile_fields = implode(',',$profile_fields);
            $sh_attributes  .= ' view="' . $profile_fields . '" ';
        } 
        
        
        if(!($listing_meta_key == '0' || $listing_meta_key == '')){
            $sh_attributes  .= ' group_meta="' . $listing_meta_key . '" group_meta_value="' . $listing_meta_value . '" ';
        }
        
        
        if(is_array($listing_user_roles) && count($listing_user_roles) > 0){
            $sh_attributes  .= ' role="' . implode(',',$listing_user_roles) . '" ';
        }
        

        $sh_attributes  .= ' hide_admins="' . $hide_admins . '" ';
        $sh_attributes  .= ' show_id="' . $display_user_id . '" ';
        $sh_attributes  .= ' show_stats="' . $display_profile_stats . '" ';
        $sh_attributes  .= ' show_social_bar="' . $display_social_bar . '" ';     
        $sh_attributes  .= ' show_role="' . $display_user_role . '" ';
        $sh_attributes  .= ' show_profile_status="' . $display_profile_status . '" ';
        
        $sh_attributes  .= ' show_result_count="' . $result_count . '" ';
        
        if($hide_until_search != ''){
            $sh_attributes  .= ' hide_until_search="' . $hide_until_search . '" ';
        }
        
        
        
        if($list_width == '2' || $list_width == '3'){
            $sh_attributes  .= ' width="' . $list_width . '" ';
        }
                
        if($order_by != '' && $order_by != '0'){
            $preset_fields = array('ID', 'login', 'nicename', 'email', 'url', 'registered', 'post_count');
            if(!in_array($order_by,$preset_fields)){
                $sh_attributes  .= ' orderby_custom="yes" ';
            }
            $sh_attributes  .= ' orderby="' . $order_by . '" ';
        }
        $sh_attributes  .= ' order="' . $order_results . '" ';
        
        
        if($users_per_page != ''){
            $sh_attributes  .= ' users_per_page="' . $users_per_page . '" ';
        }
        
        $sh_attributes  .= ' limit_results="' . $limit_results . '" ';
        
        
        if($new_window == 'yes'){
            $sh_attributes  .= ' new_window="' . $new_window . '" '; 
        }
        
        if($modal_window == 'yes'){
            $sh_attributes  .= ' modal="' . $modal_window . '" '; 
        }
        
        if($logout_redirect != ''){
            $sh_attributes  .= ' logout_redirect ="' . $logout_redirect . '" ';
        }
        

        return do_shortcode('[upme '.$sh_attributes.' ]');
    }
    
}