<?php

class UAIO_User_Profile_Fields{
    
    public $uaio;
    public $settings;

    public function __construct(){
        $this->settings = get_option('uaio_options');
        add_filter('upme_profile_header_fields',array($this,'upmecustom_profile_header_fields'),10,2);
    }
    
    
    public function upmecustom_profile_header_fields($display,$params ){
        global $upme_options;
        extract($params);

        if($view == '' || $this->settings['header_fields']['header_fields_compact_view_status'] == '1'){
            $fields = get_option('upme_profile_fields');
            $field_names = array();
            $date_fields = array();
            foreach($fields as $field){
                if($field['type'] == 'usermeta' && isset($field['meta'])){
                    $field_names[$field['meta']] = $field['name'];
                }

                if(isset($field['field']) && $field['field'] == 'datetime'){
                    $date_fields[] = $field['meta'];
                }

            }    

            $header_fields = isset($this->settings['header_fields']['header_fields_list']) ? $this->settings['header_fields']['header_fields_list'] : array() ;
        
            $header_fields_display = isset($this->settings['header_fields']['header_fields_display_type']) ? $this->settings['header_fields']['header_fields_display_type'] : '0';

            if(is_array($header_fields)){
                foreach($header_fields as $header_field){
                    if($header_field != '0'){
                        $value = get_user_meta($id,$header_field,true);
                        if($header_field == 'user_url'){
                            $value = "<a href='".$value."' >".$value."</a>";
                        }

                        if(in_array($header_field, $date_fields)){
                                if('' != $value){
                                    $upme_settings = $upme_options->upme_settings;
                                    $upme_date_format = (string) isset($upme_settings['date_format']) ? $upme_settings['date_format'] : 'mm/dd/yy';       
                                    // echo "<pre>";echo $header_field;exit;
                                    $value = upme_date_format_to_custom($value, $upme_date_format);
                                }
                        }

                        if($header_fields_display == '0'){                            
                            $display .= "<p><strong>".$field_names[$header_field]." : </strong>".$value."</p>";
                        }else{
                            $display .= "<p>".$value."</p>";
                        }
                        
                        
                    }
                }
            }
        }

        return $display;
    }
}
    
    