<?php

class UAIO_Profile_Tabs{
    
    public $uaio;

    public function __construct(){
        add_action('init',array($this,'register_profile_tabs'));
        add_action('init',array($this,'init_tabs'));
        add_action( 'admin_init', array($this,'add_profile_tabs_meta_box' ));
        add_action( 'save_post', array($this,'save_profile_tab_fields'),10,2);
        
        
        
    }
    
    public function init_tabs(){
        add_filter('upme_profile_tab_items', array($this,'profile_tab_items'),10,2);
        add_filter('upme_profile_view_forms',array($this,'profile_view_forms'),10,2);
    }
    
    public function register_profile_tabs(){
  
        register_post_type( 'profile_tabs',
            array(
                'labels' => array(
                    'name'              => __('Profile Tabs','uaio'),
                    'singular_name'     => __('Profile Tab','uaio'),
                    'add_new'           => __('Add New','uaio'),
                    'add_new_item'      => __('Add New Profile Tab','uaio'),
                    'edit'              => __('Edit','uaio'),
                    'edit_item'         => __('Edit Profile Tab','uaio'),
                    'new_item'          => __('New Profile Tab','uaio'),
                    'view'              => __('View','uaio'),
                    'view_item'         => __('View Profile Tab','uaio'),
                    'search_items'      => __('Search Profile Tab','uaio'),
                    'not_found'         => __('No Profile Tab found','uaio'),
                    'not_found_in_trash' => __('No Profile Tab found in Trash','uaio'),
                ),

                'public' => true,
                'menu_position' => 100,
                'supports' => array( 'title', 'editor', 'thumbnail'),
                'has_archive' => true
            )
        );

    }
    
    public function add_profile_tabs_meta_box(){
        add_meta_box( 'profile_tabs_meta_box', __('Profile Tab Details','uaio'), array($this,'display_profile_tab_fields'), 'profile_tabs', 'normal', 'high');
    }
    
    public function display_profile_tab_fields($tab){
        global $profile_tabs_data;
        ob_start();    
        
        $profile_tabs_data['uaio_tab_status']           = get_post_meta( $tab->ID, '_uaio_tab_status', true );
        $profile_tabs_data['uaio_tab_display_status']   = get_post_meta( $tab->ID, '_uaio_tab_display_status', true );
        $profile_tabs_data['uaio_icon']                 = get_post_meta( $tab->ID, '_uaio_icon', true );  
        
        uaio_get_template('profile-tabs');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    public function save_profile_tab_fields($tab_id, $profile_tab) {

        if ( $profile_tab->post_type == 'profile_tabs' ) {

          if ( isset( $_POST['uaio-tab-display-status'] ) && $_POST['uaio-tab-display-status'] != '' ) {
            update_post_meta( $tab_id, '_uaio_tab_display_status',  $_POST['uaio-tab-display-status'] );
          }
            
          if ( isset( $_POST['uaio-icon'] ) && $_POST['uaio-icon'] != '') {
            update_post_meta( $tab_id, '_uaio_icon',  $_POST['uaio-icon'] );
          }
            
          if ( isset( $_POST['uaio-tab-status'] ) && $_POST['uaio-tab-status'] != '' ) {
            update_post_meta( $tab_id, '_uaio_tab_status',  $_POST['uaio-tab-status'] );
          }

        }
    }
    
    public function active_profiles(){
        $args = array(
            'post_type' => 'profile_tabs',
            'post_status'=>'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => '_uaio_tab_status',
                    'value' => '1'
                )
            )
        );
        $query = new WP_Query( $args );
        return $query;
    }
    
    public function profile_tab_items($display,$params){
        
        extract($params);
        if($view != 'compact' && $hide_profile_tabs != 'yes' ){
                
            $query = $this->active_profiles();

            if ( $query->have_posts() ) : 

                while ( $query->have_posts() ) : $query->the_post(); 
                    $id = get_the_ID();
                    $title = get_the_title();
                    $filtered_title = str_replace(' ','-',$title);
                    $icon = get_post_meta($id,'_uaio_icon',true);
                    $tab_display_status = get_post_meta($id,'_uaio_tab_display_status',true);
                    $display_status = $this->tab_display_status($tab_display_status,$params);
                    if($display_status){
                        $display .= '<div class="upme-profile-tab" data-tab-id="upme-'. $filtered_title .'-panel" >
                                <i class="upme-profile-icon upme-icon-' .$icon .'" ></i>';
                        if($params['title_display'] == 'enabled'){            
                            $display .= '<div class="upme-profile-tab-title">'. apply_filters('upme_profile_tab_items_'.$filtered_title.'_title', $title ,$params) . '</div>';
                        }
                        $display .= '</div>'; 
                        
                    }

                       


               endwhile;
            wp_reset_postdata(); 

            endif;
        }
        
            
        return $display;

    }
    
    public function profile_view_forms($display,$params){
        
        extract($params);
        if($view != 'compact' && $hide_profile_tabs != 'yes' ){
            $query = $this->active_profiles();

            if ( $query->have_posts() ) : 

                while ( $query->have_posts() ) : $query->the_post(); 
                    $id = get_the_ID();
                    $title = get_the_title();
                    $filtered_title = str_replace(' ','-',$title);
                    $icon = get_post_meta($id,'_uaio_icon',true);
                    $tab_display_status = get_post_meta($id,'_uaio_tab_display_status',true);
            
                    $display_status = $this->tab_display_status($tab_display_status,$params);
                    if($display_status){
                        $display .= '<div class="upme-'. $filtered_title .'-panel upme-profile-tab-panel" style="display:none;" >
                                    '.do_shortcode(get_the_content()).'       
                                </div>';
                    }
                    

                endwhile;
            wp_reset_postdata(); 

            endif;
        }
        
        return $display;
        
    }
    
    public function tab_display_status($tab_display_status,$params){
        extract($params);
        $display_status = false;
        if($tab_display_status == '1' && is_user_logged_in()){
            $display_status = true;
        }else if($tab_display_status == '2' && is_user_logged_in() && $id == get_current_user_id() ){
            $display_status = true;
        }else{
            $display_status = true;
        }
        
        return $display_status;
    }
    

}


