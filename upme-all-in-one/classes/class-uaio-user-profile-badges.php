<?php

class UAIO_User_Profile_Badges{
    
    public $uaio;

    public function __construct(){
        add_action('init',array($this,'register_user_badges'));
        //add_action( 'admin_init', array($this,'add_user_badges_meta_box' ));
        //add_action( 'save_post', array($this,'save_user_badges_fields'),10,2);
    }
    
    public function register_user_badges(){
  
        register_post_type( UAIO_USER_BADGES_POST_TYPE ,
            array(
                'labels' => array(
                    'name'              => __('User Badges','uaio'),
                    'singular_name'     => __('User Badge','uaio'),
                    'add_new'           => __('Add New','uaio'),
                    'add_new_item'      => __('Add New User Badge','uaio'),
                    'edit'              => __('Edit','uaio'),
                    'edit_item'         => __('Edit User Badge','uaio'),
                    'new_item'          => __('New User Badge','uaio'),
                    'view'              => __('View','uaio'),
                    'view_item'         => __('View User Badge','uaio'),
                    'search_items'      => __('Search User Badge','uaio'),
                    'not_found'         => __('No User Badge found','uaio'),
                    'not_found_in_trash' => __('No User Badge found in Trash','uaio'),
                ),

                'public' => true,
                'menu_position' => 100,
                'supports' => array( 'title', 'editor', 'thumbnail'),
                'has_archive' => true
            )
        );

    }
    
    public function add_user_badges_meta_box(){
        add_meta_box( 'user_badges_meta_box', __('User Badge Details','uaio'), array($this,'display_user_badge_fields'), UAIO_USER_BADGES_POST_TYPE , 'normal', 'high');
    }
    
    public function display_user_badge_fields($tab){
        global $user_badge_data;
        ob_start();    
    
        uaio_get_template('user-badges');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    public function save_profile_tab_fields($id, $post) { }
}


