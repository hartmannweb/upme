<?php

class UAIO_Registration_Forms{
    
    public $uaio;
    public $reg_form_query;

    public function __construct(){
        add_action('init',array($this,'register_registration_forms'));
        add_action( 'admin_init', array($this,'add_registration_forms_meta_box' ));
        add_action( 'save_post', array($this,'save_registration_forms_fields'),10,2);   
        
        add_filter('manage_edit-' . UAIO_USER_REG_FORMS_POST_TYPE . '_columns', array($this,'registration_forms_custom_columns'));
        add_action( 'manage_' . UAIO_USER_REG_FORMS_POST_TYPE . '_posts_custom_column', array($this,'registration_forms_custom_column_values'), 10, 2 );
        
        add_shortcode('uaio_reg_form', array($this,'process_reg_form_shortcode'));
        
        add_action('init',array($this,'init_registration_forms'));
//        $this->reg_form_query =  $this->init_registration_forms();
        
        add_filter( 'upme_custom_registration_fields', array($this,'custom_registration_fields'),10,2);
        
        add_filter( 'upme_register_before_head', array($this,'register_before_head'),10,2);
        add_filter( 'upme_registration_head', array($this,'registration_head'),10,2);
        add_filter( 'upme_register_after_head', array($this,'register_after_head'),10,2);
        add_filter( 'upme_register_after_fields', array($this,'register_after_fields'),10,2);
        add_filter( 'upme_profile_head_default_message',array($this,'register_head_default_message'),10,2);
    }
    
    public function register_registration_forms(){
 
        register_post_type( UAIO_USER_REG_FORMS_POST_TYPE,
            array(
                'labels' => array(
                    'name'              => __('Registration Forms','uaio'),
                    'singular_name'     => __('Registration Form','uaio'),
                    'add_new'           => __('Add New','uaio'),
                    'add_new_item'      => __('Add New Registration Form','uaio'),
                    'edit'              => __('Edit','uaio'),
                    'edit_item'         => __('Edit Registration Form','uaio'),
                    'new_item'          => __('New Registration Form','uaio'),
                    'view'              => __('View','uaio'),
                    'view_item'         => __('View Registration Form','uaio'),
                    'search_items'      => __('Search Registration Form','uaio'),
                    'not_found'         => __('No Registration Form found','uaio'),
                    'not_found_in_trash' => __('No Registration Form found in Trash','uaio'),
                ),

                'public' => true,
                'menu_position' => 100,
                'supports' => array( 'title'),
                'has_archive' => true
            )
        );

    }
    
    public function add_registration_forms_meta_box(){
        add_meta_box( 'registration_forms_meta_box', __('Registration Form Details','uaio'), array($this,'display_registration_form_info'), UAIO_USER_REG_FORMS_POST_TYPE, 'normal', 'high');
        
        add_meta_box( 'registration_forms_field_meta_box', __('Registration Form Fields','uaio'), array($this,'display_registration_form_fields'), UAIO_USER_REG_FORMS_POST_TYPE, 'normal', 'high');
        
        add_meta_box( 'registration_forms_contents_meta_box', __('Registration Form Contents','uaio'), array($this,'display_registration_form_contents'), UAIO_USER_REG_FORMS_POST_TYPE, 'normal', 'high');
    }
    
    public function display_registration_form_info($reg_form){
        global $registration_forms_data,$uaio,$upme_roles;
        ob_start();    
        
        $registration_forms_data['uaio_registration_form_name']  = get_post_meta( $reg_form->ID, '_uaio_registration_form_name', true );
        $registration_forms_data['uaio_redirect_url']   = get_post_meta( $reg_form->ID, '_uaio_redirect_url', true );
        $registration_forms_data['uaio_captcha']        = get_post_meta( $reg_form->ID, '_uaio_captcha', true );  
        $registration_forms_data['uaio_user_role']      = get_post_meta( $reg_form->ID, '_uaio_user_role', true );
        $registration_forms_data['uaio_display_login']  = get_post_meta( $reg_form->ID, '_uaio_display_login', true );
        $registration_forms_data['user_roles']          = $upme_roles->upme_get_available_user_roles();
        
        if(isset($registration_forms_data['user_roles']['administrator'])){
           unset($registration_forms_data['user_roles']['administrator']); 
        }

        $uaio->template_loader->get_template_part('registration-forms');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    public function display_registration_form_fields($reg_form){
        global $registration_forms_data,$uaio,$upme_roles;
        ob_start();    
        
        /* Get end of array */
        $upme_fields = get_option('upme_profile_fields');
        $reg_form_fields = (array) get_post_meta(  $reg_form->ID, '_uaio_reg_form_fields', true );
//        echo "<pre>";print_r($reg_form_fields);exit;
        $reg_fields = array();
        $upme_fields_meta = array();
        
        foreach($upme_fields as $k => $field){
            
            if (!isset($deleted))
                    $deleted = 0;

            if (!isset($private))
                $private = 0;

            if (!isset($required))
                $required = 0;
            
            if($deleted == 0 && $private == 0 && isset($field['show_in_register']) && $field['show_in_register'] == '1'){
                $reg_fields[$field['meta']] = $field['name'];
            }
            
        }
        
        $registration_forms_data['reg_fields']          = $reg_fields;
        $registration_forms_data['reg_form_fields']     = $reg_form_fields;
        
        $uaio->template_loader->get_template_part('registration-forms-fields');
        
        $display = ob_get_clean();
        echo $display;
    }
    
    public function display_registration_form_contents($reg_form){
        global $registration_forms_data,$uaio,$upme_roles;
        ob_start();    
        
        $registration_forms_data['reg_content_before_head']     = get_post_meta( $reg_form->ID, '_uaio_reg_content_before_head', true );
        $registration_forms_data['reg_content_head']            = get_post_meta( $reg_form->ID, '_uaio_reg_content_head', true );
        $registration_forms_data['reg_content_after_head']      = get_post_meta( $reg_form->ID, '_uaio_reg_content_after_head', true );
        $registration_forms_data['reg_content_after_fields']    = get_post_meta( $reg_form->ID, '_uaio_reg_content_after_fields', true );
        $registration_forms_data['reg_content_header_title_message']    = get_post_meta( $reg_form->ID, '_uaio_reg_content_header_title_message', true );

        $uaio->template_loader->get_template_part('registration-forms-contents');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    public function save_registration_forms_fields($reg_form_id, $reg_form) {
        
        $is_autosave = wp_is_post_autosave( $reg_form_id );
        $is_revision = wp_is_post_revision( $reg_form_id );
        $is_valid_nonce = ( isset( $_POST[ 'uaio_register_form_fields_nonce' ] ) && wp_verify_nonce( $_POST[ 'uaio_register_form_fields_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
        // Exits script depending on save status
        if ( !$is_valid_nonce ) {
            return;
        }
        
        if ( !current_user_can( 'manage_options') )
            return $reg_form_id;
        
        
        $uaio_redirect_url = isset( $_POST['uaio-registration-redirect'] ) ? $_POST['uaio-registration-redirect'] : '';
        $uaio_registration_captcha = isset( $_POST['uaio-registration-captcha'] ) ? $_POST['uaio-registration-captcha'] : '';
        
        $uaio_registration_user_role = isset( $_POST['uaio-registration-user-role'] ) ? $_POST['uaio-registration-user-role'] : '';
        if($uaio_registration_user_role == '0'){
            $uaio_registration_user_role = get_option('default_role');
        }
        
        $uaio_registration_display_login = isset( $_POST['uaio-registration-display-login'] ) ? $_POST['uaio-registration-display-login'] : '';
        $uaio_registration_form_name = isset( $_POST['uaio-registration-form-name'] ) ? $_POST['uaio-registration-form-name'] : '';
        if($uaio_registration_form_name == ''){
            $uaio_registration_form_name = wp_generate_password(12,false);
        }
        
        $uaio_registration_fields_hidden = isset( $_POST['uaio-registration-fields-hidden'] ) ? $_POST['uaio-registration-fields-hidden'] : '';
        $uaio_registration_fields_hidden = substr($uaio_registration_fields_hidden,0,-1);
        $uaio_registration_fields_hidden = array_filter(explode(',',$uaio_registration_fields_hidden));
        
//        echo "<pre>";print_r($uaio_registration_fields_hidden);exit;
        
        // Registration form content fields
        $reg_form_before_head   = isset( $_POST['uaio-reg-form-before-head'] ) ? $_POST['uaio-reg-form-before-head'] : '';
        $reg_form_head          = isset( $_POST['uaio-reg-form-head'] ) ? $_POST['uaio-reg-form-head'] : '';
        $reg_form_after_head    = isset( $_POST['uaio-reg-form-after-head'] ) ? $_POST['uaio-reg-form-after-head'] : '';
        $reg_form_after_field   = isset( $_POST['uaio-reg-form-after-fields'] ) ? $_POST['uaio-reg-form-after-fields'] : '';
        $reg_content_header_title_message   = isset( $_POST['uaio-reg-form-header-title-message'] ) ? $_POST['uaio-reg-form-header-title-message'] : '';

    
        if ( $reg_form->post_type == UAIO_USER_REG_FORMS_POST_TYPE ) {

            update_post_meta( $reg_form_id, '_uaio_redirect_url',  $uaio_redirect_url );
            update_post_meta( $reg_form_id, '_uaio_registration_form_name',  $uaio_registration_form_name );
            update_post_meta( $reg_form_id, '_uaio_captcha',  $uaio_registration_captcha );
            update_post_meta( $reg_form_id, '_uaio_user_role',  $uaio_registration_user_role );
            update_post_meta( $reg_form_id, '_uaio_display_login', $uaio_registration_display_login );
            
            if(is_array($uaio_registration_fields_hidden) && count($uaio_registration_fields_hidden) != 0){
                update_post_meta( $reg_form_id, '_uaio_reg_form_fields', $uaio_registration_fields_hidden );
            }
            
            update_post_meta( $reg_form_id, '_uaio_reg_content_before_head', $reg_form_before_head );
            update_post_meta( $reg_form_id, '_uaio_reg_content_head', $reg_form_head );
            update_post_meta( $reg_form_id, '_uaio_reg_content_after_head', $reg_form_after_head );
            update_post_meta( $reg_form_id, '_uaio_reg_content_after_fields', $reg_form_after_field );
            update_post_meta( $reg_form_id, '_uaio_reg_content_header_title_message', $reg_content_header_title_message );
            

        }
    }
    
    function registration_forms_custom_columns( $columns ) {

        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Title','uaio'),
            'uaio_reg_shortcode' => __( 'Registration Form Shortcode','uaio' ),
            'date' => __( 'Dates','uaio' )
        );

        return $columns;
    }

    function registration_forms_custom_column_values($column, $post_id ) {
        global $post;

	    switch( $column ) {

		  /* If displaying the 'duration' column. */
		  case 'uaio_reg_shortcode' :
                echo "[uaio_reg_form id='". $post_id ."' ]";
                break;

		

		  /* Just break out of the switch statement for everything else. */
		  default :
			break;
	   }
        
    }
    
    public function process_reg_form_shortcode($atts,$content){
        extract(shortcode_atts(array(
		'id'	=>	'0',
     	), $atts));
        
        $redirect_url   = get_post_meta( $id, '_uaio_redirect_url', true );
        $captcha        = get_post_meta( $id, '_uaio_captcha', true );  
        $user_role      = get_post_meta( $id, '_uaio_user_role', true );
        $display_login  = get_post_meta( $id, '_uaio_display_login', true );
        
        $sh_attributes = '';
        
        $sh_attributes  .= ' name="' . get_post_meta( $id, '_uaio_registration_form_name', true ) . '" ';
        
        
        if($redirect_url != ''){
            $sh_attributes  .= ' redirect_to="' . $redirect_url . '" ';
        }        
        
        if($captcha == '0'){
            $captcha = 'no';
        }
        $sh_attributes  .= ' captcha="' . $captcha . '" ';       
        $sh_attributes  .= ' user_role="' . $user_role . '" ';        
        
        if($display_login == '1'){
            $sh_attributes  .= ' display_login ="yes" ';
        }
        
//        echo $sh_attributes;
        return do_shortcode('[upme_registration '.$sh_attributes.' ]');
        
        
        
    }
    
    public function custom_registration_fields($fields , $params){       

        if ($this->reg_form_query->have_posts()) {
            while ($this->reg_form_query->have_posts()) : $this->reg_form_query->the_post();
                $form_id = get_the_ID();
                if($params['form_name'] == get_post_meta( $form_id, '_uaio_registration_form_name', true )){
                    $reg_form_fields = (array) get_post_meta(  $form_id, '_uaio_reg_form_fields', true );
                    $filter_fields = array('type' => 'allow', 'fields' => $reg_form_fields);
                    $fields = $this->traverse_registration_fields( $fields, $filter_fields);
                }       

            endwhile;
            wp_reset_query();
        }

        return $fields;
    }

 
    public function traverse_registration_fields($fields, $filter_fields){
        foreach ($fields as $key => $field) {
            switch ($filter_fields['type']) {
                case 'allow':
                    if(! in_array($field['meta'], $filter_fields['fields'])) {
                    unset($fields[$key]);
                    }
                    break;
                case 'remove':
                    if(in_array($field['meta'], $filter_fields['fields'])) {
                    unset($fields[$key]);
                    }
                    break;
            }
        }
        return $fields;
    }
    
    public function init_registration_forms(){
        $args = array(
                'order' => 'DESC',
                'orderby' => 'date',
                'posts_per_page' => -1,
                'post_type' => UAIO_USER_REG_FORMS_POST_TYPE,
                'post_status'=>'publish',
            );

        $query = new WP_Query($args);
        $this->reg_form_query = $query;
        
    }
    
    public function register_before_head($display,$params){
        extract($params);       
            
        if ($this->reg_form_query->have_posts()) {
            while ($this->reg_form_query->have_posts()) : $this->reg_form_query->the_post();
                $form_id = get_the_ID();
                if($params['name'] == get_post_meta( $form_id, '_uaio_registration_form_name', true )){
                    
                    $display .= get_post_meta(  $form_id, '_uaio_reg_content_before_head', true );
                }       

            endwhile;
            wp_reset_query();
        }
        
        return $display;
    }
    
    public function registration_head($display,$params){
        extract($params);
        
        if ($this->reg_form_query->have_posts()) {
            while ($this->reg_form_query->have_posts()) : $this->reg_form_query->the_post();
                $form_id = get_the_ID();
                if($params['name'] == get_post_meta( $form_id, '_uaio_registration_form_name', true )){
                    $display .= get_post_meta(  $form_id, '_uaio_reg_content_head', true );
                }       

            endwhile;
            wp_reset_query();
        }
        
        return $display;
        
    }
    
    public function register_after_head($display,$params){
        extract($params);
        
        if ($this->reg_form_query->have_posts()) {
            
            while ($this->reg_form_query->have_posts()) : $this->reg_form_query->the_post();
//            echo $params['name'];exit;
                $form_id = get_the_ID();
                if($params['name'] == get_post_meta( $form_id, '_uaio_registration_form_name', true )){
                    $display .= get_post_meta(  $form_id, '_uaio_reg_content_after_head', true );
                }       

            endwhile;
            wp_reset_query();
        }
        
        return $display;
        
    }
    
    public function register_after_fields($display,$params){
        extract($params);
        
        if ($this->reg_form_query->have_posts()) {
            while ($this->reg_form_query->have_posts()) : $this->reg_form_query->the_post();
                $form_id = get_the_ID();
                if($params['name'] == get_post_meta( $form_id, '_uaio_registration_form_name', true )){
                    $display .= get_post_meta(  $form_id, '_uaio_reg_content_after_fields', true );
                }       

            endwhile;
            wp_reset_query();
        }
        
        return $display;
        
    } 
        
    public function register_head_default_message($display,$params){
        extract($params);
        
        if ($this->reg_form_query->have_posts()) {
            while ($this->reg_form_query->have_posts()) : $this->reg_form_query->the_post();
                $form_id = get_the_ID();
                if($params['name'] == get_post_meta( $form_id, '_uaio_registration_form_name', true )){
                    $display = get_post_meta(  $form_id, '_uaio_reg_content_header_title_message', true );
                }       

            endwhile;
            wp_reset_query();
        }
        
        return $display;
        
    }
    
}