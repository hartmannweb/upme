<?php

class UAIO_Users{
    
    public function get_active_users(){
        
        $users_query = new WP_User_Query( array( 
                'meta_query' => array(
                    'relation' => 'AND',
                    0 => array(
                        'key'     => 'upme_user_profile_status',
                        'value'   => 'ACTIVE',
                        'compare' => '='
                    ),
                    1 => array(
                        'key'     => 'upme_approval_status',
                        'value'   => 'ACTIVE',
                        'compare' => '='
                    ),
                    2 => array(
                        'key'     => 'upme_activation_status',
                        'value'   => 'ACTIVE',
                        'compare' => '='
                    )
                )
                ) );
        $results = $users_query->get_results();
        return $results;
        
    }
}

