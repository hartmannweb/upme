<?php

/**
 * Manage functionality of file upload field types
 */
class UAIO_File_Manager{

	/**
	 * Intialize ations and filters for file uplod fields
	 */
	public function __construct(){
		add_filter( 'uaio_edit_non_image_fileupload_fields', array( $this,'uaio_edit_non_image_fileupload_fields'),10,2 );
		add_filter( 'uaio_view_non_image_fileupload_fields', array( $this,'uaio_view_non_image_fileupload_fields'),10,2 );
		add_filter('upme_custom_file_field_types', array($this, 'uaio_custom_file_field_types' ), 10,2 );
		add_action('init', array( $this, 'uaio_file_download'));


	}

	/**
	 * Define file types for file upload fields
	 *
	 * @param array 	$input_types exisitng input types array of UPME
	 * @param array 	$params dynamic parameters
	 * @return array 	$input_types Updated input types array of UPME
	 */
	public function uaio_custom_file_field_types($input_types, $params){
		global $uaio_field_type_manager;
		array_push($input_types, 'pdf');
		array_push($input_types, 'msword');
		array_push($input_types, 'mspowerpoint');
		array_push($input_types, 'msexcel');
		array_push($input_types, 'textDocument');
		array_push($input_types, 'spreadsheet');
		array_push($input_types, 'presentation');
		array_push($input_types, 'mp3');

		array_push($uaio_field_type_manager->file_field_types, 'pdf');
		array_push($uaio_field_type_manager->file_field_types, 'msword');
		array_push($uaio_field_type_manager->file_field_types, 'mspowerpoint');
		array_push($uaio_field_type_manager->file_field_types, 'msexcel');
		array_push($uaio_field_type_manager->file_field_types, 'textDocument');
		array_push($uaio_field_type_manager->file_field_types, 'spreadsheet');
		array_push($uaio_field_type_manager->file_field_types, 'presentation');
		array_push($uaio_field_type_manager->file_field_types, 'mp3');
		return $input_types;
	}

	/**
	 * Display uploaded file and delete in edit screen
	 *
	 * @param string 	$display exisitng display value for this field through other filters. By default, it will be empty
	 * @param array 	$params dynamic parameters(user_id,meta,field,delete_link)
	 * @return string  	$display HTML for download and delete links for this file.
	 */
	public function uaio_edit_non_image_fileupload_fields($display,$params){
	    global $uaio_field_type_manager;

	    $id = $params['user_id'];
		$meta = $params['meta'];
		$field = $params['field'];
		$display_delete_link = $params['delete_link'];

	    $upme_settings = get_option('upme_options');
		$profile_page_id = (int) isset($upme_settings['profile_page_id']) ? $upme_settings['profile_page_id'] : 0;
		if ($profile_page_id) {
			$url = get_permalink($profile_page_id);

		}else{
			$url = $_SERVER['REQUEST_URI'];
		
		}

		$url = upme_add_query_string($url,'upme_file_download=yes');
    	$url = upme_add_query_string($url,'upme_file_owner='.$id);
    	$url = upme_add_query_string($url,'upme_file_field='.$meta);
    	$url = upme_add_query_string($url,'upme_file_field_type='.$field);

		if(in_array($field, $uaio_field_type_manager->file_field_types)){
			$display = '<div class="upme-note"><a href="' . $url . '" alt="" > '. __('Download File','uaio'). ' </a>' . $display_delete_link . '</div>';
	                                        
		}
		return $display;
	}

	/**
	 * Display uploaded file and delete in view screen
	 *
	 * @param string 	$display exisitng display value for this field through other filters. By default, it will be empty
	 * @param array 	$params dynamic parameters(user_id,meta,field)
	 * @return string  	$display HTML for download links for this file.
	 */
	public function uaio_view_non_image_fileupload_fields($display,$params){
		global $upme,$uaio_field_type_manager;

		$id = $params['user_id'];
		$meta = $params['meta'];
		$field = $params['field'];

		if(in_array($field, $uaio_field_type_manager->file_field_types )){

			//if($upme->can_edit_profile($upme->logged_in_user,$id)){

				$upme_settings = get_option('upme_options');
				$profile_page_id = (int) isset($upme_settings['profile_page_id']) ? $upme_settings['profile_page_id'] : 0;
				if ($profile_page_id) {
	        		$url = get_permalink($profile_page_id);

	    		}else{
	    			$url = $_SERVER['REQUEST_URI'];
	    		
	    		}

	    		$url = upme_add_query_string($url,'upme_file_download=yes');
	        	$url = upme_add_query_string($url,'upme_file_owner='.$id);
	        	$url = upme_add_query_string($url,'upme_file_field='.$meta);
	        	$url = upme_add_query_string($url,'upme_file_field_type='.$field);

				$display = '<a href="' . $url . '" alt="" > '. __('Download File','uaio') .' </a>';
	        
			// }else{
			// 	$display = '-';        
			// }
			                                
		}
		return $display;
	}

	/**
	 * Download the file to user
	 */
	public function uaio_file_download(){
		if(isset($_GET['upme_file_download']) && $_GET['upme_file_download'] =='yes'){
			$upme_file_owner = $_GET['upme_file_owner'];
			$upme_file_field = $_GET['upme_file_field'];
			$upme_file_field_type = $_GET['upme_file_field_type'];

			$file_link = get_the_author_meta($upme_file_field, $upme_file_owner);

			$upload_dir = wp_upload_dir(); 
			$file_dir =  str_replace($upload_dir['baseurl'], $upload_dir['basedir'], $file_link);


			header('Cache-Control: public');
			header('Content-Description: File Transfer');
			header('Content-disposition: attachment;filename='.basename($file_dir));

			$extension = explode('.',$file_link);
			$extension = end($extension);
			$extension = strtolower($extension);

			switch($upme_file_field_type){
				case 'pdf':
					header('Content-Type: application/pdf');
					break;
				case 'doc':
					header('Content-Type:application/msword');
					break;
				case 'docx':
					header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
					break;
				case 'odt':
					header('Content-Type: application/vnd.oasis.opendocument.text');
					break;
				case 'xls':
					header('Content-Type: application/excel');
					break;
				case 'xlsx':
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					break;	
				case 'ods':
					header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');
					break;					
				case 'ppt':
					header('Content-Type: application/vnd.ms-powerpoint');
					break;
				case 'pptx':
					header('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
					break;	
				case 'odp':
					header('Content-Type: application/vnd.oasis.opendocument.presentation');
					break;					
				case 'txt':
					header('Content-Type: text/plain');
					break;
				case 'mp3':
					header('Content-Type: audio/mpeg');
					break;
			}
			
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: '. filesize($file_dir));
			readfile($file_dir);
			exit;
		}
	}

}

$uaio_file_manager = new UAIO_File_Manager();