<?php
/**
 * Manage functionality for File upload field extensions
 */
class UAIO_File_Extension_Manager{

	/**
	 * Initialize actions and filters for file upload extensions
	 */
	public function __construct(){

		add_action('upme_after_upload_profile_files', array($this,'uaio_after_upload_profile_files' ),10,3);
		add_action('upme_before_upload_profile_files', array($this, 'uaio_before_upload_profile_files' ),10,3);
        add_action('upme_register_before_upload_profile_files', array($this, 'uaio_before_upload_profile_files' ),10,3);
	}

	/**
	 * Remove restricted file types for each field after uploading
	 *
	 * @param array 	$userid user Id for the current user profile
	 * @param array 	$clean_key meta key of the field
	 * @param array 	$params dynamic parameters
	 * @return void 	-
	 */
	public function uaio_after_upload_profile_files($userid, $clean_key, $params){
		global $upme_save;
		$upme_save->allowed_extensions 	= array('image/gif', 'image/jpeg', 'image/png'); 
		$upme_save->allowed_exts = array('gif','png','jpeg','jpg');
	}

	/**
	 * Add file type restrictions for each field before uploading
	 *
	 * @param array 	$userid user Id for the current user profile
	 * @param array 	$clean_key meta key of the field
	 * @param array 	$params dynamic parameters
	 * @return void 	-
	 */	
	public function uaio_before_upload_profile_files($userid, $clean_key, $params){
		global $upme_save;

		$field = $upme_save->upme_fileds_meta_type_array[$clean_key];

		switch ($field) {
			case 'pdf':
				$upme_save->allowed_extensions 	= array('application/pdf');        
			 	$upme_save->allowed_exts 		= array('pdf');
				break;

			case 'msword':
				$upme_save->allowed_extensions 	= array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document');        
			 	$upme_save->allowed_exts 		= array('doc','docx');
				break;

			case 'mspowerpoint':
				$upme_save->allowed_extensions 	= array('application/vnd.ms-powerpoint','application/mspowerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation');        
			 	$upme_save->allowed_exts 		= array('ppt','pptx');
				break;

			case 'msexcel':
				$upme_save->allowed_extensions 	= array('application/excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');        
			 	$upme_save->allowed_exts 		= array('xls','xlsx');
				break;

			case 'textDocument':
				$upme_save->allowed_extensions 	= array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','text/plain','application/vnd.oasis.opendocument.text');        
			 	$upme_save->allowed_exts 		= array('doc','docx','txt','odt');
				break;

			case 'spreadsheet':
				$upme_save->allowed_extensions 	= array('application/excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.oasis.opendocument.spreadsheet');        
			 	$upme_save->allowed_exts 		= array('xls','xlsx','ods');
				break;

			case 'presentation':
				$upme_save->allowed_extensions 	= array('application/vnd.ms-powerpoint','application/mspowerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/vnd.oasis.opendocument.presentation');        
			 	$upme_save->allowed_exts 		= array('ppt','pptx','odp');
				break;

			case 'mp3':
				$upme_save->allowed_extensions 	= array('audio/mpeg','audio/mp3');        
			 	$upme_save->allowed_exts 		= array('mp3');
				break;
			
			default:
				# code...
				break;
		}
	}

}

$uaio_file_extenion_manager = new UAIO_File_Extension_Manager();