<?php

class UAIO_Profile_Forms{
    
    public $uaio;

    public function __construct(){
        add_action('init',array($this,'register_user_profile'));
        add_action( 'admin_init', array($this,'add_user_profile_meta_box' ));
        add_action( 'save_post', array($this,'save_user_profile_fields'),10,2);   
        
        add_filter('manage_edit-' . UAIO_USER_USER_PROFILE_POST_TYPE . '_columns', array($this,'user_profile_custom_columns'));
        add_action( 'manage_' . UAIO_USER_USER_PROFILE_POST_TYPE . '_posts_custom_column', array( $this,'user_profile_custom_column_values'), 10, 2 );
       
        add_shortcode('uaio_profile_form', array($this,'process_profile_form_shortcode'));
    }
    
    public function register_user_profile(){
  
        register_post_type( UAIO_USER_USER_PROFILE_POST_TYPE,
            array(
                'labels' => array(
                    'name'              => __('User Profiles','uaio'),
                    'singular_name'     => __('User Profile','uaio'),
                    'add_new'           => __('Add New','uaio'),
                    'add_new_item'      => __('Add New User Profile','uaio'),
                    'edit'              => __('Edit','uaio'),
                    'edit_item'         => __('Edit User Profile','uaio'),
                    'new_item'          => __('New User Profile','uaio'),
                    'view'              => __('View','uaio'),
                    'view_item'         => __('View User Profile','uaio'),
                    'search_items'      => __('Search User Profile','uaio'),
                    'not_found'         => __('No User Profile found','uaio'),
                    'not_found_in_trash' => __('No User Profile found in Trash','uaio'),
                ),

                'public' => true,
                'menu_position' => 100,
                'supports' => array( 'title'),
                'has_archive' => true
            )
        );

    }
    
    public function add_user_profile_meta_box(){
        add_meta_box( 'user_profile_meta_box', __('User Profile Details','uaio'), array($this,'display_user_profile_info'), UAIO_USER_USER_PROFILE_POST_TYPE, 'normal', 'high');
       // add_meta_box( 'user_profile_field_meta_box', __('User Profile Fields','uaio'), array($this,'display_user_profile_fields'), 'user_profile', 'normal', 'high');
    }
    
    public function display_user_profile_info($profile_form){
        global $user_profile_data,$uaio,$upme_roles;
        ob_start();    
        
        $user_profile_data['profile_form_name']          =    get_post_meta( $profile_form->ID, '_uaio_profile_form_name',  true );
        $user_profile_data['profile_user']               =    get_post_meta( $profile_form->ID, '_uaio_profile_user',  true );
        $user_profile_data['profile_view_type']          =    get_post_meta( $profile_form->ID, '_uaio_profile_view_type',  true );
        $user_profile_data['profile_fields']             =    get_post_meta( $profile_form->ID, '_uaio_profile_fields',  true );
        $user_profile_data['display_user_id']            =    get_post_meta( $profile_form->ID, '_uaio_display_user_id', true );
        $user_profile_data['display_profile_status']     =    get_post_meta( $profile_form->ID, '_uaio_display_profile_status', true );
        $user_profile_data['use_in_sidebar']             =    get_post_meta( $profile_form->ID, '_uaio_use_in_sidebar', true );
        $user_profile_data['display_profile_stats']      =    get_post_meta( $profile_form->ID, '_uaio_display_profile_stats', true );
        $user_profile_data['display_social_bar']         =    get_post_meta( $profile_form->ID, '_uaio_display_social_bar', true );
        $user_profile_data['display_user_role']          =    get_post_meta( $profile_form->ID, '_uaio_display_user_role', true );
        $user_profile_data['logout_redirect']            =    get_post_meta( $profile_form->ID, '_uaio_profile_logout_redirect', true );
        
        $user_profile_data['user_profiles']   = $uaio->users->get_active_users();
        $user_profile_data['user_profile_fields']   = get_option('upme_profile_fields');
        $uaio->template_loader->get_template_part('user-profiles');
        
        $display = ob_get_clean();
        echo $display; 
    }
    
    public function save_user_profile_fields($profile_form_id, $profile_form) {
        
        $is_autosave = wp_is_post_autosave( $profile_form_id );
        $is_revision = wp_is_post_revision( $profile_form_id );
        $is_valid_nonce = ( isset( $_POST[ 'uaio_user_profiles_fields_nonce' ] ) && wp_verify_nonce( $_POST[ 'uaio_user_profiles_fields_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
        // Exits script depending on save status
        if ( !$is_valid_nonce ) {
            return;
        }
        
        if ( !current_user_can( 'manage_options') )
            return $profile_form_id;
        
        
        $uaio_redirect_url = isset( $_POST['uaio-registration-redirect'] ) ? $_POST['uaio-registration-redirect'] : '';
        
        
        $profile_form_name      = isset( $_POST['uaio-user-profile-name'] ) ? $_POST['uaio-user-profile-name'] : '';
        $profile_user           = isset( $_POST['uaio-profile-user'] ) ? $_POST['uaio-profile-user'] : '';
        $profile_view_type      = isset( $_POST['uaio-profile-view-type'] ) ? $_POST['uaio-profile-view-type'] : '';
        $profile_fields         = isset( $_POST['uaio-profile-fields'] ) ? $_POST['uaio-profile-fields'] : '';
        $display_user_id        = isset( $_POST['uaio-display-user-id'] ) ? $_POST['uaio-display-user-id'] : '';
        $display_profile_status = isset( $_POST['uaio-display-profile-status'] ) ? $_POST['uaio-display-profile-status'] : '';
        $display_profile_stats  = isset( $_POST['uaio-display-profile-stats'] ) ? $_POST['uaio-display-profile-stats'] : '';
        $display_social_bar     = isset( $_POST['uaio-display-social-bar'] ) ? $_POST['uaio-display-social-bar'] : '';
        $display_user_role      = isset( $_POST['uaio-display-user-role'] ) ? $_POST['uaio-display-user-role'] : '';
        $logout_redirect        = isset( $_POST['uaio-logout-redirect'] ) ? $_POST['uaio-logout-redirect'] : '';
        $use_in_sidebar         = isset( $_POST['uaio-use-in-sidebar'] ) ? $_POST['uaio-use-in-sidebar'] : '';
        

        if($profile_form_name == ''){
            $profile_form_name = wp_generate_password(12,false);
        }
        
        if ( $profile_form->post_type == UAIO_USER_USER_PROFILE_POST_TYPE ) {
            //echo "<pre>";print_r($profile_fields);exit;
            update_post_meta( $profile_form_id, '_uaio_profile_form_name',  $profile_form_name );
            update_post_meta( $profile_form_id, '_uaio_profile_user',  $profile_user );
            update_post_meta( $profile_form_id, '_uaio_profile_view_type',  $profile_view_type );
            update_post_meta( $profile_form_id, '_uaio_profile_fields',  $profile_fields );
            update_post_meta( $profile_form_id, '_uaio_display_user_id', $display_user_id );
            update_post_meta( $profile_form_id, '_uaio_display_profile_status', $display_profile_status );
            update_post_meta( $profile_form_id, '_uaio_display_profile_stats', $display_profile_stats );
            update_post_meta( $profile_form_id, '_uaio_display_social_bar', $display_social_bar );
            update_post_meta( $profile_form_id, '_uaio_display_user_role', $display_user_role );
            update_post_meta( $profile_form_id, '_uaio_profile_logout_redirect', $logout_redirect );
            update_post_meta( $profile_form_id, '_uaio_use_in_sidebar', $use_in_sidebar );
        }
    }
    
    function user_profile_custom_columns( $columns ) {

        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Title','uaio'),
            'uaio_user_profile_shortcode' => __( 'User Profile Form Shortcode','uaio' ),
            'date' => __( 'Dates','uaio' )
        );

        return $columns;
    }

    function user_profile_custom_column_values($column, $post_id ) {
        global $post;

	    switch( $column ) {

		  /* If displaying the 'duration' column. */
		  case 'uaio_user_profile_shortcode' :
                echo "[uaio_profile_form id='". $post_id ."' ]";
                break;

		

		  /* Just break out of the switch statement for everything else. */
		  default :
			break;
	   }
        
    }
    
    public function process_profile_form_shortcode($atts,$content){
        extract(shortcode_atts(array(
		'id'	=>	'0',
     	), $atts));
        
        $profile_form_name          =    get_post_meta( $id, '_uaio_profile_form_name',  true );
        $profile_user               =    get_post_meta( $id, '_uaio_profile_user',  true );
        $profile_view_type          =    get_post_meta( $id, '_uaio_profile_view_type',  true );
        $profile_fields             =    get_post_meta( $id, '_uaio_profile_fields',  true );
        $display_user_id            =    get_post_meta( $id, '_uaio_display_user_id', true );
        $display_profile_status     =    get_post_meta( $id, '_uaio_display_profile_status', true );
        $use_in_sidebar             =    get_post_meta( $id, '_uaio_use_in_sidebar', true );
        $display_profile_stats      =    get_post_meta( $id, '_uaio_display_profile_stats', true );
        $display_social_bar         =    get_post_meta( $id, '_uaio_display_social_bar', true );
        $display_user_role          =    get_post_meta( $id, '_uaio_display_user_role', true );
        $logout_redirect            =    get_post_meta( $id, '_uaio_profile_logout_redirect', true );
        

        $sh_attributes = '';
        
        $sh_attributes  .= ' name="' . $profile_form_name . '" ';
        
        
        if($profile_user != 'default'){
            $sh_attributes  .= ' id="' . $profile_user . '" ';
        }        
        
        if($profile_view_type == 'compact'){
            $sh_attributes  .= ' view="compact" ';
        }else if($profile_view_type == 'fields'){
            $profile_fields = implode(',',$profile_fields);
            $sh_attributes  .= ' view="' . $profile_fields . '" ';
        } 
        
        $sh_attributes  .= ' show_id="' . $display_user_id . '" ';
        $sh_attributes  .= ' show_stats="' . $display_profile_stats . '" ';
        $sh_attributes  .= ' show_social_bar="' . $display_social_bar . '" ';
        
        if($use_in_sidebar == 'yes'){
            $sh_attributes  .= ' use_in_sidebar="' . $use_in_sidebar . '" ';
        }
        
        $sh_attributes  .= ' show_role="' . $display_user_role . '" ';
        $sh_attributes  .= ' show_profile_status="' . $display_profile_status . '" ';
        
        if(isset($display_login) && $display_login != ''){
            $sh_attributes  .= ' logout_redirect ="' . $logout_redirect . '" ';
        }
    
        return do_shortcode('[upme '.$sh_attributes.' ]');
    }
    
}