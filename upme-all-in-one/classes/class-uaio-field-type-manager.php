<?php
/**
 * Manage functionality for all the custom field types
 */
class UAIO_Field_Type_Manager{

	public	$file_field;
	public	$embedly_field;
	public	$wp_editor_field;
	public	$text_field;
	public	$date_field;

	public	$list_field;
	public	$google_maps_field;
	public	$post_field;
	public	$page_field;
	public	$chosen_select_field;
    
    public	$conditional_text_field;
    public	$user_badge_field;
    public	$mailchimp_field;

	/**
	 * Indialize actions, filters and data
	 */
	public function __construct(){
        
        define('UPME_BADGE_MAX_LIMIT',1);

		$this->current_option = get_option('upme_options');

		$this->uaio_initialize_objects();

		add_filter( 'upme_allowed_input_field_types',array($this,'uaio_allowed_input_field_types' ), 10, 2);
		add_filter( 'upme_include_validation_field_types', array($this,'uaio_include_validation_field_types' ) , 10, 2);
		add_filter( 'upme_user_defined_custom_field_types', array($this,'uaio_user_defined_custom_field_types' ), 10, 2); 

		add_filter( 'upme_registration_custom_field_types', array($this,'uaio_registration_custom_field_types' ),10, 2); 
		add_filter( 'upme_view_custom_field_type_input',  array($this,'uaio_view_custom_field_type_input' ), 10 ,2);
		add_filter( 'upme_backend_edit_custom_field_type_input', array($this,'uaio_backend_edit_custom_field_type_input' ), 10, 2);
	
		add_filter( 'upme_edit_custom_field_type_input', array($this,'uaio_edit_custom_field_type_input' ), 10, 2);
	
		add_filter( 'upme_backend_custom_field_type_restrictions',array($this,'uaio_backend_custom_field_type_restrictions'), 10,3);
        add_filter( 'upme_registration_custom_field_type_restrictions',array($this,'uaio_registration_custom_field_type_restrictions'), 10,2);
        add_filter( 'upme_frontend_custom_field_type_restrictions',array($this,'uaio_frontend_custom_field_type_restrictions'), 10,2);
        

		$this->file_field_types = array();	

		$this->upme_fileds_info_array = array();
		$this->profile_fields = get_option('upme_profile_fields');
		foreach ($this->profile_fields as $key => $value) {
            $this->upme_fileds_info_array[$value['meta']] = $value;
        }

        add_action('personal_options_update', array($this, 'uaio_save_user_extra_fields'), 9999);
        add_action('edit_user_profile_update', array($this, 'uaio_save_user_extra_fields'));

	}

	/**
	 * Intialize objects of custom field type classes
	 *
	 * @return void 	-
	 */
	public function uaio_initialize_objects(){
		$this->file_field 		= new UAIO_File_Field();
		$this->embedly_field 	= new UAIO_Embedly_Card_Field();
		$this->wp_editor_field  = new UAIO_WP_Editor();
		$this->text_field 		= new UAIO_Text_Field();
		$this->date_field 		= new UAIO_Date_Field();

		$this->list_field        = new UAIO_List_Field();
		$this->google_maps_field = new UAIO_Google_Maps_Field();
		$this->chosen_select_field   = new UAIO_Chosen_Field();
        
        $this->user_badge_field = new UAIO_User_Badge_Field();
        $this->mailchimp_field  = new UAIO_Mailchimp_Field();
	}

	/**
	 * Save custom fields in backend profile
	 *
	 * @param array 	$userid user Id for the current user profile
	 * @return void 	-
	 */
	public function uaio_save_user_extra_fields($user_id){
		global $upme_admin;
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Set date format from admin settings
            $upme_settings = get_option('upme_options');
            // Get profile fields
            $profile_fields = get_option('upme_profile_fields');
            // Get list of required fields
            $required_fields = array();
            $profile_fields_by_meta = array();

            foreach ($profile_fields as $key => $field) {
                $field_settings = $field;
                extract($field);

                if(isset($profile_fields[$key]['required']) && $profile_fields[$key]['required'] == '1'){
                    $required_fields[$profile_fields[$key]['meta']] = $field_settings;
                }
                
                $profile_fields_by_meta[$profile_fields[$key]['meta']] = $field_settings;
            }

            foreach ($_POST as $key => $value) {

            	$custom_save_status = false;
            	if(strpos($key, 'wp_editor_')  !== false ){            	
            		$key = str_replace('wp_editor_', '', $key);
            		$custom_save_status = true;
            	}
            	if(strpos($key, 'upme_multiple_chosen_')  !== false ){            	
            		$key = str_replace('upme_multiple_chosen_', '', $key);
            		$custom_save_status = true;

            		if (is_array($value)) { // checkboxes
                        $value = implode(', ', $value);
                    }
                }
                if(strpos($key, 'upme_user_badge_')  !== false ){            	
            		$key = str_replace('upme_user_badge_', '', $key);
            		$custom_save_status = true;
                    // echo $key;
                    if(is_array($value) && count(array_filter($value)) > UPME_BADGE_MAX_LIMIT ){
                        $upme_admin->errors[] = __($profile_fields_by_meta[$key]['name'],'upme'). __(' can have only '.UPME_BADGE_MAX_LIMIT.' badge(s).','upme');
                    }

            		if (is_array($value)) { // checkboxes
                        $value = implode(', ', $value);
                    }
                    
                    
              
                }

            	
            	if($custom_save_status){            	

	                // Validation for required fields
	                if (array_key_exists($key, $required_fields)) {
	                    switch ($required_fields[$key]['field']) {
	                        case 'wpEditor':                           
	                            if('' == trim($value)){
	                                $upme_admin->errors[] = __($required_fields[$key]['name'],'upme'). __(' is required.','upme');
	                            }
	                            break;
	                        case 'chosen_multiple':  
	                        	          //echo "<pre>";print_r('a'.$value.'b');exit;              
	                            if('' == trim($value)){
	                                $upme_admin->errors[] = __($required_fields[$key]['name'],'upme'). __(' is required.','upme');
	                            }	                            
	                            break;
                            case 'user_badge':  
	                        	          //echo "<pre>";print_r('a'.$value.'b');exit;              
	                            if('' == trim($value)){
	                                $upme_admin->errors[] = __($required_fields[$key]['name'],'upme'). __(' is required.','upme');
	                            }	                            
	                            break;
	                    }
	                    
	                }
                    
                    
	                //$this->errors[]

	                if(!(isset($upme_admin->errors) && count($upme_admin->errors) > 0)){                    

	                    /* UPME action for executing custom functionality before saving a field */
	                    do_action('upme_before_save_backend_field_'.$key,$key,$value,$user_id);
	                    // End Action

	                    // To Do Need to check for adding new meta when it was not same as old
	                    update_user_meta($user_id, $key, $value);

	                    /* UPME action for executing custom functionality after saving a field */
	                    do_action('upme_after_save_backend_field_'.$key,$key,$value,$user_id);
	                    // End Action
	                }
	            }
            }


            if(!(isset($upme_admin->errors) && count($upme_admin->errors) > 0)){                        
                upme_update_user_cache($user_id);
            }

        }
	}

	/**
	 * Add restrictions for each field type on backend
	 *
	 * @param array 	$errors exsitng errors 
	 * @param array 	$post  $_POST data for user request
	 * @param array 	$params dynamic parameters
	 * @return array 	errors for the custom fields
	 */
	public function uaio_backend_custom_field_type_restrictions($errors,$post,$params){

		$field = isset($params['field_settings']['field']) ? $params['field_settings']['field'] : '';
		$meta = isset( $params['field_settings']['meta']) ? $params['field_settings']['meta'] : '';
		$name = isset( $params['field_settings']['name']) ? $params['field_settings']['name'] : '';
		$value = isset( $post[$meta] ) ? $post[$meta] : '';
		$errors = $this->uaio_custom_field_type_validation($errors,$name,$meta,$field,$value);
		return $errors;
	}

	/**
	 * Add restrictions for each field type on frontend
	 *
	 * @param array 	$errors exsitng errors 
	 * @param array 	$params dynamic parameters
	 * @return array 	errors for the custom fields
	 */
	public function uaio_frontend_custom_field_type_restrictions($errors,$params){
		global $upme_save;
		extract($params);
		if(isset($upme_save->upme_fileds_meta_value_array[$meta])){
			$name = $upme_save->upme_fileds_meta_value_array[$meta];
			$field = $upme_save->upme_fileds_meta_type_array[$meta];
			$errors = $this->uaio_custom_field_type_validation($errors,$name,$meta,$field,$value);
		}

    	return $errors;
	}

	/**
	 * Add restrictions for each field type on registration
	 *
	 * @param array 	$errors exsitng errors 
	 * @param array 	$params dynamic parameters
	 * @return array 	errors for the custom fields
	 */
	public function uaio_registration_custom_field_type_restrictions($errors,$params){
		extract($params);

		if(isset($this->upme_fileds_info_array[$meta])){
			$name = $this->upme_fileds_info_array[$meta]['name'];
			$field = $this->upme_fileds_info_array[$meta]['field'];
			$errors = $this->uaio_custom_field_type_validation($errors,$name,$meta,$field,$value);
		}

		
		return $errors;
	}

	/**
	 * Execute mandatory validations for custom field types
	 *
	 * @param array 	$errors exsitng errors 
	 * @param string 	$name  name of custom field
	 * @param string 	$mets  meta key of custom field
	 * @param string 	$field  custom field type
	 * @param string 	$value  value of custom field for user
	 * @return array 	errors for the custom fields
	 */
	public function uaio_custom_field_type_validation($errors,$name,$meta,$field,$value){

		if($field == 'numeric'){
			if( ! empty($value) && ! is_numeric($value)){
				$errors[] = $name . __(' should be numeric.','uaio').'<br/>';
			}
		}else if($field == 'textonly'){
			// if(! empty($value) && ! ctype_alpha($value) ){
			if (! empty($value) && preg_match("/[^a-zA-Z ]/", $value)) {
				$errors[] = $name . __(' should be text only.','uaio').'<br/>';
			}
		}else if($field == 'user_badge'){

			if(is_array($value) && count(array_filter($value)) > UPME_BADGE_MAX_LIMIT){
				$errors[] = $name . __(' can have only '.UPME_BADGE_MAX_LIMIT.' badge(s).','uaio').'<br/>';
			}
		}


		return $errors;
	}

	/**
	 * All allowed field types for UPME
	 *
	 * @param array 	$input_types exsitng input types of UPME 
	 * @param array 	$params  dynamic params
	 * @return array 	$input_types updated allowed input types
	 */
	public function uaio_allowed_input_field_types($input_types, $params){
		$input_types['pdf'] = __('PDF Upload','uaio');
		$input_types['msword'] = __('MS Word Upload','uaio');
		$input_types['mspowerpoint'] = __('MS Powerpoint Upload','uaio');
		$input_types['msexcel'] = __('MS Excel Upload','uaio');
		$input_types['textDocument'] = __('Text Document Upload','uaio');
		$input_types['spreadsheet'] = __('Spreadsheet','uaio');
		$input_types['presentation'] = __('Presentation','uaio');
		$input_types['mp3'] = __('MP3 Upload','uaio');

		$input_types['wpEditor'] = __('WP Editor','uaio');

		$input_types['numeric'] = __('Numeric Field','uaio');
		$input_types['textonly'] = __('Text Only Field','uaio');
		$input_types['embedly_card'] = __('Embedly Card','uaio');
		$input_types['past_date'] = __('DatePicker (Past Dates)','uaio');
		$input_types['future_date'] = __('DatePicker (Future Dates)','uaio');

		$input_types['ordered_list'] = __('Ordered List','uaio');
		$input_types['unordered_list'] = __('Unordered List','uaio');

//		$input_types['wp_page'] = __('WP Pages','uaio');
//		$input_types['wp_post'] = __('WP Posts','uaio');
		$input_types['chosen_select'] = __('Chosen Select Field','uaio');
		$input_types['chosen_multiple'] = __('Chosen Multiple Select Field','uaio');

        $input_types['google_map'] = __('Google Map Field','uaio');
        $input_types['user_badge'] = __('User Badge Field','uaio');
        $input_types['mailchimp'] = __('Mailchimp Subscription Field','uaio');
		return $input_types;
	}

	/**
	 * Field types for validations
	 *
	 * @param array 	$input_types exsitng input types of UPME  for validation
	 * @param array 	$params  dynamic params
	 * @return array 	$input_types updated input types of UPME  for validation
	 */
	public function uaio_include_validation_field_types($input_types, $params){
		array_push($input_types, 'pdf');
		array_push($input_types, 'msword');
		array_push($input_types, 'mspowerpoint');
		array_push($input_types, 'msexcel');
		array_push($input_types, 'textDocument');
		array_push($input_types, 'spreadsheet');
		array_push($input_types, 'presentation');
		array_push($input_types, 'mp3');

		array_push($input_types, 'wpEditor');

		array_push($input_types, 'numeric');
		array_push($input_types, 'textonly');
		
		array_push($input_types, 'embedly_card');
		array_push($input_types, 'past_date');
		array_push($input_types, 'future_date');

		array_push($input_types, 'ordered_list');
		array_push($input_types, 'unordered_list');

//		array_push($input_types, 'wp_page');
//		array_push($input_types, 'wp_post');
		array_push($input_types, 'chosen_select');
		array_push($input_types, 'chosen_multiple');
        array_push($input_types, 'google_map');
        
        array_push($input_types, 'user_badge');
        array_push($input_types, 'mailchimp');
		return $input_types;
	}

	/**
	 * Add user defined custom field types
	 *
	 * @param array 	$field_types exsitng field types of UPME
	 * @param array 	$params  dynamic params
	 * @return array 	$field_types updated field types of UPME
	 */
	public function uaio_user_defined_custom_field_types($field_types,$params){
		array_push($field_types, 'pdf');
		array_push($field_types, 'msword');
		array_push($field_types, 'mspowerpoint');
		array_push($field_types, 'msexcel');
		array_push($field_types, 'textDocument');
		array_push($field_types, 'spreadsheet');
		array_push($field_types, 'presentation');
		array_push($field_types, 'mp3');

		array_push($field_types, 'wpEditor');

		array_push($field_types, 'numeric');
		array_push($field_types, 'textonly');
		
		array_push($field_types, 'embedly_card');

		array_push($field_types, 'past_date');
		array_push($field_types, 'future_date');

		array_push($field_types, 'ordered_list');
		array_push($field_types, 'unordered_list');

//		array_push($field_types, 'wp_page');
//		array_push($field_types, 'wp_post');
		array_push($field_types, 'chosen_select');
		array_push($field_types, 'chosen_multiple');
        array_push($field_types, 'google_map');
        array_push($field_types, 'user_badge');
        array_push($field_types, 'mailchimp');
        
		return $field_types;
	}

	/**
	 * Show custom field types on registration
	 *
	 * @param string 	$display exsitng HTML generated from other filters
	 * @param array 	$params  dynamic params
	 * @return string 	$display updated HTML for custom field
	 */
	public function uaio_registration_custom_field_types($display,$params){
		extract($params);
		switch ($field) {
			case 'pdf':
			case 'msword':
			case 'mspowerpoint':
			case 'msexcel':
			case 'textDocument':
			case 'spreadsheet':
			case 'presentation':
			case 'mp3':
    
                $object = $this->file_field;
				$display .= $object->uaio_file_registration_field($display,$params);
				break;
				break;
			
			case 'embedly_card':
				$object = $this->embedly_field;
				$display .= $object->uaio_embedly_card_registration_field($display,$params);
				break;

			case 'wpEditor':
				$object = $this->wp_editor_field;
				$display .= $object->uaio_wp_editor_registration_field($display,$params);
				break;

			case 'numeric':
				$object = $this->text_field;
				$display .= $object->uaio_numeric_registration_field($display,$params);
				break;

			case 'textonly':
				$object = $this->text_field;
				$display .= $object->uaio_textonly_registration_field($display,$params);
				break;

			case 'past_date':
				$object = $this->date_field;
				$display .= $object->uaio_past_date_registration_field($display,$params);
				break;
			case 'future_date':
				$object = $this->date_field;
				$display .= $object->uaio_future_date_registration_field($display,$params);
				break;

			case 'ordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_ordered_list_registration_field($display,$params);
				break;

			case 'unordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_unordered_list_registration_field($display,$params);
				break;

			case 'chosen_select':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_select_registration_field($display,$params);
				break;

			case 'chosen_multiple':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_multiple_registration_field($display,$params);
				break;
            
            case 'google_map':
				$object = $this->google_maps_field;
				$display .= $object->uaio_google_map_registration_field($display,$params);
				break;
            
            case 'mailchimp':
				$object = $this->mailchimp_field;
				$display .= $object->uaio_mailchimp_registration_field($display,$params);
				break;
        
			
			default:
				# code...
				break;
		}
		return $display;
	}

	/**
	 * Show custom field types on profile view
	 *
	 * @param string 	$display exsitng HTML generated from other filters
	 * @param array 	$params  dynamic params
	 * @return string 	$display updated HTML for custom field
	 */
	public function uaio_view_custom_field_type_input($display,$params){
		$params['id'] 	= $params['user_id'];
		extract($params);

		switch ($field) {
			case 'pdf':
			case 'msword':
			case 'mspowerpoint':
			case 'msexcel':
			case 'textDocument':
			case 'spreadsheet':
			case 'presentation':
			case 'mp3':
				$object = $this->file_field;
				$display .= $object->uaio_file_view_field($display,$params); 

				
				break;

			case 'embedly_card':
				$object = $this->embedly_field;
				$display .= $object->uaio_embedly_card_view_field($display,$params);
				break;
		
			case 'wpEditor':
				$object = $this->wp_editor_field;
				$display .= $object->uaio_wp_editor_view_field($display,$params);
				break;

			case 'numeric':
				$object = $this->text_field;
				$display .= $object->uaio_numeric_view_field($display,$params);
				break;

			case 'textonly':
				$object = $this->text_field;
				$display .= $object->uaio_textonly_view_field($display,$params);
				break;

			case 'past_date':
				$object = $this->date_field;
				$display .= $object->uaio_past_date_view_field($display,$params);
				break;
			case 'future_date':
				$object = $this->date_field;
				$display .= $object->uaio_future_date_view_field($display,$params);
				break;

			case 'ordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_ordered_list_view_field($display,$params);
				break;

			case 'unordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_unordered_list_view_field($display,$params);
				break;

			case 'chosen_select':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_select_view_field($display,$params);
				break;

			case 'chosen_multiple':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_multiple_view_field($display,$params);
				break;
            
            case 'google_map':
				$object = $this->google_maps_field;
				$display .= $object->uaio_google_map_view_field($display,$params);
				break;
            
            case 'user_badge':
				$object = $this->user_badge_field;
				$display .= $object->uaio_user_badge_view_field($display,$params);
				break;
            
            case 'mailchimp':
				$object = $this->mailchimp_field;
				$display .= $object->uaio_mailchimp_view_field($display,$params);
				break;

			default:
				# code...
				break;
		}

		return $display;
	}

	/**
	 * Show custom field types on profile edit on backend
	 *
	 * @param string 	$display exsitng HTML generated from other filters
	 * @param array 	$params  dynamic params
	 * @return string 	$display updated HTML for custom field
	 */
	public function uaio_backend_edit_custom_field_type_input($display, $params){
		$params['id'] = $params['user_id'];
		extract($params);
		
		switch ($field) {
			case 'pdf':
			case 'msword':
			case 'mspowerpoint':
			case 'msexcel':
			case 'textDocument':
			case 'spreadsheet':
			case 'presentation':
			case 'mp3':
            
				break;

			case 'embedly_card':
				$object = $this->embedly_field;
				$display .= $object->uaio_embedly_card_backend_edit_field($display,$params);
				break;

			case 'wpEditor':
				$object = $this->wp_editor_field;
				$display .= $object->uaio_wp_editor_backend_edit_field($display,$params);
				
				break;

			case 'numeric':
				$object = $this->text_field;
				$display .= $object->uaio_numeric_backend_edit_field($display,$params);
				break;

			case 'textonly':
				$object = $this->text_field;
				$display .= $object->uaio_textonly_backend_edit_field($display,$params);
				break;

			case 'past_date':
				$object = $this->date_field;
				$display .= $object->uaio_past_date_backend_edit_field($display,$params);
				break;
			case 'future_date':
				$object = $this->date_field;
				$display .= $object->uaio_future_date_backend_edit_field($display,$params);
				break;

			case 'ordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_ordered_list_backend_edit_field($display,$params);
				break;

			case 'unordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_unordered_list_backend_edit_field($display,$params);
				break;

			case 'chosen_select':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_select_backend_edit_field($display,$params);
				break;

			case 'chosen_multiple':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_multiple_backend_edit_field($display,$params);
				break;
            
            case 'user_badge':
				$object = $this->user_badge_field;
				$display .= $object->uaio_user_badge_backend_edit_field($display,$params);
				break;
            
            case 'google_map':
				$object = $this->google_maps_field;
				$display .= $object->uaio_google_map_backend_edit_field($display,$params);
				break;
            
            case 'mailchimp':
				$object = $this->mailchimp_field;
				$display .= $object->uaio_mailchimp_backend_edit_field($display,$params);
				break;

			
			default:
				# code...
				break;
		}
		return $display;
	}

	/**
	 * Show custom field types on profile edit
	 *
	 * @param string 	$display exsitng HTML generated from other filters
	 * @param array 	$params  dynamic params
	 * @return string 	$display updated HTML for custom field
	 */
	public function uaio_edit_custom_field_type_input($display, $params){
		$params['id'] = $params['user_id'];
		extract($params);
		
		switch ($field) {
			case 'pdf':
			case 'msword':
			case 'mspowerpoint':
			case 'msexcel':
			case 'textDocument':
			case 'spreadsheet':
			case 'presentation':
			case 'mp3':
				$object = $this->file_field;
				$display .= $object->uaio_file_edit_field($display,$params);                
				break;

			case 'embedly_card':
				$object = $this->embedly_field;
				$display .= $object->uaio_embedly_card_edit_field($display,$params);
				break;

			case 'wpEditor':
				$object = $this->wp_editor_field;
				$display .= $object->uaio_wp_editor_edit_field($display,$params);
				break;

			case 'numeric':
				$object = $this->text_field;
				$display .= $object->uaio_numeric_edit_field($display,$params);
				break;

			case 'textonly':
				$object = $this->text_field;
				$display .= $object->uaio_textonly_edit_field($display,$params);
				break;

			case 'past_date':
				$object = $this->date_field;
				$display .= $object->uaio_past_date_edit_field($display,$params);
				break;
			case 'future_date':
				$object = $this->date_field;
				$display .= $object->uaio_future_date_edit_field($display,$params);
				break;

			case 'ordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_ordered_list_edit_field($display,$params);
				break;

			case 'unordered_list':
				$object = $this->list_field;
				$display .= $object->uaio_unordered_list_edit_field($display,$params);
				break;

			case 'chosen_select':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_select_edit_field($display,$params);
				break;

			case 'chosen_multiple':
				$object = $this->chosen_select_field;
				$display .= $object->uaio_chosen_multiple_edit_field($display,$params);
				break;
            
            case 'user_badge':
				$object = $this->user_badge_field;
				$display .= $object->uaio_user_badge_edit_field($display,$params);
				break;
            
            case 'google_map':
				$object = $this->google_maps_field;
				$display .= $object->uaio_google_map_edit_field($display,$params);
				break;
            
            case 'mailchimp':
				$object = $this->mailchimp_field;
				$display .= $object->uaio_mailchimp_edit_field($display,$params);
				break;
		
			default:
				# code...
				break;
		}

		return $display;
	}


	
}

$uaio_field_type_manager = new UAIO_Field_Type_Manager();







