<?php

class UAIO_Profile_Tab_Fields{
    
    public $uaio;

    public function __construct(){
    
        add_shortcode('uaio_profile_tab_field', array($this,'profile_tab_field_shortcode'));
        
    }
    
    public function profile_tab_field_shortcode($atts,$content){
        global $upme,$upme_options;
        
        extract(shortcode_atts(array(
		'meta_key'	=>	'',
     	), $atts));
        
        $profile_fields = $upme_options->upme_profile_fields;
        
        $field_params = isset( $upme_options->upme_profile_fields_by_key[$meta_key] ) ? $upme_options->upme_profile_fields_by_key[$meta_key] : array();

        $key_index = isset($field_params['key_index']) ? $field_params['key_index'] : '0';
        
        $user_id =  isset($upme->current_view_profile_id) ? $upme->current_view_profile_id : '0';
        
        $display = '';
        
        $display .= $upme->separator_empty_field_labels($user_id,$field_params,$profile_fields[$key_index]);

        $display .= $upme->none_empty_field_labels($user_id,'',$field_params,$profile_fields[$key_index]);
        
        return $display;
    }
    

}


