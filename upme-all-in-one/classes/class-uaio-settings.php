<?php

class UAIO_Settings{
    
    public function __construct(){
        
        add_action('admin_menu', array(&$this, 'add_menu'), 9);
        add_action('admin_init', array($this,'save_settings_page') );
        
    }
    
    public function add_menu(){
        add_menu_page(__('All In One', "uaio"), __("UPME All In One Settings", "uaio"),'manage_options','uaio-all-in-one',array(&$this,'general_settings'));
        
    
        add_submenu_page('uaio-all-in-one', __("User Badges Settings", "uaio"), __("User Badges Settings", "uaio"),'manage_options','uaio-user-badges',array(&$this,'user_badge_settings'));
        
        add_submenu_page('uaio-all-in-one', __("Profil Field Settings", "uaio"), __("Profile Field Settings", "uaio"),'manage_options','uaio-profile-fields',array(&$this,'profile_fields_settings'));
        
        add_submenu_page('uaio-all-in-one', __("Email Settings", "uaio"), __("Email Settings", "uaio"),'manage_options','uaio-emails',array(&$this,'email_settings'));
    }

    
    public function plugin_options_tabs($type,$tab) {
        $current_tab = $tab;
        $this->plugin_settings_tabs = array();
        
        switch($type){
            case 'general':
                $this->plugin_settings_tabs['uaio_section_general']  = __('General Settings','uaio');
                break;
            case 'user_badge':
                $this->plugin_settings_tabs['uaio_section_user_badge']  = __('User Badge','uaio');
                break;
            case 'header_fields':
                $this->plugin_settings_tabs['uaio_section_header_fields']  = __('Profile Fields','uaio');
                break;
            case 'mailchimp':
                $this->plugin_settings_tabs['uaio_section_mailchimp']  = __('Mailchimp','uaio');
                $this->plugin_settings_tabs['uaio_section_mailchimp_fields']  = __('Mailchimp Fields','uaio');
                break;

        }
        
        ob_start();
        ?>

        <h2 class="nav-tab-wrapper">
        <?php 
            foreach ( $this->plugin_settings_tabs as $tab_key => $tab_caption ) {
            $active = $current_tab == $tab_key ? 'nav-tab-active' : '';
            $page = isset($_GET['page']) ? $_GET['page'] : '';
        ?>
                <a class="nav-tab <?php echo $active; ?> " href="?page=<?php echo $page; ?>&tab=<?php echo $tab_key; ?>"><?php echo $tab_caption; ?></a>
            
        <?php } ?>
        </h2>

        <?php
                
        return ob_get_clean();
    }
    
    public function plugin_options_tab_content($tab,$params = array()){
        global $uaio,$uaio_settings_data;
        
        $social_options = get_option('uaio_options');
        
        ob_start();
        switch($tab){
            case 'uaio_section_general':                
                $data = isset($social_options['general']) ? $social_options['general'] : array();
                $uaio_settings_data['google_map_api_key'] = isset($data['google_map_api_key']) ? $data['google_map_api_key'] : '';
                
                $uaio_settings_data['tab'] = $tab;
            
                $uaio->template_loader->get_template_part('general-settings');            
                break;
            
            case 'uaio_section_user_badge':                
	            $data = isset($social_options['user_badges']) ? $social_options['user_badges'] : array();
	            //$uaio_settings_data['default_badge'] = isset($data['default_badge']) ? $data['default_badge'] : 0;
                $uaio_settings_data['field_badges'] = isset($data['field_badges']) ? $data['field_badges'] : array();
                
                $uaio_settings_data['tab'] = $tab;
            
                $uaio->template_loader->get_template_part('user-badge-settings');            
                break;
            
            case 'uaio_section_header_fields':                
	            $data = isset($social_options['header_fields']) ? $social_options['header_fields'] : array();
                $uaio_settings_data['header_fields_list'] = isset($data['header_fields_list']) ? $data['header_fields_list'] : array();
                $uaio_settings_data['header_fields_display_type'] = isset($data['header_fields_display_type']) ? $data['header_fields_display_type'] : '0';
                $uaio_settings_data['header_fields_compact_view_status'] = isset($data['header_fields_compact_view_status']) ? $data['header_fields_compact_view_status'] : '0';
                $uaio_settings_data['tab_fields_list'] = isset($data['tab_fields_list']) ? $data['tab_fields_list'] : array();
            
                $uaio_settings_data['tab'] = $tab;
            
                $uaio->template_loader->get_template_part('header-fields-settings');            
                break;
            
            case 'uaio_section_mailchimp':                
	            $data = isset($social_options['mailchimp']) ? $social_options['mailchimp'] : array();
           
                $uaio_settings_data['api_key'] = isset($data['api_key']) ? $data['api_key'] : '';
           
                $uaio_settings_data['tab'] = $tab;
            
                $uaio->template_loader->get_template_part('mailchimp-settings');            
                break;
            
            case 'uaio_section_mailchimp_fields':                
      
                $uaio_settings_data['tab'] = $tab;
            
                $uaio->template_loader->get_template_part('mailchimp-fields');            
                break;
        }
        
        $display = ob_get_clean();
        return $display;
        
    }
    
    public function save_settings_page(){
        
        $uaio_settings_pages = array('uaio-user-badges','uaio-profile-fields','uaio-emails','uaio-all-in-one');
        
        if(isset($_POST['uaio_apply_default_badge'])){
            $user_query = new WP_User_Query(array ( 'orderby' => 'registered', 'order' => 'ASC' ));

            $users = $user_query->get_results();

            foreach ($users as $key => $user) {
                $social_options = get_option('uaio_options');
                $data = $social_options['user_badges'];
	            $default_badge = isset($data['default_badge']) ? $data['default_badge'] : 0;
                update_user_meta($user->data->ID, 'user_badge',$default_badge);
            }
            
            add_action( 'admin_notices', array( $this, 'admin_notices' ) );

        }else{
      
            if(isset($_POST['uaio_tab']) && isset($_GET['page']) && in_array($_GET['page'],$uaio_settings_pages)){
                $tab = '';
                if ( isset ( $_POST['uaio_tab'] ) )
                   $tab = $_POST['uaio_tab']; 

                if($tab != ''){
                    $func = 'save_'.$tab;
                    $this->$func();
                }          
            }
        }
        
        
    }
    
    public function save_uaio_section_general(){
        
        if(isset($_POST['uaio_general'])){
            foreach($_POST['uaio_general'] as $k=>$v){
                $this->settings[$k] = $v;
            }            
            
        }
        
        $uaio_options = get_option('uaio_options');
        $uaio_options['general'] = $this->settings;
        update_option('uaio_options',$uaio_options);
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );  

        
    }

    public function save_uaio_section_user_badge(){
        
        if(isset($_POST['uaio_badge'])){
            foreach($_POST['uaio_badge'] as $k=>$v){
                $this->settings[$k] = $v;
            }
        }
        
        
        if(!isset($_POST['uaio_badge']['default_badge'])){
            $this->settings['default_badge'] = 0;
        }
        
        $uaio_options = get_option('uaio_options');
        $uaio_options['user_badges'] = $this->settings;
        update_option('uaio_options',$uaio_options);
        
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );
    }
    
    public function save_uaio_section_header_fields(){
        
        if(isset($_POST['uaio_header_fields'])){
            foreach($_POST['uaio_header_fields'] as $k=>$v){
                $this->settings[$k] = $v;
            }            
            
        }
        
        $uaio_options = get_option('uaio_options');
        $uaio_options['header_fields'] = $this->settings;
        update_option('uaio_options',$uaio_options);
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );  

        
    }
    
    public function save_uaio_section_mailchimp(){
        
        if(isset($_POST['uaio_mailchimp'])){
            foreach($_POST['uaio_mailchimp'] as $k=>$v){
                $this->settings[$k] = $v;
            }            
            
        }
        
        $uaio_options = get_option('uaio_options');
        $uaio_options['mailchimp'] = $this->settings;
        update_option('uaio_options',$uaio_options);
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );  

        
    }
    
    public function save_uaio_section_mailchimp_fields(){
        
        if(isset($_POST['uaio_mailchimp_fields'])){
            $uaio_mailchimp_field_lists = $_POST['uaio_mailchimp_fields'];       
            
        }
        
        update_option('uaio_mailchimp_field_lists',$uaio_mailchimp_field_lists);
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );   

        
    }
    
    public function admin_notices(){
        ?>
        <div class="updated">
          <p><?php esc_html_e( 'Settings saved successfully.', 'coupinewp' ); ?></p>
       </div>
        <?php
    }
    
    
    public function general_settings(){
        global $uaio,$uaio_settings_data;
        
        add_settings_section( 'uaio_section_general', __('General Settings','uaio'), array( &$this, 'section_general_desc' ), 'uaio-all-in-one' );
        
        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'uaio_section_general';
        $uaio_settings_data['tab'] = $tab;
        
        $tabs = $this->plugin_options_tabs('general',$tab);
   
        $uaio_settings_data['tabs'] = $tabs;
        
        $tab_content = $this->plugin_options_tab_content($tab);
        $uaio_settings_data['tab_content'] = $tab_content;
        
        ob_start();
        $uaio->template_loader->get_template_part( 'menu-page-container');
        $display = ob_get_clean();
        echo $display;
        
    
    }

    public function user_badge_settings(){
        global $uaio,$uaio_settings_data;
        
        add_settings_section( 'uaio_section_user_badge', __('User Badge Settings','uaio'), array( &$this, 'section_general_desc' ), 'uaio-user-badges' );
        
        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'uaio_section_user_badge';
        $uaio_settings_data['tab'] = $tab;
        
        $tabs = $this->plugin_options_tabs('user_badge',$tab);
   
        $uaio_settings_data['tabs'] = $tabs;
        
        $tab_content = $this->plugin_options_tab_content($tab);
        $uaio_settings_data['tab_content'] = $tab_content;
        
        ob_start();
		$uaio->template_loader->get_template_part( 'menu-page-container');
		$display = ob_get_clean();
		echo $display;
        
    
    }
    
    public function profile_fields_settings(){
        global $uaio,$uaio_settings_data;
        
        add_settings_section( 'uaio_section_header_fields', __('Header Fields Settings','uaio'), array( &$this, 'section_general_desc' ), 'uaio-header-fields' );
        
        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'uaio_section_header_fields';
        $uaio_settings_data['tab'] = $tab;
        
        $tabs = $this->plugin_options_tabs('header_fields',$tab);
   
        $uaio_settings_data['tabs'] = $tabs;
        
        $tab_content = $this->plugin_options_tab_content($tab);
        $uaio_settings_data['tab_content'] = $tab_content;
        
        ob_start();
		$uaio->template_loader->get_template_part( 'menu-page-container');
		$display = ob_get_clean();
		echo $display;
        
    
    }
    
    public function email_settings(){
        global $uaio,$uaio_settings_data;
        
        add_settings_section( 'uaio_section_mailchimp', __('Mailchimp Settings','uaio'), array( &$this, 'email_section_general_desc' ), 'uaio-mailchimp' );
        
        add_settings_section( 'uaio_section_mailchimp_fields', __('Mailchimp Fields Settings','uaio'), array( &$this, 'email_section_general_desc' ), 'uaio-mailchimp' );
        
        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'uaio_section_mailchimp';
        $uaio_settings_data['tab'] = $tab;
        
        $tabs = $this->plugin_options_tabs('mailchimp',$tab);
   
        $uaio_settings_data['tabs'] = $tabs;
        
        $tab_content = $this->plugin_options_tab_content($tab);
        $uaio_settings_data['tab_content'] = $tab_content;
        
        ob_start();
		$uaio->template_loader->get_template_part( 'menu-page-container');
		$display = ob_get_clean();
		echo $display;
        
    
    }
    
}


