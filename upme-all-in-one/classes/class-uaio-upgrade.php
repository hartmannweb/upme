<?php

add_action('admin_init', 'uaio_upgrade_routine');

function uaio_upgrade_routine() {

 
    $stored_version = get_option('uaio_version');
    $current_version = uaio_get_plugin_version();

    if (!$stored_version && $current_version) {
        uaio_initialize_regular_tasks();
        update_option('uaio_version', $current_version);
    }

    if (version_compare($current_version, $stored_version) == 0) {
        return;
    }

    update_option('uaio_version', $current_version);
}

function uaio_initialize_regular_tasks(){
    $current_option = get_option('uaio_options');
    update_option('uaio_options', $current_option);
}