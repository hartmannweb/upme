<?php
/**
 * Manage Embedly Card field related functionality
 */
class UAIO_Embedly_Card_Field{

    /**
     * Show embedly card field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_embedly_card_edit_field($display,$params){

        extract($params);

        $display .= '<input title="' . $name . '" ' . $disabled . ' type="text" class="upme-input ' . $required_class . ' upme-edit-' . $meta . '" name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '" value="' . $value . '" />';
        return $display;                       
    }

    /**
     * Show embedly card field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_embedly_card_view_field($display,$params){

        extract($params);

        wp_register_script('uaio_embedly', '//cdn.embedly.com/widgets/platform.js', array('jquery'));
        wp_enqueue_script('uaio_embedly');
        

        $value = '<a class="embedly-card" href="' . $value . '">&nbsp;</a>';
        return $value;
    }

    /**
     * Show embedly card field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_embedly_card_registration_field($display,$params){
        global $upme;
        extract($params);

        $display .= '<input type="text" class="upme-input ' . $required_class . '" name="' . $meta . '" id="' . $id . '" value="' . $upme->post_value($meta) . '" title="' . $name . '" />';
        return $display;

    }

    /**
     * Show embedly card field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_embedly_card_backend_edit_field($display,$params){

        extract($params);
        return '<input type="text" ' . $disabled . ' name="' . $name . '" id="' . $meta . '" value="' . esc_attr($value) . '" class="regular-text">';
                             
        
    }

}