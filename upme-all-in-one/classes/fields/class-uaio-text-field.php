<?php
/**
 * Manage Text field related functionality
 */
class UAIO_Text_Field{

    /**
     * Show textonly field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_textonly_edit_field($display,$params){

        extract($params);
        $display .= '<input title="' . $name . '" ' . $disabled . ' type="text" class="upme-input upme-textonly ' . $required_class . ' upme-edit-' . $meta . '" name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '" value="' . $value . '" />';
        return $display;   
    }

    /**
     * Show textonly field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_textonly_view_field($display,$params){

        extract($params);
        return $value;
    }

    /**
     * Show textonly field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_textonly_registration_field($display,$params){
        global $upme;
        extract($params);

        $display .= '<input type="text" class="upme-input upme-textonly ' . $required_class . '" name="' . $meta . '" id="' . $id . '" value="' . $upme->post_value($meta) . '" title="' . $name . '" />';
        return $display;

    }

    /**
     * Show textonly field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_textonly_backend_edit_field($display,$params){

        extract($params);

        return '<input type="text" ' . $disabled . ' name="' . $name . '" id="' . $meta . '" value="' . esc_attr($value) . '" class="regular-text upme-textonly">';
                
    }

    /**
     * Show numeric field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_numeric_edit_field($display,$params){

        extract($params);

        $display .= '<input title="' . $name . '" ' . $disabled . ' type="text" class="upme-input upme-numeric ' . $required_class . ' upme-edit-' . $meta . '" name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '" value="' . $value . '" />';
        return $display;                       
    }

    /**
     * Show numeric field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_numeric_view_field($display,$params){

        extract($params);
        return $value;
    }

    /**
     * Show numeric field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_numeric_registration_field($display,$params){
        global $upme;
        extract($params);

        $display .= '<input type="text" class="upme-input upme-numeric ' . $required_class . '" name="' . $meta . '" id="' . $id . '" value="' . $upme->post_value($meta) . '" title="' . $name . '" />';
        return $display;

    }

    /**
     * Show numeric field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_numeric_backend_edit_field($display,$params){

        extract($params);
        return '<input type="text" ' . $disabled . ' name="' . $name . '" id="' . $meta . '" value="' . esc_attr($value) . '" class="regular-text upme-numeric">';
                             
        
    }

}