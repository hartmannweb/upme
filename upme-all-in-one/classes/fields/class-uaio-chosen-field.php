<?php

class UAIO_Chosen_Field{

	public function __construct(){

	}
	public function uaio_chosen_select_edit_field($display,$params){
		global $predefined;
		
		$this->uaio_include_chosen_scripts();

		extract($params);

		$loop = array();
        if (isset($array['predefined_loop']) && $array['predefined_loop'] != '' && $array['predefined_loop'] != '0') {

            $loop = $predefined->get_array($array['predefined_loop']);
            if ('countries' == $array['predefined_loop']) {
                array_shift($loop);
            }
        } else if (isset($array['choices']) && $array['choices'] != '') {
            $loop = explode(PHP_EOL, $array['choices']);
        }

        if (isset($loop)) {

            $profile_user_meta = $value;


            // Check for country loop
            $country_loop_status = isset($array['predefined_loop']) ? $array['predefined_loop'] : '';

            if ('' == $profile_user_meta && '' != $country_loop_status && 'countries' == $country_loop_status) {

                $profile_user_meta = $loop[$upme_settings['default_predefined_country']];
            }

            $display .= '<select title="' . $name . '" ' . $disabled . ' class="chosen-field-type upme-input ' . $required_class . '" name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '">';
            $display .= '<option value="" ' . selected($profile_user_meta, "", 0) . '>' . __('Please Select', 'upme') . '</option>';
            foreach ($loop as $option) {
                // Added as per http://codecanyon.net/item/user-profiles-made-easy-wordpress-plugin/discussion/4109874?filter=All+Discussion&page=27#comment_4352415
                $option = upme_stripslashes_deep(trim($option));

                $display .= '<option value="' . $option . '" ' . selected($profile_user_meta, $option, 0) . '>' . $option . '</option>';
            }
            $display .= '</select>';
        }
        $display .= '<div class="upme-clear"></div>';

        return $display;
	}

	public function uaio_chosen_select_view_field($display,$params){

		extract($params);
		return $value;
	}

    public function uaio_chosen_select_registration_field($display,$params){
    	global $predefined,$upme;

    	$this->uaio_include_chosen_scripts();
        extract($params);

        $loop = array();

        if (isset($array['predefined_loop']) && $array['predefined_loop'] != '' && $array['predefined_loop'] != '0') {
            $loop = $predefined->get_array($array['predefined_loop']);
            if ('countries' == $array['predefined_loop']) {
                array_shift($loop);
            }
        } else if (isset($array['choices']) && $array['choices'] != '') {
            $loop = explode(PHP_EOL, $array['choices']);
        }

        if (isset($loop)) {

            // Check for country loop
            $country_loop_status = isset($array['predefined_loop']) ? $array['predefined_loop'] : '';

            $selected_value = $upme->post_value($meta);
            if ('' == $upme->post_value($meta) && '' != $country_loop_status && 'countries' == $country_loop_status) {
                $upme_settings = get_option('upme_options');
                $selected_value = $loop[$upme_settings['default_predefined_country']];
            }

            $display .= '<select class="chosen-field-type upme-input' . $required_class . '" name="' . $meta . '[]" id="' . $meta . '" title="' . $name . '">';
            $display .= '<option value="" >' . __('Please Select', 'upme') . '</option>';
            foreach ($loop as $option) {

                // Added as per http://codecanyon.net/item/user-profiles-made-easy-wordpress-plugin/discussion/4109874?filter=All+Discussion&page=27#comment_4352415

                $option = upme_stripslashes_deep(trim($option));

                $display .= '<option value="' . $option . '" ' . selected($selected_value, $option, 0) . '>' . $option . '</option>';
            }
            $display .= '</select>';
        }
        $display .= '<div class="upme-clear"></div>';

        return $display;

    }

    public function uaio_chosen_select_backend_edit_field($display,$params){

    	$this->uaio_include_chosen_scripts();
        extract($params);

        $loop = array();
	    if (isset($array['predefined_loop']) && $array['predefined_loop'] != '' && $array['predefined_loop'] != '0') {
	        $loop = $predefined->get_array($array['predefined_loop']);
	        if('countries' == $array['predefined_loop']){
	            array_shift($loop);
	        } 

	    } else if (isset($array['choices']) && $array['choices'] != '') {
	        $loop = explode(PHP_EOL, $array['choices']);
	    }

	    // Check for country loop
	    $country_loop_status = isset($array['predefined_loop']) ? $array['predefined_loop'] : '';
	    $profile_user_meta = $value;
	    if('' == $profile_user_meta && '' != $country_loop_status && 'countries' == $country_loop_status){
	        
	        $profile_user_meta = $loop[$upme_settings['default_predefined_country']];
	    }

	    $display = '';
	    if (count($loop) > 0) {
	        $display .= '<select ' . $disabled . ' class=" chosen-admin-field-type input" name="upme[' . $meta . ']" id="' . $meta . '">';
	        $display .= '<option value="" ' . selected($profile_user_meta, "", 0) . '>' . __('Please Select', 'upme') . '</option>';
	        foreach ($loop as $option) {
	            $option = upme_stripslashes_deep(trim($option));

	            $display .= '<option value="' . $option . '" ' . selected($profile_user_meta, $option, 0) . '>' . $option . '</option>';
	        }
	        $display .= '</select>';
	    }
	    return $display;

    }

    public function uaio_chosen_multiple_edit_field($display,$params){
        global $predefined;
        
        $this->uaio_include_chosen_scripts();

        extract($params);

        $loop = array();
        if (isset($array['predefined_loop']) && $array['predefined_loop'] != '' && $array['predefined_loop'] != '0') {

            $loop = $predefined->get_array($array['predefined_loop']);
            if ('countries' == $array['predefined_loop']) {
                array_shift($loop);
            }
        } else if (isset($array['choices']) && $array['choices'] != '') {
            $loop = explode(PHP_EOL, $array['choices']);
        }

        if (isset($loop)) {

            $profile_user_meta = explode(', ', $value);
            $profile_user_meta = (array) $profile_user_meta;

            // Check for country loop
            $country_loop_status = isset($array['predefined_loop']) ? $array['predefined_loop'] : '';

            if ('' == $profile_user_meta && '' != $country_loop_status && 'countries' == $country_loop_status) {

                $profile_user_meta = $loop[$upme_settings['default_predefined_country']];
            }


            $display .= '<select multiple title="' . $name . '" ' . $disabled . ' class="upme-chosen-multiple chosen-field-type upme-input ' . $required_class . '" name="' . $meta . '-' . $id . '[]" id="' . $meta . '-' . $id . '">';
            $display .= '<option value="" '. selected($profile_user_meta[0], "", 0) .' ></option>';
            foreach ($loop as $option) {
                // Added as per http://codecanyon.net/item/user-profiles-made-easy-wordpress-plugin/discussion/4109874?filter=All+Discussion&page=27#comment_4352415
                $option = upme_stripslashes_deep(trim($option));

                $selected = '';
                if(in_array($option, $profile_user_meta)){
                    $selected = 'selected';
                }

                $display .= '<option value="' . $option . '" ' . $selected . '>' . $option . '</option>';
            }
            $display .= '</select>';
        }
        $display .= '<div class="upme-clear"></div>';

        return $display;
    }

    public function uaio_chosen_multiple_view_field($display,$params){

        extract($params);
        $value = explode(',',$value);
        if(is_array($value)){
            foreach($value as $k=>$v){
                if(trim($v) ==''){
                    unset($value[$k]);
                }                
            }
            $value = implode(',',$value);
        }
        
        return $value;
    }

    public function uaio_chosen_multiple_registration_field($display,$params){
        global $predefined,$upme;

        $this->uaio_include_chosen_scripts();
        extract($params);

        $loop = array();

        if (isset($array['predefined_loop']) && $array['predefined_loop'] != '' && $array['predefined_loop'] != '0') {
            $loop = $predefined->get_array($array['predefined_loop']);
            if ('countries' == $array['predefined_loop']) {
                array_shift($loop);
            }
        } else if (isset($array['choices']) && $array['choices'] != '') {
            $loop = explode(PHP_EOL, $array['choices']);
        }

        if (isset($loop)) {

            // Check for country loop
            $country_loop_status = isset($array['predefined_loop']) ? $array['predefined_loop'] : '';

            $selected_value = explode(', ', $upme->post_value($meta));
            $selected_value = (array) $selected_value;

            if ('' == $upme->post_value($meta) && '' != $country_loop_status && 'countries' == $country_loop_status) {
                $upme_settings = get_option('upme_options');
                $selected_value = $loop[$upme_settings['default_predefined_country']];
            }

            $display .= '<select multiple class="upme-chosen-multiple chosen-field-type upme-input' . $required_class . '" name="' . $meta . '" id="' . $meta . '" title="' . $name . '">';
            $display .= '<option value="" >' . __('Please Select', 'upme') . '</option>';
            foreach ($loop as $option) {

                // Added as per http://codecanyon.net/item/user-profiles-made-easy-wordpress-plugin/discussion/4109874?filter=All+Discussion&page=27#comment_4352415

                $option = upme_stripslashes_deep(trim($option));

                $selected = '';
                if(in_array($option, $selected_value)){
                    $selected = 'selected';
                }

                $display .= '<option value="' . $option . '" ' . $selected . '>' . $option . '</option>';

                
            }
            $display .= '</select>';
        }
        $display .= '<div class="upme-clear"></div>';

        return $display;

    }

    public function uaio_chosen_multiple_backend_edit_field($display,$params){

        $this->uaio_include_chosen_scripts();
        extract($params);

        $loop = array();
        if (isset($array['predefined_loop']) && $array['predefined_loop'] != '' && $array['predefined_loop'] != '0') {
            $loop = $predefined->get_array($array['predefined_loop']);
            if('countries' == $array['predefined_loop']){
                array_shift($loop);
            } 

        } else if (isset($array['choices']) && $array['choices'] != '') {
            $loop = explode(PHP_EOL, $array['choices']);
        }

        // Check for country loop
        $country_loop_status = isset($array['predefined_loop']) ? $array['predefined_loop'] : '';
        
        $profile_user_meta = explode(', ', $value);
        $profile_user_meta = (array) $profile_user_meta;

        // if('' == $profile_user_meta && '' != $country_loop_status && 'countries' == $country_loop_status){
            
        //     $profile_user_meta = $loop[$upme_settings['default_predefined_country']];
        // }

        $display = '';
        if (count($loop) > 0) {
            $display .= '<select multiple ' . $disabled . ' class="upme-chosen-multiple chosen-admin-field-type input" name="upme_multiple_chosen_' . $meta . '[]" id="' . $meta . '">';
            $display .= '<option value="" ' . selected($profile_user_meta[0], "", 0) . '>' . __('Please Select', 'upme') . '</option>';
            foreach ($loop as $option) {
                $option = upme_stripslashes_deep(trim($option));

                $selected = '';
                if(in_array($option, $profile_user_meta)){
                    $selected = 'selected';
                }

                $display .= '<option value="' . $option . '" ' . $selected . '>' . $option . '</option>';
            }
            $display .= '</select>';
        }
        return $display;

    }

    public function uaio_include_chosen_scripts(){

    	wp_register_script('upme_chosen_js', upme_url . 'admin/js/chosen/chosen.jquery.js');
		wp_enqueue_script('upme_chosen_js');
    
        wp_register_style('upme_chosen_css', upme_url . 'admin/js/chosen/chosen.css');
        wp_enqueue_style('upme_chosen_css');
    }

}