<?php
/**
 * Manage File upload field related functionality
 */
class UAIO_File_Field{

    /**
     * Show file field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
	public function uaio_file_edit_field($display,$params){

		extract($params);

		// Include removal link for profile images
        $display_delete_link = '<div class="upme-delete-image-wrapper" upme-data-field-name="' . $meta . '" upme-data-user-id="' . $id . '"><i class="upme-icon-remove" original-title="remove"></i> <label class="upme-delete-image"  >' . __('Delete File', 'upme') . '</label></div>';
        $display_delete_link .= '<div id="upme-spinner-' . $meta . '" class="upme-delete-spinner"><i original-title="spinner" class="upme-icon-spinner upme-tooltip3"></i><label>' . __('Loading', 'upme') . '</label></div>';


        if (get_the_author_meta($meta, $id) != '') {
        	$non_image_fileupload_fields_params = array('meta' => $meta, 'field'=> $field, 'user_id' => $id, 'delete_link' => $display_delete_link);
            $display .= apply_filters('uaio_edit_non_image_fileupload_fields', '' ,$non_image_fileupload_fields_params);
        }

        
        if (upme_is_safari() || upme_is_opera()) {
            $display .= '<input title="' . $name . '" ' . $disabled . ' type="file" name="' . $meta . '-' . $id . '" id="file_' . $meta . '-' . $id . '" style="display:block;" />';
            
            $display .= '<input type="hidden" name="' . $meta . '-' . $id . '-upload-status" id="file_' . $meta . '-' . $id . '-upload-status" class="upme-file-upload-status" value="'. get_the_author_meta($meta, $id) .'" />';
            
            
        } else {
            $display .= '<input title="' . $name . '" class="upme-fileupload-field ' . $required_class . '" ' . $disabled . ' type="file" name="' . $meta . '-' . $id . '" id="file_' . $meta . '-' . $id . '" style="display:block;" />';
            
            $display .= '<input type="hidden" name="' . $meta . '-' . $id . '-upload-status" id="file_' . $meta . '-' . $id . '-upload-status" class="upme-file-upload-status" value="'. get_the_author_meta($meta, $id) .'" />';
            
        }

        return $display;
	}

    /**
     * Show file field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
	public function uaio_file_view_field($display,$params){

		extract($params);

		$non_image_fileupload_fields_params = array('meta' => $meta, 'field'=> $field, 'user_id' => $id);
        $display .= apply_filters('uaio_view_non_image_fileupload_fields','',$non_image_fileupload_fields_params);

        return $display;
	}
    
    public function uaio_file_registration_field($display,$params){
        global  $upme;
        extract($params);
        $display .= '<input type="file" class="upme-input' . $required_class . '" name="' . $meta . '" id="reg_' . $meta . '" value="' . $upme->post_value($meta) . '" title="' . $name . '" />';
        
        return $display;
    }

}