<?php

class UAIO_User_Badge_Field{

    public $user_badge_fields;
    
	public function __construct(){
        $social_options = get_option('uaio_options');
        $this->user_badge_fields = isset($social_options['user_badges']['field_badges']) ? $social_options['user_badges']['field_badges'] : array();

	}
	
    public function uaio_user_badge_edit_field($display,$params){
        global $predefined;
        
        $this->uaio_include_chosen_scripts();

        extract($params);

        $loop = array();
        
        if(isset($this->user_badge_fields[$meta]) && is_array($this->user_badge_fields[$meta])){
            $badges_query = new WP_Query( array( 'post_status' => 'publish', 'post__in' => $this->user_badge_fields[$meta] , 'post_type'=> UAIO_USER_BADGES_POST_TYPE ,'posts_per_page' => '-1' ) );
        }else{
            $badges_query = new WP_Query( array( 'post_status' => 'publish', 'post_type'=> UAIO_USER_BADGES_POST_TYPE ,'posts_per_page' => '-1' ) );
        
            
        }
        
        if ( $badges_query->have_posts() ) :
            while ($badges_query->have_posts()) : $badges_query->the_post();
                array_push($loop, array('key' => get_the_ID(), 'value' => get_the_title()));
            endwhile;
        endif;
        
        
     
        
        if (isset($loop)) {

            $profile_user_meta = explode(', ', $value);
            $profile_user_meta = (array) $profile_user_meta;

            // Check for country loop
            $country_loop_status = isset($array['predefined_loop']) ? $array['predefined_loop'] : '';

            if ('' == $profile_user_meta && '' != $country_loop_status && 'countries' == $country_loop_status) {

                $profile_user_meta = $loop[$upme_settings['default_predefined_country']];
            }


            $display .= '<select multiple title="' . $name . '" ' . $disabled . ' class="upme-chosen-multiple chosen-field-type upme-input ' . $required_class . '" name="' . $meta . '-' . $id . '[]" id="' . $meta . '-' . $id . '">';
            $display .= '<option value="" '. selected($profile_user_meta[0], "", 0) .' ></option>';
            
            foreach ($loop as $option) {
                // Added as per http://codecanyon.net/item/user-profiles-made-easy-wordpress-plugin/discussion/4109874?filter=All+Discussion&page=27#comment_4352415
                $option['value'] = upme_stripslashes_deep(trim($option['value']));

                $selected = '';
                if(in_array($option['key'], $profile_user_meta)){
                    $selected = 'selected';
                }

                $display .= '<option value="' . $option['key'] . '" ' . $selected . '>' . $option['value'] . '</option>';
            }
            $display .= '</select>';
        }
        $display .= '<div class="upme-clear"></div>';

        return $display;
    }

    public function uaio_user_badge_view_field($display,$params){

        extract($params);
        $value = explode(',',$value);
        if(is_array($value)){
            $display = '';

            $badges_query = new WP_Query( array( 'post_type' => UAIO_USER_BADGES_POST_TYPE , 'post__in' => $value ) );
            if ( $badges_query->have_posts() ) :
                while ($badges_query->have_posts()) : $badges_query->the_post();

                    if (has_post_thumbnail()){ 
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail' ); 
                        $link = get_permalink();
                        $display .= '<a href="'.$link.'" target="_blank" ><img class="uaio-badge" src="'.$image[0].'" title="'.get_the_title().'" /></a>';
                    }
                endwhile;
            endif;
            
        }

        
        return $display;
    }

    public function uaio_user_badge_backend_edit_field($display,$params){

        $this->uaio_include_chosen_scripts();
        extract($params);

        $loop = array();
        
        if(isset($this->user_badge_fields[$meta]) && is_array($this->user_badge_fields[$meta]) &&
          isset($this->user_badge_fields[$meta][0]) && $this->user_badge_fields[$meta][0] != '0'){
            
            $badges_query = new WP_Query( array( 'post_status' => 'publish', 'post__in' => $this->user_badge_fields[$meta] , 'post_type'=> UAIO_USER_BADGES_POST_TYPE ,'posts_per_page' => '-1' ) );
            
        }else{
            $badges_query = new WP_Query( array( 'post_status' => 'publish', 'post_type'=> UAIO_USER_BADGES_POST_TYPE ,'posts_per_page' => '-1' ) );
        
            
        }
        
        if ( $badges_query->have_posts() ) :
                while ($badges_query->have_posts()) : $badges_query->the_post();
                    array_push($loop, array('key' => get_the_ID(), 'value' => get_the_title()));
                endwhile;
            endif;
        
        
        
        
        $profile_user_meta = explode(', ', $value);
        $profile_user_meta = (array) $profile_user_meta;

        // if('' == $profile_user_meta && '' != $country_loop_status && 'countries' == $country_loop_status){
            
        //     $profile_user_meta = $loop[$upme_settings['default_predefined_country']];
        // }

        $display = '';
        if (count($loop) > 0) {
            $display .= '<select multiple ' . $disabled . ' class="upme-chosen-multiple chosen-admin-field-type input" name="upme_user_badge_' . $meta . '[]" id="' . $meta . '">';
            $display .= '<option value="" ' . selected($profile_user_meta[0], "", 0) . '></option>';
            foreach ($loop as $option) {
                $option['value'] = upme_stripslashes_deep(trim($option['value']));

                $selected = '';
                if(in_array($option['key'], $profile_user_meta)){
                    $selected = 'selected';
                }

                $display .= '<option value="' . $option['key'] . '" ' . $selected . '>' . $option['value'] . '</option>';
            }
            $display .= '</select>';
        }
        return $display;

    }

    public function uaio_include_chosen_scripts(){

    	wp_register_script('upme_chosen_js', upme_url . 'admin/js/chosen/chosen.jquery.js');
		wp_enqueue_script('upme_chosen_js');
    
        wp_register_style('upme_chosen_css', upme_url . 'admin/js/chosen/chosen.css');
        wp_enqueue_style('upme_chosen_css');
    }

}