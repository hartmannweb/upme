<?php
/**
 * Manage Google maps field related functionality
 */
class UAIO_Google_Maps_Field{
    
    public $maps;
    public function __construct(){
        $this->maps = array();
    }

    /**
     * Show google maps field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_google_map_edit_field($display,$params){
        
        $this->uaio_maps_init_scripts();

        extract($params);
        $display .= '<input title="' . $name . '" ' . $disabled . ' type="text" class="upme-input upme-textonly uaio-google-map ' . $required_class . ' upme-edit-' . $meta . '" name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '" value="' . $value . '" />';
        return $display;   
    }

    /**
     * Show google maps field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_google_map_view_field($display,$params){

        extract($params);
        
        $this->maps[$meta] = $value;
        
        $this->uaio_maps_init_scripts();
        
        return '<div style="width:100%;height:200px" id="'. $meta .'"></div>';
    }

    /**
     * Show google maps field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_google_map_registration_field($display,$params){
        global $upme;
        extract($params);
        
        $this->uaio_maps_init_scripts();

        $display .= '<input type="text" class="upme-input upme-textonly uaio-google-map ' . $required_class . '" name="' . $meta . '" id="' . $id . '" value="' . $upme->post_value($meta) . '" title="' . $name . '" />';
        return $display;

    }

    /**
     * Show google maps field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_google_map_backend_edit_field($display,$params){
        
        $this->uaio_maps_init_scripts();

        extract($params);

        return '<input type="text" ' . $disabled . ' name="' . $name . '" id="' . $meta . '" value="' . esc_attr($value) . '" class="regular-text upme-textonly uaio-google-map ">';
                
    }
    
    public function uaio_maps_init_scripts(){    
        wp_enqueue_script('uaio-gmaps-api');
        wp_enqueue_script('uaio-common'); 
        
        $conf = array('maps' => $this->maps);
        
        wp_localize_script( 'uaio-common', 'uaioCommon', $conf );
    }
    
   

}