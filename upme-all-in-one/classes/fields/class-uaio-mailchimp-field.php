<?php

class UAIO_Mailchimp_Field{

    public $mailchimp_fields;
    public $api_key;
    public $mailchimp_field_lists;
    
	public function __construct(){
        add_action('upme_user_register',array($this,'uaio_mailchimp_registration'));
        add_action('wp_ajax_uaio_process_mailchimp_subscription', array($this, 'process_mailchimp_subscription'));
        
        $uaio_options = get_option('uaio_options');

        $api_key =  isset($uaio_options['mailchimp']['api_key']) ? $uaio_options['mailchimp']['api_key'] : 'invalid-us1';
        $this->api_key = (strpos($api_key,'-us1')) ? $api_key : $api_key . '-us1';
        
        $this->mailchimp_field_lists = get_option('uaio_mailchimp_field_lists');
	}
    
    public function uaio_mailchimp_registration_field($display,$params){
        global $upme;
        extract($params);
        
        $display .= '<input type="checkbox" class=" ' . $required_class . ' " name="' . $meta . '" id="reg_' . $meta . '" value="1" title="' . $name . '" />';

        return $display;
    }
	
    public function uaio_mailchimp_edit_field($display,$params){
        global $upme;
        extract($params);
        //print_r($params);exit;
        $subscribe_status = get_user_meta($user_id,$meta,true);
        
        $subscribe_button_text = __('Click to Subscribe','uaio');
        if($subscribe_status == '1'){
            $subscribe_button_text = __('Click to Unsubscribe','uaio');
        }
        
        $subscribe_button_text = apply_filters('uaio_mailchimp_subscribe_btn_text',$subscribe_button_text,$subscribe_status, $params);
        
        $display .= '<input type="button" data-user="'.$user_id.'" data-meta="'.$meta.'" class="  uaio_mailchimp_btn upme-button " name="uaio_mailchimp_' . $meta . '" id="uaio_mailchimp_' . $meta . '" value="' . $subscribe_button_text . '" title="' . $name . '" />';

        return $display;
    }

    public function uaio_mailchimp_view_field($display,$params){ 
    
        global $upme;
        extract($params);
        //print_r($params);exit;
        $subscribe_status = get_user_meta($user_id,$meta,true);
        
        $subscribe_button_text = __('Unsubscribed','uaio');
        if($subscribe_status == '1'){
            $subscribe_button_text = __('Subscribed','uaio');
        }
    
        return $subscribe_button_text;
    }

    public function uaio_mailchimp_backend_edit_field($display,$params){
        global $upme;
        extract($params);
        //print_r($params);exit;
        $subscribe_status = get_user_meta($user_id,$meta,true);
        
        $subscribe_button_text = __('Click to Subscribe','uaio');
        if($subscribe_status == '1'){
            $subscribe_button_text = __('Click to Unsubscribe','uaio');
        }
        
        $subscribe_button_text = apply_filters('uaio_mailchimp_subscribe_btn_text',$subscribe_button_text,$subscribe_status, $params);
        
        $display .= '<input type="button" data-user="'.$user_id.'" data-meta="'.$meta.'" class="  uaio_mailchimp_btn button button-primary " name="uaio_mailchimp_' . $meta . '" id="uaio_mailchimp_' . $meta . '" value="' . $subscribe_button_text . '" title="' . $name . '" />';

        return $display;

    }

    public function uaio_include_chosen_scripts(){

    	wp_register_script('upme_chosen_js', upme_url . 'admin/js/chosen/chosen.jquery.js');
		wp_enqueue_script('upme_chosen_js');
    
        wp_register_style('upme_chosen_css', upme_url . 'admin/js/chosen/chosen.css');
        wp_enqueue_style('upme_chosen_css');
    }
    
    public function uaio_mailchimp_registration($user_id){

         $profile_fields = get_option('upme_profile_fields');
         
        
         $MailChimp = new \Drewm\MailChimp($this->api_key);
        
         foreach($profile_fields as $field){
            extract($field);
            if($field == 'mailchimp'){
                $subscription_status =  get_user_meta($user_id,$meta,true);
                $email = get_user_meta($user_id,'user_email',true);
                $first_name = get_user_meta($user_id,'first_name',true);
                $last_name = get_user_meta($user_id,'last_name',true);
                
                if($subscription_status == '1'){
                    
                    
                    $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => $this->mailchimp_field_lists[$meta],
                        'email'             => array('email'=> $email),
                        'merge_vars'        => array('FNAME'=> $first_name, 'LNAME'=> $last_name ),
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,                   
                    
                    ));
                }
            }
         }
    }
    
    public function process_mailchimp_subscription(){
        
        $MailChimp = new \Drewm\MailChimp($this->api_key);
              
        if(isset($_POST['action']) && $_POST['action'] == 'uaio_process_mailchimp_subscription'){
      
            $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : 0;
            $meta = isset($_POST['meta']) ? $_POST['meta'] : 0;
            
            $subscription_status =  get_user_meta($user_id,$meta,true);
            $email = get_user_meta($user_id,'user_email',true);
            $first_name = get_user_meta($user_id,'first_name',true);
            $last_name = get_user_meta($user_id,'last_name',true);
            
            $response = array();

            if($subscription_status != '1'){
                $result = $MailChimp->call('lists/subscribe', array(
                    'id'                => $this->mailchimp_field_lists[$meta],
                    'email'             => array('email'=> $email),
                    'merge_vars'        => array('FNAME'=> $first_name, 'LNAME'=> $last_name ),
                    'double_optin'      => false,
                    'update_existing'   => true,
                    'replace_interests' => false,
                    'send_welcome'      => false,                   

                ));

                if(!isset($result['error'])){
                    $response = array('status' => 'success', 'type' => 'subscribe');
                    update_user_meta($user_id,$meta,'1');
                }else{
                    $response = array('status' => 'error', 'type' => 'subscribe');
                }
                
                
            }else{
                $result = $MailChimp->call('lists/unsubscribe', array(
                    'id'                => $this->mailchimp_field_lists[$meta],
                    'email'             => array('email'=> $email),
                    'delete_member'     => true,
                    'send_goodbye'      => false,
                    'send_notify'       => false               

                ));
                
                if(!isset($result['error']) && isset($result['complete'])){
                    $response = array('status' => 'success', 'type' => 'unsubscribe');
                    update_user_meta($user_id,$meta,'0');
                }else{
                    $response = array('status' => 'error', 'type' => 'unsubscribe');
                }
            }
            echo json_encode($response);exit;
        }
    }
    

}

