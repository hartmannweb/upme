<?php
/**
 * Manage list field related functionality
 */
class UAIO_List_Field{

    /**
     * Show unordered list field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_unordered_list_edit_field($display,$params){

        extract($params);

        $custom_editor_styles = apply_filters('upme_text_editor_styles','',$params);
        $display .= '<textarea title="' . $name . '" ' . $disabled . ' class="upme-input ' . $required_class . ' '.$custom_editor_styles.' " name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '">' . get_the_author_meta($meta, $id) . '</textarea>';
       
        return $display;   
    }

    /**
     * Show unordered list field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_unordered_list_view_field($display,$params){

        extract($params);

        $loop = explode(PHP_EOL, $value);
        if (count($loop) > 0) {
            $display .= '<ul>';
           
            foreach ($loop as $option) {
                $option = upme_stripslashes_deep(trim($option));

                $display .= '<li>' . $option . '</li>';
            }
            $display .= '</ul>';
        }

        return $display;
    }

    /**
     * Show unordered list field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_unordered_list_registration_field($display,$params){
        global $upme;
        extract($params);

        $custom_editor_styles = apply_filters('upme_text_editor_styles','',$params);

        $display .= '<textarea class="upme-input' . $required_class . ' '.$custom_editor_styles.' " name="' . $meta . '" id="reg_' . $meta . '" title="' . $name . '">' . $upme->post_value($meta) . '</textarea>';
       
        return $display;

    }

    /**
     * Show unordered list field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_unordered_list_backend_edit_field($display,$params){

        extract($params);

        $custom_editor_styles = apply_filters('upme_text_editor_styles','',$params);

        return '<textarea ' . $disabled . ' name="upme[' . $meta . ']" id="' . $meta . '" rows="5" cols="30" class="'.$custom_editor_styles.'">' . $value . '</textarea></td>';
               
    }

    /**
     * Show ordered list field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_ordered_list_edit_field($display,$params){

        extract($params);

        $custom_editor_styles = apply_filters('upme_text_editor_styles','',$params);
        $display .= '<textarea title="' . $name . '" ' . $disabled . ' class="upme-input ' . $required_class . ' '.$custom_editor_styles.' " name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '">' . get_the_author_meta($meta, $id) . '</textarea>';
       
        return $display;   
    }

    /**
     * Show ordered list field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_ordered_list_view_field($display,$params){

        extract($params);

        $loop = explode(PHP_EOL, $value);
        if (count($loop) > 0) {
            $display .= '<ol>';
           
            foreach ($loop as $option) {
                $option = upme_stripslashes_deep(trim($option));

                $display .= '<li>' . $option . '</li>';
            }
            $display .= '</ol>';
        }

        return $display;
    }

    /**
     * Show ordered list field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_ordered_list_registration_field($display,$params){
        global $upme;
        extract($params);

        $custom_editor_styles = apply_filters('upme_text_editor_styles','',$params);

        $display .= '<textarea class="upme-input' . $required_class . ' '.$custom_editor_styles.' " name="' . $meta . '" id="reg_' . $meta . '" title="' . $name . '">' . $upme->post_value($meta) . '</textarea>';
       
        return $display;

    }

    /**
     * Show ordered list field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_ordered_list_backend_edit_field($display,$params){

        extract($params);

        $custom_editor_styles = apply_filters('upme_text_editor_styles','',$params);

        return '<textarea ' . $disabled . ' name="upme[' . $meta . ']" id="' . $meta . '" rows="5" cols="30" class="'.$custom_editor_styles.'">' . $value . '</textarea></td>';
                
    }

}