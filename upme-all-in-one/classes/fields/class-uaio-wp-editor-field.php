<?php
/**
 * Manage WYSIWYG Editor field related functionality
 */
class UAIO_WP_Editor{

    /**
     * Show WYSIWYG Editor field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
	public function uaio_wp_editor_edit_field($display,$params){

		extract($params);
        $this->uaio_include_tmce_scripts();
//disabled="disabled"
        ob_start();
        
        if($disabled != ''){
           $display .= '<div style="opacity: 0.5;pointer-events: none;" >';
        }
        // wp_editor(str_replace("\r\n", "<br />", html_entity_decode($value)),$meta);
        wp_editor((html_entity_decode(wpautop($value))),$meta);
        
        $display .= ob_get_contents();
        
        if($disabled != ''){
           $display .= '</div>';
        }
        
        ob_clean();
        return $display;
	}

    /**
     * Show WYSIWYG Editor field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_wp_editor_registration_field($display,$params){

        extract($params);
        
        $this->uaio_include_tmce_scripts();

        ob_start();
        wp_editor('',$meta);
        $display .= ob_get_contents();
        ob_clean();
        return $display;
    }

    /**
     * Show WYSIWYG Editor field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_wp_editor_backend_edit_field($display,$params){

        extract($params);

        ob_start();
        wp_editor(str_replace("\r\n", "<br />", html_entity_decode($value)), 'wp_editor_'.$meta);
        $display .= ob_get_contents();
        ob_clean();
        return $display;
    }

    /**
     * Show WYSIWYG Editor field value on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
	public function uaio_wp_editor_view_field($display,$params){

		extract($params);
        // return str_replace("\r\n", "<br />", html_entity_decode($value)); 
        return html_entity_decode(wpautop($value)); 
	}
    
    public function uaio_include_tmce_scripts(){

    	$lang_strings = upme_tinymce_language_setting();
        
        wp_register_script('upme_tmce', upme_url . 'admin/js/tinymce_language_strings.js');
        wp_enqueue_script('upme_tmce');
        wp_localize_script('upme_tmce', 'UPMETmce', $lang_strings);
    }

}