<?php
/**
 * Manage Date field related functionality
 */
class UAIO_Date_Field{

    /**
     * Show future date field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_future_date_edit_field($display,$params){
        global $uaio_field_type_manager;
        extract($params);
        
        $formatted_date_value = '';
        $date_values = esc_attr($value);
        if('' != $date_values){
            $upme_settings = $uaio_field_type_manager->current_option;
            $upme_date_format = (string) isset($upme_settings['date_format']) ? $upme_settings['date_format'] : 'mm/dd/yy';

            $formatted_date_value = upme_date_format_to_custom($date_values, $upme_date_format);
        }
        
        $display .= '<input readonly="readonly" title="' . $name . '" ' . $disabled . ' type="text" class="upme-input uaio_future_date ' . $required_class . '" name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '" value="' . $formatted_date_value . '" />';
        $display .= '<input type="button" class="upme-button-alt uaio_future_date_reset" value="'.__('Clear Date','upme').'" />';
 
        return $display; 

  
    }

    /**
     * Show future date field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_future_date_view_field($display,$params){
        global $uaio_field_type_manager;
        extract($params);

        $upme_settings = $uaio_field_type_manager->current_option;
        $upme_date_format = (string) isset($upme_settings['date_format']) ? $upme_settings['date_format'] : 'mm/dd/yy';

        $value = upme_date_format_to_custom($value, $upme_date_format);
        return $value;
    }

    /**
     * Show future date field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_future_date_registration_field($display,$params){
        global $upme;
        extract($params);


        $display .= '<input type="text" readonly="readonly" class="upme-input' . $required_class . ' uaio_future_date" name="' . $meta . '" id="reg_' . $meta . '" value="' . $upme->post_value($meta) . '" title="' . $name . '" />';
        $display .= '<input type="button" class="upme-button-alt uaio_future_date_reset" value="'.__('Clear Date','upme').'" />';
                               
        return $display;

    }

    /**
     * Show future date field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_future_date_backend_edit_field($display,$params){
        global $uaio_field_type_manager;
        extract($params);

        $formatted_date_value = '';
        $date_values = esc_attr($value);
        if('' != $date_values){
            $upme_settings = $uaio_field_type_manager->current_option;
            $upme_date_format = (string) isset($upme_settings['date_format']) ? $upme_settings['date_format'] : 'mm/dd/yy';

            $formatted_date_value = upme_date_format_to_custom($date_values, $upme_date_format);
        }

        
        $display .='<input readonly="readonly" type="text" ' . $disabled . ' name="upme[' . $meta . ']" id="' . $meta . '" value="' . $formatted_date_value . '" class="regular-text uaio_future_date">';
        $display .= '<input type="button" class="upme-button-alt uaio_future_date_reset" value="'.__('Clear Date','upme').'" />';

        return $display;
                
    }

    /**
     * Show past date field on profile edit
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_past_date_edit_field($display,$params){
        global $uaio_field_type_manager;
        extract($params);

        $formatted_date_value = '';
        $date_values = esc_attr($value);
        if('' != $date_values){
            $upme_settings = $uaio_field_type_manager->current_option;
            $upme_date_format = (string) isset($upme_settings['date_format']) ? $upme_settings['date_format'] : 'mm/dd/yy';

            $formatted_date_value = upme_date_format_to_custom($date_values, $upme_date_format);
        }
        
        $display .= '<input readonly="readonly" title="' . $name . '" ' . $disabled . ' type="text" class="upme-input uaio_past_date ' . $required_class . '" name="' . $meta . '-' . $id . '" id="' . $meta . '-' . $id . '" value="' . $formatted_date_value . '" />';
        $display .= '<input type="button" class="upme-button-alt uaio_past_date_reset" value="'.__('Clear Date','upme').'" />';
 
        return $display;                       
    }

    /**
     * Show past date field on profile view
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_past_date_view_field($display,$params){
        global $uaio_field_type_manager;
        extract($params);

        $upme_settings = $uaio_field_type_manager->current_option;
        $upme_date_format = (string) isset($upme_settings['date_format']) ? $upme_settings['date_format'] : 'mm/dd/yy';

        $value = upme_date_format_to_custom($value, $upme_date_format);
        return $value;
    }

    /**
     * Show past date field on registration
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_past_date_registration_field($display,$params){
        global $upme;
        extract($params);

        $display .= '<input type="text" readonly="readonly" class="upme-input' . $required_class . ' uaio_past_date" name="' . $meta . '" id="reg_' . $meta . '" value="' . $upme->post_value($meta) . '" title="' . $name . '" />';
        $display .= '<input type="button" class="upme-button-alt uaio_past_date_reset" value="'.__('Clear Date','upme').'" />';
                               
        return $display;

    }

    /**
     * Show past date field on profile edit on backend
     *
     * @param string    $display exsitng HTML generated from other filters
     * @param array     $params  dynamic params
     * @return string   $display updated HTML for custom field
     */
    public function uaio_past_date_backend_edit_field($display,$params){
        global $uaio_field_type_manager;
        extract($params);
        $formatted_date_value = '';
        $date_values = esc_attr($value);
        if('' != $date_values){
            $upme_settings = $uaio_field_type_manager->current_option;
            $upme_date_format = (string) isset($upme_settings['date_format']) ? $upme_settings['date_format'] : 'mm/dd/yy';

            $formatted_date_value = upme_date_format_to_custom($date_values, $upme_date_format);
        }

        
        $display .='<input readonly="readonly" type="text" ' . $disabled . ' name="upme[' . $meta . ']" id="' . $meta . '" value="' . $formatted_date_value . '" class="regular-text uaio_past_date">';
        $display .= '<input type="button" class="upme-button-alt uaio_past_date_reset" value="'.__('Clear Date','upme').'" />';

        return $display;
                                 
        
    }

}