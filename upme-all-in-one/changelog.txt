Version 1.9 (2016.10.01 -  UPME 2.2.01)
* Fixed Bug   : issue with missing Google Map API Key setting
* Fixed Bug   : issue in WP Editor field tags
* Fixed Bug   : issue with profile header date fields
* Fixed Bug   : issue with textonly field validation

Version 1.8 (UPME 2.1.13)
* Fixed Bug   : issue with all profile tabs not loading on profile
* Added       : Settings to enable header fields in compact view

Version 1.7 (UPME 2.1.11)
* Fixed Bug   : issue with custom field type conflicts with other addons
* Fixed Bug   : issue with uploaded files not displaying for public

Version 1.6 (UPME 2.1.06)
* Fixed Bug   : issue with profile tabs

Version 1.5 (UPME 2.1.05)
* Added 	  : file upload field support on registration
* Fixed Bug   : issue with User can Edit in Wp Editor field

Version 1.4 (UPME 2.1.01)
* New Feature : Implement support for profile tab fields
* New Feature : Manage multiple registration forms
* Fixed Bug   : issue in registration form query
* Fixed Bug   : issue in user listing orderby field
* Fixed Bug   : issue in user listing user roles
* Fixed Bug   : invalid help messages
* Fixed Bug   : PHP notice on profile form

Version 1.1 (UPME 2.0.26)
* New Feature : Manage multiple user listings
* New Feature : Manage multiple registration forms
* New Feature : Manage dynamic profile displays
* New Feature : User Badges manager
* Fixed Bug   : conflicts with other social login plugins

Version 1.0 (UPME 2.0.25)
* Social login with Facebook, Twitter and LinkedIn
* Custom field types
* Profile Tabs
