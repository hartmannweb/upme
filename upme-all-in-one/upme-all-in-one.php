<?php
/*
  Plugin Name: UPME All In One
  Plugin URI: http://upmeaddons.innovativephp.com/upme-all-in-one
  Description: An awesome addon for wide range of bonus features for User Profiles Made Easy
  Version: 1.9
  Author: Rakhitha Nimesh
  Author URI: http://upmeaddons.innovativephp.com/
 */

 
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

function uaio_get_plugin_version() {
    $default_headers = array('Version' => 'Version');
    $plugin_data = get_file_data(__FILE__, $default_headers, 'plugin');
    return $plugin_data['Version'];
}

/* Validating existence of required plugins */
add_action( 'plugins_loaded', 'uaio_plugin_init' );

function uaio_plugin_init(){
    if(!class_exists('UPME')){
        add_action( 'admin_notices', 'uaio_plugin_admin_notice' );
    }else{
        
    }
}

function uaio_plugin_admin_notice() {
   $message = __('<strong>UPME All In One Addon</strong> requires <strong>User Profiles Made Easy</strong> plugin to function properly','upmeinc');
   echo '<div class="error"><p>'.$message.'</p></div>';
}

if( !class_exists( 'UPME_All_In_One' ) ) {
    
    class UPME_All_In_One{
    
        private static $instance;

        public static function instance() {
            
            if ( ! isset( self::$instance ) && ! ( self::$instance instanceof UPME_All_In_One ) ) {
                self::$instance = new UPME_All_In_One();
                self::$instance->setup_constants();

                add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
                self::$instance->includes();
                
                add_action('admin_enqueue_scripts',array(self::$instance,'load_admin_scripts'),9);
                add_action('wp_enqueue_scripts',array(self::$instance,'load_scripts'),9);
                 
                self::$instance->template_loader    = new UAIO_Template_Loader();
                self::$instance->settings           = new UAIO_Settings();
                self::$instance->profile_tabs       = new UAIO_Profile_Tabs();
                
                self::$instance->private_content    = new UAIO_Private_Content();
                self::$instance->registration_forms = new UAIO_Registration_Forms();
                self::$instance->profile_forms      = new UAIO_Profile_Forms();
                self::$instance->users              = new UAIO_Users();
                self::$instance->user_listings      = new UAIO_User_Listings();
                self::$instance->profile_badges      = new UAIO_User_Profile_Badges();
                self::$instance->profile_fields      = new UAIO_User_Profile_Fields();
                self::$instance->profile_tab_fields  = new UAIO_Profile_Tab_Fields();
            }
            return self::$instance;
        }

        public function setup_constants() {
            if ( ! defined( 'UAIO_USER_BADGES_POST_TYPE' ) ) {
                define( 'UAIO_USER_BADGES_POST_TYPE', 'uaio_profile_badge' );
            }
            if ( ! defined( 'UAIO_USER_REG_FORMS_POST_TYPE' ) ) {
                define( 'UAIO_USER_REG_FORMS_POST_TYPE', 'uaio_register_forms' );
            }
            if ( ! defined( 'UAIO_USER_USER_LISTING_POST_TYPE' ) ) {
                define( 'UAIO_USER_USER_LISTING_POST_TYPE', 'uaio_user_listing' );
            }
            if ( ! defined( 'UAIO_USER_USER_PROFILE_POST_TYPE' ) ) {
                define( 'UAIO_USER_USER_PROFILE_POST_TYPE', 'uaio_user_profile' );
            }
            
        }
        
        public function load_scripts(){
            global $uaio_gmaps_status;

            $uaio_settings = get_option('uaio_options');
            $google_maps_api_key = isset($uaio_settings['general']['google_map_api_key']) ? $uaio_settings['general']['google_map_api_key'] : '';
            

            wp_register_script('uaio-gmaps-api', 'http://maps.googleapis.com/maps/api/js?key='.$google_maps_api_key.'&libraries=places');
            
            wp_register_script('uaio-front', UAIO_PLUGIN_URL . 'js/uaio-front.js', array('jquery'));
            wp_enqueue_script('uaio-front');
            
            $front_params = array(

                'Messages' => array(
                    'Processing' => __('Processing.', 'uaio'),
                    'ClickToUnsubscribe' => __('Click to Unsubscribe.', 'uaio'),
                    'ClickToSubscribe' => __('Click to Subscribe.', 'uaio'),
                ),
                'AdminAjax' => admin_url('admin-ajax.php'),
            );
            wp_localize_script('uaio-front', 'UAIOFront', $front_params);
            
            wp_register_script('uaio-common', UAIO_PLUGIN_URL . 'js/uaio-fields-common.js', array('jquery'));
            
            wp_register_style('uaio-front-style', UAIO_PLUGIN_URL . 'css/uaio-front.css');
            wp_enqueue_style('uaio-front-style');
           
        }
        
        public function load_admin_scripts(){
            /* Add scripts for shortcodes */  
            
            if(is_admin()){

                $uaio_settings = get_option('uaio_options');
                $google_maps_api_key = isset($uaio_settings['general']['google_maps_api_key']) ? $uaio_settings['general']['google_maps_api_key'] : '';
            
            
                wp_register_style('uaio-admin-css', UAIO_PLUGIN_URL . 'css/uaio-admin.css');
                wp_enqueue_style('uaio-admin-css');

                wp_register_script('uaio-admin', UAIO_PLUGIN_URL . 'js/uaio-admin.js');
	            wp_enqueue_script('uaio-admin');
                
            
                $params = array('regFieldEmpty' => __('Please select a field','uaio'),
                                'fieldAdded' => __('Field already added.','uaio'),
                                'Messages' => array(
                                    'Processing' => __('Processing.', 'uaio'),
                                    'ClickToUnsubscribe' => __('Click to Unsubscribe.', 'uaio'),
                                    'ClickToSubscribe' => __('Click to Subscribe.', 'uaio'),
                                ),
                                'AdminAjax' => admin_url('admin-ajax.php'),
                               
                               );
                wp_localize_script('uaio-admin','uaioAdmin',$params);
    
                wp_register_script('uaio-gmaps-api', 'http://maps.googleapis.com/maps/api/js?key='.$google_maps_api_key.'&libraries=places');
                wp_register_script('uaio-common', UAIO_PLUGIN_URL . 'js/uaio-fields-common.js', array('jquery'));
                
//                wp_register_style('uaio-social-icons', UAIO_PLUGIN_URL . 'css/social/css/style.css');
//                wp_enqueue_style('uaio-social-icons');
            }

        }
        
        private function includes() {
            
            require_once UAIO_PLUGIN_DIR . 'functions.php';
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-upgrade.php';
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-template-loader.php';      
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-settings.php'; 
            
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-profile-tabs.php'; 
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-private-content.php'; 
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-registration-forms.php'; 
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-profile-forms.php';
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-user-listings.php';
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-users.php';
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-user-profile-badges.php';
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-user-profile-fields.php';
            require_once UAIO_PLUGIN_DIR . 'lib/Drewm/MailChimp.php';
            require_once UAIO_PLUGIN_DIR . 'classes/class-uaio-profile-tab-fields.php'; 

            if ( is_admin() ) {
            }
        }

        public function load_textdomain() {
            
        }
        
    }
}

// Plugin version
if ( ! defined( 'UAIO_VERSION' ) ) {
    define( 'UAIO_VERSION', '1.9' );
}

// Plugin Folder Path
if ( ! defined( 'UAIO_PLUGIN_DIR' ) ) {
    define( 'UAIO_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Plugin Folder URL
if ( ! defined( 'UAIO_PLUGIN_URL' ) ) {
    define( 'UAIO_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

require_once UAIO_PLUGIN_DIR . 'upme-field-types.php'; 

function UPME_All_In_One() {
    global $uaio;
	$uaio = UPME_All_In_One::instance();
}

UPME_All_In_One();




