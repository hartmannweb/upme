<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function uaio_get_template($main,$sub = ''){
    global $uaio;
    
    $template = $main;
    if($sub != ''){
        $template = $template . '-' . $sub;
    }
    
    return $uaio->template_loader->get_template_part($template);
}

/**
 * Common function for enablin translation strings for the plugin domain
 *
 * @access public
 * @since 1.0
 * @param string $string String to be translated
 * @param string $domain Plugin textdomain
 * @return void 
 */
function uaio_language_entry($string,$domain = 'uaio'){
	return __($string,$domain);
}

add_filter( 'upme_profile_display_fields','upme_profile_display_fields',10,2);
function upme_profile_display_fields($profile_fields, $profile_fields_params){
    global $upme_options;
    
    $uaio_options = get_option('uaio_options');
    
    $data = isset($uaio_options['header_fields']) ? $uaio_options['header_fields'] : array();
    $tab_fields_list = isset($data['tab_fields_list']) ? $data['tab_fields_list'] : array();

    foreach($tab_fields_list as $k => $v ){
        $key_index = $upme_options->upme_profile_fields_by_key[$v]['key_index'];
        if(isset($profile_fields[$key_index])){
            unset($profile_fields[$key_index]);
        }    
    }
    
    return $profile_fields;
}

 /* TODO 1.8 
    - Add setting for zoom level google map
    - Add support for upme_backend_select_field_custom_values filters for all select field types
  */
