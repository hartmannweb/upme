<?php

class UPCM_Settings{
    
    public function __construct(){
        
        add_action('admin_menu', array(&$this, 'add_menu'), 9);
        add_action('admin_init', array($this,'save_settings_page') );
        
    }
    
    public function add_menu(){
        
     add_submenu_page('upme-settings', __("UPME Contacts", "upme"), __("UPME Contacts", "upme"),'manage_options','upcm-contacts-manager',array(&$this,'contact_settings'));
        //add_menu_page(__('UPME Contacts', "upcm"), __("UPME Contacts", "upcm"),'manage_options','upcm-contacts-manager',array(&$this,'contact_settings'));
    }
    
    public function contact_settings(){
        global $upcm,$upcm_settings_data;
        
        add_settings_section( 'upcm_section_contacts_general', __('General Settings','upcm'), array( &$this, 'filter_section_general_desc' ), 'upcm-contacts-manager' );
        
//        add_settings_section( 'upcm_section_search_features', __('Features Settings','upcm'), array( &$this, 'filter_section_general_desc' ), 'upcm-contacts-manager' );
        
        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'upcm_section_contacts_general';
        $upcm_settings_data['tab'] = $tab;
        
        $tabs = $this->plugin_options_tabs('upcm_contacts',$tab);
   
        $upcm_settings_data['tabs'] = $tabs;
        
        $tab_content = $this->plugin_options_tab_content($tab);
        $upcm_settings_data['tab_content'] = $tab_content;
        
        ob_start();
		$upcm->template_loader->get_template_part( 'menu-page-container');
		$display = ob_get_clean();
		echo $display;
        
    
    }

    
    public function plugin_options_tabs($type,$tab) {
        $current_tab = $tab;
        $this->plugin_settings_tabs = array();
        
        switch($type){
            case 'upcm_contacts':
                $this->plugin_settings_tabs['upcm_section_contacts_general']  = __('General Settings','upcm');
//                $this->plugin_settings_tabs['upcm_section_search_features']  = __('Features Settings','upcm');
                break;

        }
        
        ob_start();
        ?>

        <h2 class="nav-tab-wrapper">
        <?php 
            foreach ( $this->plugin_settings_tabs as $tab_key => $tab_caption ) {
            $active = $current_tab == $tab_key ? 'nav-tab-active' : '';
            $page = isset($_GET['page']) ? $_GET['page'] : '';
        ?>
                <a class="nav-tab <?php echo $active; ?> " href="?page=<?php echo $page; ?>&tab=<?php echo $tab_key; ?>"><?php echo $tab_caption; ?></a>
            
        <?php } ?>
        </h2>

        <?php
                
        return ob_get_clean();
    }
    
    public function plugin_options_tab_content($tab,$params = array()){
        global $upcm,$upcm_settings_data;
        
        $upcm_options = get_option('upcm_options');
        
        ob_start();
        switch($tab){

            
            case 'upcm_section_contacts_general':                
	            $data = isset($upcm_options['contacts_general']) ? $upcm_options['contacts_general'] : array();
           
                $upcm_settings_data['google_maps_api_key'] = isset($data['google_maps_api_key']) ? $data['google_maps_api_key'] : '';
                
                $upcm_settings_data['contact_profile_tab_status'] = isset($data['contact_profile_tab_status']) ? $data['contact_profile_tab_status'] : '0';
                $upcm_settings_data['contact_form_status'] = isset($data['contact_form_status']) ? $data['contact_form_status'] : '0';
                $upcm_settings_data['contact_google_map_status'] = isset($data['contact_google_map_status']) ? $data['contact_google_map_status'] : '0';
                $upcm_settings_data['contact_address_status'] = isset($data['contact_address_status']) ? $data['contact_address_status'] : '0';
                $upcm_settings_data['contact_email_status'] = isset($data['contact_email_status']) ? $data['contact_email_status'] : '0';
                $upcm_settings_data['contact_telephone_status'] = isset($data['contact_telephone_status']) ? $data['contact_telephone_status'] : '0';
            
                $upcm_settings_data['tab'] = $tab;
            
                $upcm->template_loader->get_template_part('contacts-general-settings');            
                break;
            
        }
        
        $display = ob_get_clean();
        return $display;
        
    }
    
    public function save_settings_page(){
        
        $upcm_settings_pages = array('upcm-contacts-manager');
        if(isset($_POST['upcm_tab']) && isset($_GET['page']) && in_array($_GET['page'],$upcm_settings_pages)){
            $tab = '';
            if ( isset ( $_POST['upcm_tab'] ) )
               $tab = $_POST['upcm_tab']; 

            if($tab != ''){
                $func = 'save_'.$tab;
                $this->$func();
            }
        }
    }

    
    public function save_upcm_section_contacts_general(){
        
        if(isset($_POST['upcm_contacts_general'])){
            foreach($_POST['upcm_contacts_general'] as $k=>$v){
                $this->settings[$k] = $v;
            }            
            
        }
        
        $upcm_options = get_option('upcm_options');
        $upcm_options['contacts_general'] = $this->settings;
        update_option('upcm_options',$upcm_options);
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );  

        
    } 
    
    
    public function admin_notices(){
        ?>
        <div class="updated">
          <p><?php esc_html_e( 'Settings saved successfully.', 'coupinewp' ); ?></p>
       </div>
        <?php
    }

    
}


