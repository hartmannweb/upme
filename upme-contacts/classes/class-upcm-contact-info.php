<?php

class UPCM_Contact_Info{
    
    public $upcm_options;
    
    public function __construct(){
        $this->upcm_options = get_option('upcm_options');
        $this->current_user = get_current_user_id();
        
        $this->contacts_general_settings = isset($this->upcm_options['contacts_general']) ? $this->upcm_options['contacts_general'] :  array();
        
        add_filter('upme_profile_tab_items', array($this,'profile_tab_items'),10,2);
        add_filter('upme_profile_view_forms',array($this,'profile_view_forms'),10,2);
        
        add_shortcode('upcm_contacts_panel', array($this,'contacts_panel'));
        
        add_action('wp_ajax_upcm_contact_user', array($this,'upcm_contact_user'));
        add_action('wp_ajax_nopriv_upcm_contact_user', array($this,'upcm_contact_user'));
        
        add_action('wp_ajax_upcm_contact_user_settings', array($this,'upcm_contact_user_settings'));
    }
    
    public function profile_tab_items($display,$params){
//        if( is_user_logged_in() && $this->current_user && $this->current_user == $params['id'] &&
//            $this->contacts_general_settings['contact_profile_tab_status']){
        extract($params);
        $upcm_contact_settings = get_user_meta($id,'upcm_contact_settings',true);
        $tab_display_status = isset($upcm_contact_settings['tab_display_status']) ? $upcm_contact_settings['tab_display_status'] : '1';
//echo "<pre>";print_r($upcm_contact_settings);exit;
        $userid = get_current_user_id();
        
        if( $this->contacts_general_settings['contact_profile_tab_status']){
            if( (is_user_logged_in() && $userid == $id) || $tab_display_status ){
                $display .= '<div class="upme-profile-tab" data-tab-id="upme-contacts-panel" >
                            <i class="upme-profile-icon upme-icon-envelope"></i>
                        </div>';
            }
        }

        return $display;
    }

    public function profile_view_forms($display,$params){
        extract($params);
        if($view != 'compact' && $hide_profile_tabs != 'yes' ){
            $upcm_contact_settings = get_user_meta($id,'upcm_contact_settings',true);
            $tab_display_status = isset($upcm_contact_settings['tab_display_status']) ? $upcm_contact_settings['tab_display_status'] : '1';
            
            $userid = get_current_user_id();
            $upcm_contacts_data['location']   = isset($upcm_contact_settings['location']) ? $upcm_contact_settings['location'] : '';
            
      
            $location   = isset($upcm_contact_settings['location']) ? $upcm_contact_settings['location'] : '';
            
            if( $this->contacts_general_settings['contact_profile_tab_status'] ){
                if( (is_user_logged_in() && $userid == $id) || $tab_display_status ){
                    $style = 'style="display:block;"';
                    if($location == ''){
                        $style = 'style="display:none;"';
                    }
                    
                    $display .= '<div data-load-map-status="ACTIVE" class="upme-contacts-panel upme-profile-tab-panel" ' . $style . ' >
                                    '.do_shortcode("[upcm_contacts_panel user_id='".$id."']").'       
                                </div>';
                }
            }
        }

        return $display;
    }
    
    public function contacts_panel($atts){
        global $upcm,$upcm_contacts_data;
        
        $atts = shortcode_atts(
                    array(
                        'user_id' => '0',
                    ), $atts, 'upcm_contacts_panel' );
        extract($atts);
        
        
        $userid = get_current_user_id();
        $upcm_contacts_data = array();         
        
        $upcm_contact_settings = get_user_meta($user_id,'upcm_contact_settings',true);
        
        $upcm_contacts_data['settings_panel_status'] = FALSE; 
        if(is_user_logged_in() && $userid  == $user_id){
            $upcm_contacts_data['settings_panel_status'] = TRUE; 
        }
        
        $upcm_contacts_data['user_id'] = $userid;
        $upcm_contacts_data['profile_id'] = $user_id; 
        $upcm_contacts_data['location']   = isset($upcm_contact_settings['location']) ? $upcm_contact_settings['location'] : '';
        $upcm_contacts_data['email']      = isset($upcm_contact_settings['email']) ? $upcm_contact_settings['email'] : '';
        $upcm_contacts_data['telephone']  = isset($upcm_contact_settings['telephone']) ? $upcm_contact_settings['telephone'] : '';
        $upcm_contacts_data['address']    = isset($upcm_contact_settings['address']) ? $upcm_contact_settings['address'] : '';
        $upcm_contacts_data['form_display_status']    = isset($upcm_contact_settings['form_display_status']) ? $upcm_contact_settings['form_display_status'] : '1';
        $upcm_contacts_data['tab_display_status']     = isset($upcm_contact_settings['tab_display_status']) ? $upcm_contact_settings['tab_display_status'] : '1';

        $upcm_options = $this->upcm_options;
        $data = isset($upcm_options['contacts_general']) ? $upcm_options['contacts_general'] : array();
   
        $upcm_contacts_data['contact_profile_tab_status'] = isset($data['contact_profile_tab_status']) ? $data['contact_profile_tab_status'] : '0';
        $upcm_contacts_data['contact_form_status'] = isset($data['contact_form_status']) ? $data['contact_form_status'] : '0';
        $upcm_contacts_data['contact_google_map_status'] = isset($data['contact_google_map_status']) ? $data['contact_google_map_status'] : '0';
        $upcm_contacts_data['contact_address_status'] = isset($data['contact_address_status']) ? $data['contact_address_status'] : '0';
        $upcm_contacts_data['contact_email_status'] = isset($data['contact_email_status']) ? $data['contact_email_status'] : '0';
        $upcm_contacts_data['contact_telephone_status'] = isset($data['contact_telephone_status']) ? $data['contact_telephone_status'] : '0';
            
        if (!wp_script_is('uaio-common')) {
            wp_enqueue_script('upcm-gmaps-api'); 
            wp_register_script('upcm-gmaps', UPCM_PLUGIN_URL . 'js/upcm-gmaps.js', array('jquery'));         
            wp_enqueue_script('upcm-gmaps');           
        }else{
            wp_enqueue_script('upcm-gmaps');
        }
        
        $this->maps = '';
        if($upcm_contacts_data['location'] != '' && $upcm_contacts_data['contact_google_map_status']  != '0'){
          $this->maps['upcm_contact_map'] = $upcm_contacts_data['location'];  
        }
        
        $conf = array('maps' => $this->maps);        
        wp_localize_script( 'upcm-gmaps', 'upcmFrontMaps', $conf );
        
        
        
        ob_start();
        $upcm->template_loader->get_template_part('contacts-panel');        
        $display = ob_get_clean();
        return $display;
    }
    
    public function upcm_contact_user(){
        $response = array();
        
        if(check_ajax_referer( 'upcm-contacts', 'verify_nonce',false )){
            $name       = isset($_POST['name']) ? $_POST['name'] : '';
            $email      = isset($_POST['email']) ? $_POST['email'] : '';
            $subject    = isset($_POST['subject']) ? $_POST['subject'] : '';
            $message    = isset($_POST['message']) ? $_POST['message'] : '';
            $user_id    = isset($_POST['user_id']) ? $_POST['user_id'] : '0';
            
            if($name == '' || $email == '' || $subject == '' || $message == '' ){
                $response['status'] = 'fail';
                $response['msg'] = __('Please fill the required fields.','upcm');
            }else if(!is_email($email)){
                $response['status'] = 'fail';
                $response['msg'] = __('Email is invalid.','upcm');
            }else{
                // Check if user has enabled contacting him
                
                $this->upcm_mail_from = $email;
                $this->upcm_mail_from_name = $name;
                
                add_filter('wp_mail_from', array($this,'upcm_mail_from'));
                add_filter('wp_mail_from_name', array($this,'upcm_mail_from_name'));               
                
                $user = get_user_by( 'id', $user_id );
                if($user){
                    $user_email = $user->data->user_email;
                    if( wp_mail( $user_email, $subject, $message )){
                        $response['status'] = 'success';
                        $response['msg'] = __('Email successfully sent.','upcm');
                    }else{
                        $response['status'] = 'fail';
                        $response['msg'] = __('Email sending failed.','upcm');
                    }
                }else{
                    $response['status'] = 'fail';
                    $response['msg'] = __('Invalid user','upcm');
                }               
                
            }
            
        }else{
            $response['status'] = 'fail';
            $response['msg'] = __('Nonce validation failed','upcm');
        }
        
        echo json_encode($response);exit;
    }
    
    public function upcm_mail_from($old) {
        return $this->upcm_mail_from;
    }
    
    public function upcm_mail_from_name($old) {
        return $this->upcm_mail_from_name;
    }
    
    public function upcm_contact_user_settings(){
        $response = array();
        
        if(check_ajax_referer( 'upcm-contacts', 'verify_nonce',false )){
            $contact_settings['location']   = isset($_POST['location']) ? $_POST['location'] : '';
            $contact_settings['email']      = isset($_POST['email']) ? $_POST['email'] : '';
            $contact_settings['telephone']  = isset($_POST['telephone']) ? $_POST['telephone'] : '';
            $contact_settings['address']    = isset($_POST['address']) ? $_POST['address'] : '';
            $contact_settings['form_display_status']    = isset($_POST['form_display_status']) ? $_POST['form_display_status'] : '1';
            $contact_settings['tab_display_status']     = isset($_POST['tab_display_status']) ? $_POST['tab_display_status'] : '1';
            
            if(trim($contact_settings['email']) != '' && !is_email($contact_settings['email'] )){
                $response['status'] = 'fail';
                $response['msg'] = __('Email is invalid.','upcm');
            }else{
                // Check if user has enabled contacting him
                if(is_user_logged_in()){
                    $user_id = get_current_user_id();
                    update_user_meta($user_id,'upcm_contact_settings',$contact_settings);
                    
                    $response['status'] = 'success';
                    $response['msg'] = __('Settings saved successfully.','upcm');
                }else{
                    $response['status'] = 'fail';
                    $response['msg'] = __('Invalid user','upcm');
                }                
            }
            
        }else{
            $response['status'] = 'fail';
            $response['msg'] = __('Nonce validation failed','upcm');
        }
        
        echo json_encode($response);exit;
    }
}
?>