jQuery(document).ready(function($) {   
    $('#upcm_contacts_profile_tab_status').change(function(){
        if($(this).val() == '1'){
            $('#upcm_contacts_form_status').parent().parent().show();
            $('#upcm_contacts_google_map_status').parent().parent().show();
            $('#upcm_contacts_address_status').parent().parent().show();
            $('#upcm_contacts_email_status').parent().parent().show();
            $('#upcm_contact_telephone_status').parent().parent().show();
        }else{
            $('#upcm_contacts_form_status').parent().parent().hide();
            $('#upcm_contacts_google_map_status').parent().parent().hide();
            $('#upcm_contacts_address_status').parent().parent().hide();
            $('#upcm_contacts_email_status').parent().parent().hide();
            $('#upcm_contact_telephone_status').parent().parent().hide();
        }        
    });    
});

