jQuery(document).ready(function($) {  

   
    
    $('#upcm_contact_form_btn').click(function(){
        var err = 0;
        var err_msg = '';
        
        $('#upcm-contact-form-msg').removeClass('upme-success').removeClass('upme-error').hide();
        var tagStart = '<span class="upme-error upme-error-block"><i class="upme-icon upme-icon-remove"></i>';  
        var tagEnd   = '</span>';
        
        if($.trim($('#upcm_contact_form_name').val()) == ''){
            err_msg += tagStart + upcmFront.messages.nameRequired + tagEnd;
        }
        if($.trim($('#upcm_contact_form_email').val()) == ''){
            err_msg += tagStart + upcmFront.messages.emailRequired + tagEnd;
        }
        if($.trim($('#upcm_contact_form_subject').val()) == ''){
            err_msg += tagStart + upcmFront.messages.subjectRequired + tagEnd;
        }
        if($.trim($('#upcm_contact_form_message').val()) == ''){
            err_msg += tagStart + upcmFront.messages.messageRequired + tagEnd;
        }
        
        if($.trim(err_msg) != ''){
            $('#upcm-contact-form-msg').removeClass('upme-success').addClass('upme-errors').html(err_msg).show();
            return;
        }else{
            // check_ajax_referrer
            $('.upcm-loading').show();
            jQuery.post(
                upcmFront.AdminAjax,
                {
                    'action': 'upcm_contact_user',
                    'user_id':   $('#upcm_contact_user_id').val(),
                    'name':   $('#upcm_contact_form_name').val(),
                    'email':   $('#upcm_contact_form_email').val(),
                    'subject':   $('#upcm_contact_form_subject').val(),
                    'message':   $('#upcm_contact_form_message').val(),
                    'verify_nonce': upcmFront.nonce,
                },
                function(response){

                    if(response.status == 'success'){
                        $('#upcm-contact-form-msg').html(response.msg).removeClass('upme-errors').addClass('upme-success').show();
                    }else{
                        $('#upcm-contact-form-msg').removeClass('upme-success').addClass('upme-errors').html(response.msg).show();
                    }
                    
                    $('.upcm-loading').hide();
                },"json");
        }        
    });
    
    $('#upcm_contact_settings_form_btn').click(function(){
        var err = 0;
        var err_msg = '';
        var email_reg = /^([a-zA-Z0-9+_\.\-])+\@(([a-zA-Z0-9+\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
        $('#upcm-contact-settings-msg').removeClass('upme-success').removeClass('upme-errors').hide();
        var tagStart = '<span class="upme-error upme-error-block"><i class="upme-icon upme-icon-remove"></i>';  
        var tagEnd   = '</span>';

        var user_email = $('#upcm_contact_settings_email').val();
        if($.trim(user_email) != '' && !email_reg.test(user_email)){
            err_msg += tagStart + upcmFront.messages.invalidEmail + tagEnd;
        }

        
        if($.trim(err_msg) != ''){
            $('#upcm-contact-settings-msg').removeClass('upme-success').addClass('upme-errors').html(err_msg).show();
            return;
        }else{
            // check_ajax_referrer
            $('.upcm-loading').show();
            jQuery.post(
                upcmFront.AdminAjax,
                {
                    'action'                :   'upcm_contact_user_settings',
                    'location'              :   $('#upcm_contact_settings_location').val(),
                    'email'                 :   $('#upcm_contact_settings_email').val(),
                    'telephone'             :   $('#upcm_contact_settings_telephone').val(),
                    'address'               :   $('#upcm_contact_settings_address').val(),
                    'form_display_status'   :   $('#upcm_contact_settings_form_status').val(),
                    'tab_display_status'    :   $('#upcm_contact_settings_tab_status').val(),
                    'verify_nonce'          :   upcmFront.nonce,
                },
                function(response){

                    if(response.status == 'success'){
                        $('#upcm-contact-settings-msg').html(response.msg).removeClass('upme-errors').addClass('upme-success').show();
                    }else{
                        $('#upcm-contact-settings-msg').removeClass('upme-success').addClass('upme-errors').html(response.msg).show();
                    }
                    
                    $('.upcm-loading').hide();
                },"json");
        }        
    }); 
    
    $('.upcm-contact-control-item').click(function(){
        $('.upcm-contact-control-item').removeClass('upcm-active');
        $(this).addClass('upcm-active');
        $('.upcm-contact-tabs').hide();
        
        var panel_id = $(this).attr('id');
        if(panel_id == 'upcm-contact-info-button'){
            $('#upcm-contact-container').show();
        }else if(panel_id == 'upcm-contact-settings-button'){
            $('#upcm-contact-settings-container').show();
        }
    });
    

});

