<?php 
    global $upcm_settings_data; 
    extract($upcm_settings_data);


    $contact_profile_tab_status = isset($contact_profile_tab_status) ? $contact_profile_tab_status : '0';
    $contact_form_status = isset($contact_form_status) ? $contact_form_status : '0';

    $contact_google_map_status = isset($contact_google_map_status) ? $contact_google_map_status : '0';
    $contact_address_status = isset($contact_address_status) ? $contact_address_status : '0';
    $contact_email_status = isset($contact_email_status) ? $contact_email_status : '0';
    $contact_telephone_status = isset($contact_telephone_status) ? $contact_telephone_status : '0';

?>

<form method="post" action="">
<table class="form-table">
    
    <tr>
        <th><label for=""><?php _e('Google Maps API Key','upas'); ?></label></th>
        <td style="width:500px;">
            <input type="text" name="upcm_contacts_general[google_maps_api_key]"  value="<?php echo $google_maps_api_key; ?>" /><br/>
        </td>
    </tr>

    <tr>
        <th><label for=""><?php _e('Display Contact Profile Tab','upas'); ?></label></th>
        <td style="width:500px;">
            <select id="upcm_contacts_profile_tab_status" name="upcm_contacts_general[contact_profile_tab_status]" class="chosen-admin_setting" >
                <option value="0" <?php selected('0',$contact_profile_tab_status); ?> ><?php _e('Disabled','upas'); ?></option>
                <option value="1" <?php selected('1',$contact_profile_tab_status); ?> ><?php _e('Enabled','upas'); ?></option>
            </select>
        </td>
    </tr>
    
    <?php 
        $contact_field_style = 'display:none';
        if($contact_profile_tab_status == '1'){
            $contact_field_style = '';
        }
    ?>
    <tr style="<?php echo $contact_field_style; ?>">
        <th><label for=""><?php _e('Display Contact Form','upas'); ?></label></th>
        <td style="width:500px;">
            <select id="upcm_contacts_form_status" name="upcm_contacts_general[contact_form_status]" class="chosen-admin_setting" >
                <option value="0" <?php selected('0',$contact_form_status); ?> ><?php _e('Disabled','upas'); ?></option>
                <option value="1" <?php selected('1',$contact_form_status); ?> ><?php _e('Enabled','upas'); ?></option>
            </select>
        </td>
    </tr>
    <tr style="<?php echo $contact_field_style; ?>">
        <th><label for=""><?php _e('Display Google Map Location','upas'); ?></label></th>
        <td style="width:500px;">
            <select id="upcm_contacts_google_map_status" name="upcm_contacts_general[contact_google_map_status]" class="chosen-admin_setting" >
                <option value="0" <?php selected('0',$contact_google_map_status); ?> ><?php _e('Disabled','upas'); ?></option>
                <option value="1" <?php selected('1',$contact_google_map_status); ?> ><?php _e('Enabled','upas'); ?></option>
            </select>
        </td>
    </tr>
    <tr style="<?php echo $contact_field_style; ?>">
        <th><label for=""><?php _e('Display Address','upas'); ?></label></th>
        <td style="width:500px;">
            <select id="upcm_contacts_address_status" name="upcm_contacts_general[contact_address_status]" class="chosen-admin_setting" >
                <option value="0" <?php selected('0',$contact_address_status); ?> ><?php _e('Disabled','upas'); ?></option>
                <option value="1" <?php selected('1',$contact_address_status); ?> ><?php _e('Enabled','upas'); ?></option>
            </select>
        </td>
    </tr>
    <tr style="<?php echo $contact_field_style; ?>">
        <th><label for=""><?php _e('Display Contact Email','upas'); ?></label></th>
        <td style="width:500px;">
            <select id="upcm_contacts_email_status" name="upcm_contacts_general[contact_email_status]" class="chosen-admin_setting" >
                <option value="0" <?php selected('0',$contact_email_status); ?> ><?php _e('Disabled','upas'); ?></option>
                <option value="1" <?php selected('1',$contact_email_status); ?> ><?php _e('Enabled','upas'); ?></option>
            </select>
        </td>
    </tr>
    <tr style="<?php echo $contact_field_style; ?>">
        <th><label for=""><?php _e('Display Contact Telephone','upas'); ?></label></th>
        <td style="width:500px;">
            <select id="upcm_contact_telephone_status" name="upcm_contacts_general[contact_telephone_status]" class="chosen-admin_setting" >
                <option value="0" <?php selected('0',$contact_telephone_status); ?> ><?php _e('Disabled','upas'); ?></option>
                <option value="1" <?php selected('1',$contact_telephone_status); ?> ><?php _e('Enabled','upas'); ?></option>
            </select>
        </td>
    </tr>
    
    
    <input type="hidden" name="upcm_tab" value="<?php echo $tab; ?>" />

    
</table>

    <?php submit_button(); ?>
</form>