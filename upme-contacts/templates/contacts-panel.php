<?php
    global $upcm_contacts_data,$upcm;
    extract($upcm_contacts_data);
   
?>

<div id="upcm-contacts-container">
    
    <?php if($settings_panel_status){ ?> 
    <div id="upcm-contact-controls">
        <div id="upcm-contact-info-button" class="upcm-contact-control-item upcm-contact-control-item-left"><?php _e('Contact Info','upcm'); ?></div>
        <div id="upcm-contact-settings-button" class="upcm-contact-control-item upcm-contact-control-item-right"><?php _e('Contact Settings','upcm'); ?></div>
    </div>
    <?php } ?>

    <div style="clear:both"></div>

    <?php if($settings_panel_status){ ?> 
    <div id="upcm-contact-settings-container" class="upcm-contact-tabs" style="display:none;">
        <div id="upcm-contact-settings-header" class="upcm-contact-form-header"><?php _e('My Settings','upcm'); ?>    
        </div>
        
        <div id="upcm-contact-settings-msg" style="display:none;"></div>
        
        <form id="upcm-contact-settings-form">
            <?php if($contact_google_map_status != '0'){ ?>
            <div class="upcm-contact-settings-label"><?php _e('Location','upcm'); ?></div>
            <div class="upcm-contact-settings-field"><input type="text" name="upcm_contact_settings_location" id="upcm_contact_settings_location" value="<?php echo $location; ?>" class="upcm_contact_settings_location" /></div>
            <?php } ?>

            <?php if($contact_email_status != '0'){ ?>
            <div class="upcm-contact-settings-label"><?php _e('Email','upcm'); ?></div>
            <div class="upcm-contact-settings-field"><input type="text" name="upcm_contact_settings_email" id="upcm_contact_settings_email" value="<?php echo $email; ?>" /></div>
            <?php } ?>

            <?php if($contact_telephone_status != '0'){ ?>
            <div class="upcm-contact-settings-label"><?php _e('Telephone','upcm'); ?></div>
            <div class="upcm-contact-settings-field"><input type="text" name="upcm_contact_settings_telephone" id="upcm_contact_settings_telephone" value="<?php echo $telephone; ?>" /></div>
            <?php } ?>

            <?php if($contact_address_status != '0'){ ?>
            <div class="upcm-contact-settings-label"><?php _e('Address','upcm'); ?></div>
            <div class="upcm-contact-settings-field"><textarea name="upcm_contact_settings_address" id="upcm_contact_settings_address" ><?php echo $address; ?></textarea></div>
            <?php } ?>

            <div class="upcm-contact-settings-label"><?php _e('Contact Form Status','upcm'); ?></div>
            <div class="upcm-contact-settings-field">
                <select name="upcm_contact_settings_form_status" id="upcm_contact_settings_form_status" >
                    <option value="1" <?php selected('1',$form_display_status); ?> ><?php _e('Display Contact Form for Public','upcm'); ?></option>
                    <option value="0" <?php selected('0',$form_display_status); ?> ><?php _e('Hide Contact Form for Public','upcm'); ?></option>
                </select>
            </div>
            
            <div class="upcm-contact-settings-label"><?php _e('Contact Tab Status','upcm'); ?></div>
            <div class="upcm-contact-settings-field">
                <select name="upcm_contact_settings_tab_status" id="upcm_contact_settings_tab_status" >
                    <option value="1" <?php selected('1',$tab_display_status); ?> ><?php _e('Display Contact Tab for Public','upcm'); ?></option>
                    <option value="0" <?php selected('0',$tab_display_status); ?> ><?php _e('Hide Contact Tab for Public','upcm'); ?></option>
                </select>
            </div>
            
            <div class="upcm-contact-settings-field"><input class="upme-button" type="button" name="upcm_contact_settings_form_btn" id="upcm_contact_settings_form_btn" value="<?php _e('Save Settings','upcm'); ?>" /><span class="upcm-loading"><?php _e('Loading...','upcm'); ?></span></div>
            
        </form>
    </div>
    <?php } ?> 
    
    <div id="upcm-contact-container" class="upcm-contact-tabs">
        <?php if($location != '' && $contact_google_map_status != '0' ){ ?>
            <div style="width:100%;height:300px" id="upcm_contact_map"></div>
        <?php } ?>
        
        <div id="upcm-contact-details">
            <?php if($email != '' && $contact_email_status != '0'){ ?>
                <div id="upcm-contact-email" class="upcm-contact-detail">
                    <p class="upcm-contact-details-title"><?php _e('Email','upcm'); ?></p>
                    <p class="upcm-contact-details-value"><?php echo $email; ?></p>
                </div>
            <?php } ?>
            <?php if($telephone != '' && $contact_telephone_status != '0'){ ?>
                <div id="upcm-contact-telephone" class="upcm-contact-detail">
                    <p class="upcm-contact-details-title"><?php _e('Telephone','upcm'); ?></p>
                    <p class="upcm-contact-details-value"><?php echo $telephone; ?></p>
                </div>
            <?php } ?>
            <?php if($address != '' && $contact_address_status != '0'){ ?>
                <div id="upcm-contact-address" class="upcm-contact-detail">
                    <p class="upcm-contact-details-title"><?php _e('Address','upcm'); ?></p>
                    <p class="upcm-contact-details-value"><?php echo $address; ?></p>
                </div>
            <?php } ?>
            
            <div style="clear:both"></div>
        </div>

        <?php if($form_display_status && $contact_form_status != '0' ){ ?> 
        <div id="upcm-contact-form-panel">
            <div id="upcm-contact-form-header" class="upcm-contact-form-header"><?php _e('Send Me a Message','upcm'); ?></div>
            <div id="upcm-contact-form-msg" style="display:none;"></div>
            <form id="upcm-contact-form">

                <div class="upcm-contact-form-label"><?php _e('Name','upcm'); ?><span>*</span></div>
                <div class="upcm-contact-form-field"><input type="text" name="upcm_contact_form_name" id="upcm_contact_form_name" value="" /></div>

                <div class="upcm-contact-form-label"><?php _e('Email','upcm'); ?><span>*</span></div>
                <div class="upcm-contact-form-field"><input type="text" name="upcm_contact_form_email" id="upcm_contact_form_email" value="" /></div>

                <div class="upcm-contact-form-label"><?php _e('Subject','upcm'); ?><span>*</span></div>
                <div class="upcm-contact-form-field"><input type="text" name="upcm_contact_form_subject" id="upcm_contact_form_subject" value="" /></div>

                <div class="upcm-contact-form-label"><?php _e('Message','upcm'); ?><span>*</span></div>
                <div class="upcm-contact-form-field"><textarea name="upcm_contact_form_message" id="upcm_contact_form_message" ></textarea></div>

                <input type="hidden" id="upcm_contact_user_id" value="<?php echo $profile_id; ?>" />

                <div class="upcm-contact-form-field"><input class="upme-button" type="button" name="upcm_contact_form_btn" id="upcm_contact_form_btn" value="<?php _e('Send','upcm'); ?>" /><span class="upcm-loading"><?php _e('Loading...','upcm'); ?></span></div>
            </form>
        </div>
        <?php } ?>
    </div>
    
</div>




    
