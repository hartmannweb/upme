<?php
/*
  Plugin Name: UPME Contacts
  Plugin URI: http://www.upmeaddons.innovativephp.com/upme-contacts/
  Description: Manage contact form and page for user profiles.
  Version: 1.5
  Author: Rakhitha Nimesh
  Author URI: http://upmeaddons.innovativephp.com
 */


// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

function upcm_get_plugin_version() {
    $default_headers = array('Version' => 'Version');
    $plugin_data = get_file_data(__FILE__, $default_headers, 'plugin');
    return $plugin_data['Version'];
}

/* Validating existence of required plugins */
add_action( 'plugins_loaded', 'upcm_plugin_init' );

function upcm_plugin_init(){
    
        UPME_Contacts();
   
}

if( !class_exists( 'UPME_Contacts' ) ) {
    
    class UPME_Contacts{
    
        private static $instance;
        public $upcm_options;

        public static function instance() {
            
            if ( ! isset( self::$instance ) && ! ( self::$instance instanceof UPME_Contacts ) ) {
                self::$instance = new UPME_Contacts();
                self::$instance->setup_constants();
                
                

                add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
                self::$instance->includes();
                
                add_action('admin_enqueue_scripts',array(self::$instance,'load_admin_scripts'),9);
                add_action('wp_enqueue_scripts',array(self::$instance,'load_scripts'),9);
                 
                self::$instance->template_loader        = new UPCM_Template_Loader();
                self::$instance->settings               = new UPCM_Settings();
                self::$instance->contact_info           = new UPCM_Contact_Info();
                
            }
            return self::$instance;
        }

        public function setup_constants() {
            $this->upcm_options = get_option('upcm_options');
        }
        
        public function load_scripts(){            
 
            $google_maps_api_key = isset($this->upcm_options['contacts_general']['google_maps_api_key']) ? $this->upcm_options['contacts_general']['google_maps_api_key'] : '';
            
            
            wp_register_style('upcm-front-style', UPCM_PLUGIN_URL . 'css/upcm-front.css');
            wp_enqueue_style('upcm-front-style');
            
            wp_register_script('upcm-front', UPCM_PLUGIN_URL . 'js/upcm-front.js', array('jquery'));
            wp_enqueue_script('upcm-front');
            
            wp_localize_script(
                'upcm-front',
                'upcmFront',
                array(
                    'AdminAjax' => admin_url( 'admin-ajax.php' ), // URL to WordPress ajax handling page
                    'nonce' => wp_create_nonce('upcm-contacts'),
                    'messages' => 
                        array(
                            'nameRequired' => __('Name is required.','upcm'),
                            'emailRequired' => __('Email is required.','upcm'),
                            'subjectRequired' => __('Subject is required.','upcm'),
                            'messageRequired' => __('Message is required.','upcm'),
                            'invalidEmail' => __('Email is invalid.','upcm'),
                        ),
                )
            );
            
            wp_register_script('upcm-gmaps-api', 'http://maps.googleapis.com/maps/api/js?key='.$google_maps_api_key.'&libraries=places');
            
            wp_register_script('upcm-gmaps', UPCM_PLUGIN_URL . 'js/upcm-gmaps.js', array('jquery'));
           
        }
        
        public function load_admin_scripts(){
            
            wp_register_style('upcm-admin-style', UPCM_PLUGIN_URL . 'css/upcm-admin.css');
            wp_enqueue_style('upcm-admin-style');
            
            wp_register_script('upcm-admin', UPCM_PLUGIN_URL . 'js/upcm-admin.js', array('jquery'));
            wp_enqueue_script('upcm-admin');
        }
        
        private function includes() {
            
            require_once UPCM_PLUGIN_DIR . 'classes/class-upcm-template-loader.php';
            require_once UPCM_PLUGIN_DIR . 'classes/class-upcm-settings.php';
            require_once UPCM_PLUGIN_DIR . 'classes/class-upcm-contact-info.php';

            if ( is_admin() ) {
            }
        }

        public function load_textdomain() {
            
        }
        
    }
}

// Plugin version
if ( ! defined( 'UPCM_VERSION' ) ) {
    define( 'UPCM_VERSION', '1.5' );
}

// Plugin Folder Path
if ( ! defined( 'UPCM_PLUGIN_DIR' ) ) {
    define( 'UPCM_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Plugin Folder URL
if ( ! defined( 'UPCM_PLUGIN_URL' ) ) {
    define( 'UPCM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

function UPME_Contacts() {
    global $upcm;
	$upcm = UPME_Contacts::instance();
}

