<?php
/*
  Plugin Name: UPME Zapier Integration Addon
  Plugin URI: http://profileplugin.com/upme-zapier-addon
  Description: UPME Zapier Integration Addon
  Version: 1.0
  Author: Rakhitha Nimesh
  Author URI: http://www.wpexpertdeveloper.com
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

register_activation_hook( __FILE__, 'upme_zia_install' );


function upme_zia_get_plugin_version() {
    $default_headers = array('Version' => 'Version');
    $plugin_data = get_file_data(__FILE__, $default_headers, 'plugin');
    return $plugin_data['Version'];
}

function upme_zia_install(){
    global $wpdb;
    
    $table_zapier_hooks = $wpdb->prefix . 'upme_zia_web_hooks';

    $sql_zapier_hooks = "CREATE TABLE IF NOT EXISTS $table_zapier_hooks (
              id int(11) NOT NULL AUTO_INCREMENT,
              hook_name longtext NOT NULL,
              zapier_url longtext NOT NULL,
              params longtext NOT NULL,
              status varchar(255) NOT NULL,
              PRIMARY KEY (id)
            );";


    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql_zapier_hooks );
}

/* Validating existence of required plugins */
add_action( 'plugins_loaded', 'upme_zia_plugin_init' );

function upme_zia_plugin_init(){
    if(!class_exists('UPME')){
        add_action( 'admin_notices', 'upme_zia_plugin_admin_notice' );
    }else{
        UPME_ZIA_Manager();
    }
}

function upme_zia_plugin_admin_notice() {
   $message = __('<strong>UPME Zapier Integration Addon</strong> requires <strong>User Profiles Made Easy</strong> plugin to function properly','zba');
   echo '<div class="error"><p>'.$message.'</p></div>';
}

if( !class_exists( 'UPME_ZIA_Manager' ) ) {
    
    class UPME_ZIA_Manager{
    
        private static $instance;

        public static function instance() {
            
            if ( ! isset( self::$instance ) && ! ( self::$instance instanceof UPME_ZIA_Manager ) ) {
                self::$instance = new UPME_ZIA_Manager();
                self::$instance->setup_constants();

                add_action('init', array( self::$instance, 'init' ) );

                add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
                self::$instance->includes();
                
                add_action('admin_enqueue_scripts',array(self::$instance,'load_admin_scripts'),9);
                add_action('wp_enqueue_scripts',array(self::$instance,'load_scripts'),9);
                
                self::$instance->settings                   = new UPME_ZIA_Settings();
                self::$instance->template_loader            = new UPME_ZIA_Template_Loader();
                self::$instance->web_hook_manager           = new UPME_ZIA_Web_Hooks_Manager();
                self::$instance->upme                       = new UPME_ZIA_UPME();
            }
            return self::$instance;
        }

        public function init(){
            self::$instance->options = get_option('upme_zia_options');
        }

        public function setup_constants() {
            
        }
        
        public function load_scripts(){    
         
        }

        public function load_customizer_styles(){

        }
        
        public function load_admin_scripts(){
            wp_register_style('upme-zia-admin-css', UPME_ZIA_PLUGIN_URL . 'css/upme-zia-admin.css');
            wp_enqueue_style('upme-zia-admin-css');
        }
        
        private function includes() {

            require_once UPME_ZIA_PLUGIN_DIR . 'classes/class-upme-zia-settings.php'; 
            require_once UPME_ZIA_PLUGIN_DIR . 'classes/class-upme-zia-template-loader.php'; 
            require_once UPME_ZIA_PLUGIN_DIR . 'classes/class-upme-zia-web-hooks-manager.php';
            require_once UPME_ZIA_PLUGIN_DIR . 'classes/class-upme-zia-web-hooks-list-table.php';
            require_once UPME_ZIA_PLUGIN_DIR . 'classes/class-upme-zia-upme.php';
                        
            if ( is_admin() ) {
            }
        }

        public function load_textdomain() {
            
        }
        
    }
}

// Plugin version
if ( ! defined( 'UPME_ZIA_VERSION' ) ) {
    define( 'UPME_ZIA_VERSION', '1.0' );
}

// Plugin Folder Path
if ( ! defined( 'UPME_ZIA_PLUGIN_DIR' ) ) {
    define( 'UPME_ZIA_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Plugin Folder URL
if ( ! defined( 'UPME_ZIA_PLUGIN_URL' ) ) {
    define( 'UPME_ZIA_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

require_once UPME_ZIA_PLUGIN_DIR . 'functions.php';

function UPME_ZIA_Manager() {
    global $upme_zia;
    $upme_zia = UPME_ZIA_Manager::instance();
}



