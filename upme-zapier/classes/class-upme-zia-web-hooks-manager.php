<?php

class UPME_ZIA_Web_Hooks_Manager{
    
    public function __construct(){   
    	add_action('init', array($this, 'init'));
    	add_action('init', array($this, 'save_web_hooks'));
    }

    public function init(){
    	global $wpdb;
        $this->web_hooks_table = $wpdb->prefix . 'upme_zia_web_hooks';
    }

    public function save_web_hooks($mode){
    	global $wpdb,$upme_zia_hook_temp_method_data;

    	$mode = 'add';
        $edit_web_hook_id = 0;
        if(isset($_GET['page']) && $_GET['page'] == 'upme-zia-web-hook-settings' && isset($_GET['action']) && $_GET['action'] == 'edit'){
            $mode = 'edit';
            $web_hook_id = isset($_GET['web_hook_id']) ? (int) $_GET['web_hook_id'] : 0;


            $sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE id = %d ", $web_hook_id );
            $result = $wpdb->get_results($sql_total);
            
            $upme_zia_web_hook_data['upme_zia_edit_web_hook_id'] = 0;
            if($result){
                $upme_zia_web_hook_data['upme_zia_edit_web_hook_id'] 		= $web_hook_id;
                $upme_zia_hook_temp_method_data['hook_name'] 		= $result[0]->hook_name;
	            $upme_zia_hook_temp_method_data['zapier_url'] 		= $result[0]->zapier_url;
	            $upme_zia_hook_temp_method_data['status'] 	        = $result[0]->status;
	    		
            }
        }

        if(isset($_POST['upme_zia_web_hook_add_submit']) ||
            isset($_POST['upme_zia_web_hook_edit_submit'])){

            $upme_zia_web_hook_data['hook_name'] 	= isset($_POST['upme_zia_action']) ? sanitize_text_field($_POST['upme_zia_action']) : '';
            $upme_zia_web_hook_data['zapier_url'] 	= isset($_POST['upme_zia_web_hook_url']) ? esc_url($_POST['upme_zia_web_hook_url']) : '';
            $upme_zia_web_hook_data['status'] 		= isset($_POST['upme_zia_action_status']) ? sanitize_text_field($_POST['upme_zia_action_status']) : '';
    		
    
            $message = '';
            if($message != ''){
                $msg_class = ' error ';
            }else{
                if(isset($_POST['upme_zia_web_hook_add_submit'])){
                    if($this->add_web_hook($upme_zia_web_hook_data)){
                        $this->message = __('UPME-Zapier web hook saved successfully.','upme_zia');
                        $this->msg_class = ' upated ';
                        wp_redirect(admin_url( 'admin.php?page=upme-zia-web-hook-settings&action=upme_zia_view_list' ));
                        exit;
                    }else{
                        $this->message = __('UPME-Zapier web hook save failed.','upme_zia');
                        $this->msg_class = ' error ';
                    }
                }

                if(isset($_POST['upme_zia_web_hook_edit_submit'])){
                    if($this->edit_web_hook($upme_zia_web_hook_data)){
                        $this->message = __('UPME-Zapier web hook updated successfully.','upme_zia');
                        $this->msg_class = ' updated ';
                        wp_redirect(admin_url( 'admin.php?page=upme-zia-web-hook-settings&action=upme_zia_view_list' ));
                    }else{
                        $this->message = __('UPME-Zapier web hook update failed.','upme_zia');
                        $this->msg_class = ' error ';
                    }
                }
            }
        }
    }

    public function add_web_hook($upme_zia_web_hook_data){
        global $wpdb;
        extract($upme_zia_web_hook_data);


        $status = $wpdb->insert( 
                    $this->web_hooks_table, 
                    array( 
                        'hook_name'  			=> $hook_name, 
                        'zapier_url' 			=> $zapier_url,
                        'status' 				=> $status
                    ), 
                    array( 
                        '%s', 
                        '%s',
                        '%s'
                    ) ); 

        return $status;
    }

    public function edit_web_hook($upme_zia_web_hook_data){
        global $wpdb;
        extract($upme_zia_web_hook_data);

        $status = $wpdb->update( 
                            $this->web_hooks_table, 
                            array( 
                                'hook_name'  			=> $hook_name, 
		                        'zapier_url' 			=> $zapier_url,
		                        'status' 				=> $status
                            ), 
                            array( 'id' => $upme_zia_edit_web_hook_id ), 
                            array( 
                                '%s', 
		                        '%s',
		                        '%s' 
                            ), 
                            array( '%d' ) 
                        );

        return $status;
    }
}





