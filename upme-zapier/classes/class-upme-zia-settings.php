<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/* Manage settings of WP Private Content Plus plugin */
class UPME_ZIA_Settings{
    
    public $template_locations;
    public $current_user;
    
    /* Intialize actions for plugin settings */
    public function __construct(){
        
        add_action('init', array($this, 'init'));
        add_action('admin_menu', array(&$this, 'admin_settings_menu'), 9);
        add_action('init', array($this,'save_settings_page') );
        
    }

    public function init(){
        $this->current_user = get_current_user_id(); 
    }
    
    /*  Save settings tabs */
    public function save_settings_page(){

        if(!is_admin())
            return;
        
        $upme_zia_settings_pages = array('upme-zia-web-hook-settings');
        if(isset($_POST['upme_zia_tab']) && isset($_GET['page']) && in_array($_GET['page'],$upme_zia_settings_pages)){
            $tab = '';
            if ( isset ( $_POST['upme_zia_tab'] ) )
               $tab = $_POST['upme_zia_tab']; 

            if($tab != ''){
                $func = 'save_'.$tab;
                
                if(method_exists($this,$func))
                    $this->$func();
            }
        }
    }  
    
    
    /* Intialize settings page and tabs */
    public function admin_settings_menu(){
        // add_menu_page(__('UPFP Frontend Publisher', "upfp"), __("UPFP Frontend Publisher", "upfp"),'manage_options','upfp-posts-manager',array(&$this,'posts_settings'));
       // add_submenu_page('upme-settings', __("Add-ons", "upme"), __("Add-ons", "upme"),'manage_options','upme-add-ons',array(&$this,'upme_addons_page'));
       // add_menu_page(__('UPFP Frontend Publisher', "upfp"), __("UPFP Frontend Publisher", "upfp"),'manage_options','upfp-posts-manager',array(&$this,'posts_settings'));
       add_submenu_page('upme-settings', __("UPME-Zapier Web Hooks'", "upme"), __("UPME-Zapier Web Hooks'", "upme"),'manage_options','upme-zia-web-hook-settings',array(&$this,'web_hook_settings'));
       // add_menu_page(__('UPME-Zapier Web Hooks', 'upme_zia' ), __('UPME-Zapier Web Hooks', 'upme_zia' ),'manage_options','upme-zia-web-hook-settings',array(&$this,'web_hook_settings'));
    }  
    
    /* Display settings */

    public function web_hook_settings(){
        global $upme_zia,$upme_zia_settings_data;
        
        add_settings_section( 'upme_zia_web_hooks_section_general', __('Web Hooks','upme_zia'), array( &$this, 'upme_zia_web_hooks_section_general_desc' ), 'upme-zia-web-hooks-general' );
        
        
        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'upme_zia_web_hooks_section_general';
        $upme_zia_settings_data['tab'] = $tab;
        
        $tabs = $this->plugin_options_tabs('web_hooks_general',$tab);
   
        $upme_zia_settings_data['tabs'] = $tabs;
        
        $tab_content = $this->plugin_options_tab_content($tab);
        $upme_zia_settings_data['tab_content'] = $tab_content;
        
        ob_start();
        $upme_zia->template_loader->get_template_part( 'menu-page-container');
        $display = ob_get_clean();
        echo $display;
        
    
    }
    
    /* Manage settings tabs for the plugin */
    public function plugin_options_tabs($type,$tab) {
        $current_tab = $tab;
        $this->plugin_settings_tabs = array();
        
        switch($type){

  
            case 'web_hooks_general':
                $this->plugin_settings_tabs['upme_zia_web_hooks_section_general']  = __('Web Hooks','upme_zia');
                break;

        }
        
        ob_start();
        ?>

        <h2 class="nav-tab-wrapper">
        <?php 
            foreach ( $this->plugin_settings_tabs as $tab_key => $tab_caption ) {
            $active = $current_tab == $tab_key ? 'nav-tab-active' : '';
            $page = isset($_GET['page']) ? $_GET['page'] : '';
        ?>
                <a class="nav-tab <?php echo $active; ?> " href="?page=<?php echo $page; ?>&tab=<?php echo $tab_key; ?>"><?php echo $tab_caption; ?></a>
            
        <?php } ?>
        </h2>

        <?php
                
        return ob_get_clean();
    }
    
    /* Manage settings tab contents for the plugin */
    public function plugin_options_tab_content($tab,$params = array()){
        global $upme_zia,$upme_zia_mailchimp_settings_data;

        $upme_zia_options = get_option('upme_zia_options');

        $this->load_upme_zia_select2_scripts_style();
        
        ob_start();
        switch($tab){
            

            case 'upme_zia_web_hooks_section_general':                
                $data = isset($upme_zia_options['web_hooks_general']) ? $upme_zia_options['web_hooks_general'] : array();
      
                $upme_zia_mailchimp_settings_data['tab'] = $tab;
                
                $upme_zia->template_loader->get_template_part('web-hooks-settings');            
                break;         
            
        }
        
        $display = ob_get_clean();
        return $display;
        
    }

    
    /* Display settings saved message */  
    public function admin_notices(){
        ?>
        <div class="updated">
          <p><?php esc_html_e( 'Settings saved successfully.', 'upme_zia' ); ?></p>
       </div>
        <?php
    }

    public function load_upme_zia_select2_scripts_style(){          

        wp_register_script('upme_zia_select2_js', UPME_ZIA_PLUGIN_URL . 'js/select2/upme-zia-select2.min.js');
        wp_enqueue_script('upme_zia_select2_js');
        
        wp_register_style('upme_zia_select2_css', UPME_ZIA_PLUGIN_URL . 'js/select2/upme-zia-select2.min.css');
        wp_enqueue_style('upme_zia_select2_css');

    }

}
