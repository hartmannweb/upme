<?php

class UPME_ZIA_UPME{

	public function __construct(){
		
		// add_action('init', array($this, 'init'));
		$this->init();	
	}

	public function init(){
		global $wpdb;

		$this->web_hooks_table = $wpdb->prefix . 'upme_zia_web_hooks';
		$sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE hook_name != '%s'
			and status = '%s' group by hook_name ", '', 'enabled' );
        $result = $wpdb->get_results($sql_total);

        if($result){ 
	        foreach ($result as $key => $web_hook) { 
	          	switch ($web_hook->hook_name) {
	        		case 'upme_user_register':
	        			add_action('upme_user_register', array($this , 'upme_zapier_user_register'));
	        			break;
	        		
	        		case 'upme_profile_update':
	        			add_action('upme_profile_field_update_triggered', array($this , 'upme_zapier_profile_field_update_triggered'));
	        			break;

	        		case 'upme_approve_profile':
	        			add_action('upme_approve_user_profile', array($this , 'upme_zapier_approve_user_profile'),10,2);
	        			break;

	        		case 'upme_disapprove_profile':
	        			add_action('upme_disapprove_user_profile', array($this , 'upme_zapier_disapprove_user_profile'),10,2);
	        			break;

	        		case 'upme_activate_profile':
	        			add_action('upme_activate_user_profile', array($this , 'upme_zapier_activate_user_profile'),10,2);
	        			break;

	        		case 'upme_deactivate_profile':
	        			add_action('upme_deactivate_user_profile', array($this , 'upme_zapier_deactivate_user_profile'),10,2);
	        			break;

	        	}

	        }
	    }
	}

	public function upme_zapier_user_register($user_id){
		global $wpdb,$upme_options;

	    $data['user_id'] 	= $user_id;
	    $data['email'] 		= get_user_meta($user_id,'user_email',true);

	    $upme_profile_fields = $upme_options->upme_profile_fields;
	    foreach ($upme_profile_fields as $key => $field) {           
            if( isset($upme_profile_fields[$key]['show_in_register']) && $upme_profile_fields[$key]['show_in_register'] == 1  ){
                $data[$field['meta']] 		= get_user_meta($user_id,$field['meta'],true);
            }
        }

		$sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE hook_name = '%s'
			and status = '%s' ", 'upme_user_register', 'enabled' );
        $result = $wpdb->get_results($sql_total);

        $this->post_to_zapier($result,$data);

	}

	public function post_to_zapier($result,$data){

		$content_type = 'application/json';

	    $blog_charset = get_option( 'blog_charset' );
	    if ( ! empty( $blog_charset ) ) {
	        $content_type .= '; charset=' . get_option( 'blog_charset' );
	    }

		$args = array(
	        'method'    => 'POST',
	        'body'      => json_encode( $data ),
	        'headers'   => array(
	            'Content-Type'  => $content_type,
	        ),
	    );

		if($result){ 
	        foreach ($result as $key => $web_hook) { 
	        	// echo "<pre>";print_r($args);exit;
	        	wp_remote_post( $web_hook->zapier_url , $args  );
	        }
	    }
	}

	public function upme_zapier_profile_field_update_triggered($params){
		global $wpdb,$upme_options;
		extract($params);

		$data['user_id'] 	= $user_id;
	    $data['email'] 		= get_user_meta($user_id,'user_email',true);
	    $sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE hook_name = '%s'
			and status = '%s' ", 'upme_profile_field_update_triggered', 'enabled' );
        $result = $wpdb->get_results($sql_total);

        $this->post_to_zapier($result,$params);
	}

	public function upme_zapier_approve_user_profile($user_id,$params){
		global $wpdb,$upme_options;
		$data['user_id'] 	= $user_id;
	    $data['email'] 		= get_user_meta($user_id,'user_email',true);
	    $sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE hook_name = '%s'
			and status = '%s' ", 'upme_approve_profile', 'enabled' );
        $result = $wpdb->get_results($sql_total);

        $this->post_to_zapier($result,$data);
	}

	public function upme_zapier_disapprove_user_profile($user_id,$params){
		global $wpdb,$upme_options;
		$data['user_id'] 	= $user_id;
	    $data['email'] 		= get_user_meta($user_id,'user_email',true);
	    $sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE hook_name = '%s'
			and status = '%s' ", 'upme_disapprove_profile', 'enabled' );
        $result = $wpdb->get_results($sql_total);

        $this->post_to_zapier($result,$data);
	}

	public function upme_zapier_activate_user_profile($user_id,$params){
		global $wpdb,$upme_options;
		$data['user_id'] 	= $user_id;
	    $data['email'] 		= get_user_meta($user_id,'user_email',true);
	    $sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE hook_name = '%s'
			and status = '%s' ", 'upme_activate_profile', 'enabled' );
        $result = $wpdb->get_results($sql_total);

        $this->post_to_zapier($result,$data);
	}

	public function upme_zapier_deactivate_user_profile($user_id,$params){
		global $wpdb,$upme_options;
		$data['user_id'] 	= $user_id;
	    $data['email'] 		= get_user_meta($user_id,'user_email',true);
	    $sql_total  = $wpdb->prepare( "SELECT * FROM $this->web_hooks_table WHERE hook_name = '%s'
			and status = '%s' ", 'upme_deactivate_profile', 'enabled' );
        $result = $wpdb->get_results($sql_total);

        $this->post_to_zapier($result,$data);
	}
}