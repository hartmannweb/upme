<?php 
    global $upme_zia,$upme_zia_settings_data,$upme_zia_hook_temp_method_data; 
    extract($upme_zia_settings_data);

    $upme_zia_web_hook_edit_id = isset($_GET['web_hook_id']) ? (int) $_GET['web_hook_id'] : 0;
    

    $mode = 'add';
    if($_GET['page'] == 'upme-zia-web-hook-settings' && isset($_GET['action']) && $_GET['action'] == 'edit'){
        $mode = 'edit';
    }

?>



<?php if(isset($upme_zia->web_hook_manager->message) && $upme_zia->web_hook_manager->message != '') { ?>
  <div id="setting-error-settings_updated" class="<?php echo $upme_zia->web_hook_manager->msg_class; ?> settings-error notice is-dismissible"> 
      <p><strong><?php echo $upme_zia->web_hook_manager->message; ?></strong></p>
  </div>
<?php } ?>

<br/>

<?php if(!isset($_GET['action']) || (isset($_GET['action']) && $_GET['action'] == 'edit') ) { ?>
<div id="upme-zapier-hooks-panel">
  <a href="<?php echo admin_url( 'admin.php?page=upme-zia-web-hook-settings&action=upme_zia_view_list' ); ?>" class='button button-primary'><?php _e('View UPME-Zapier Web Hooks List','upme_zia'); ?></a>

<form action="" method="POST"  >
    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><label ><?php echo __('UPME-Zapier Action','upme_zia'); ?></label></th>
                <td>
                	<select name="upme_zia_action" id="upme_zia_action" class="upme_zia_filter_select" >
               			<option <?php echo selected('upme_user_register',$upme_zia_hook_temp_method_data['hook_name'] ); ?> value="upme_user_register" ><?php echo __('UPME User Registration','upme_zia'); ?></option>
               		  <option <?php echo selected('upme_profile_update',$upme_zia_hook_temp_method_data['hook_name'] ); ?> value="upme_profile_update" ><?php echo __('UPME Profile Update','upme_zia'); ?></option>
                    <option <?php echo selected('upme_approve_profile',$upme_zia_hook_temp_method_data['hook_name'] ); ?> value="upme_approve_profile" ><?php echo __('UPME Approve Profile','upme_zia'); ?></option>
                    <option <?php echo selected('upme_disapprove_profile',$upme_zia_hook_temp_method_data['hook_name'] ); ?> value="upme_disapprove_profile" ><?php echo __('UPME Disapprove Profile','upme_zia'); ?></option>
                    <option <?php echo selected('upme_activate_profile',$upme_zia_hook_temp_method_data['hook_name'] ); ?> value="upme_activate_profile" ><?php echo __('UPME Activate Profile','upme_zia'); ?></option>
                    <option <?php echo selected('upme_deactivate_profile',$upme_zia_hook_temp_method_data['hook_name'] ); ?> value="upme_deactivate_profile" ><?php echo __('UPME Deactivate Profile','upme_zia'); ?></option>

                  
                  </select>
                </td>
            </tr>
            <tr>
                <th scope="row"><label ><?php echo __('UPME-Zapier Action Status','upme_zia'); ?></label></th>
                <td>
                  <select name="upme_zia_action_status" id="upme_zia_action_status" class="upme_zia_filter_select" >
                    <option <?php echo selected('enabled',$upme_zia_hook_temp_method_data['status'] ); ?> value="enabled" ><?php echo __('Enabled','upme_zia'); ?></option>
                    <option <?php echo selected('disabled',$upme_zia_hook_temp_method_data['status'] ); ?> value="disabled" ><?php echo __('Disabled','upme_zia'); ?></option>
                  </select>
                </td>
            </tr>
            <tr>
                <th scope="row"><label ><?php echo __('UPME-Zapier Zapier Web Hook URL','upme_zia'); ?></label></th>
                <td>
                  <input type="text" value="<?php echo $upme_zia_hook_temp_method_data['zapier_url']; ?>" name="upme_zia_web_hook_url" id="upme_zia_web_hook_url" class="" />
                    
                </td>
            </tr>            
            
            <tr>
                <th scope="row"><label >&nbsp;</label></th>
                <?php if($mode == 'add'){ ?>
                    <td><input name="upme_zia_web_hook_add_submit" type="submit" id="upme_zia_web_hook_add_submit" value="<?php echo __('Add','upme_zia'); ?>" class="button button-primary"></td>
            
                <?php }else { ?>
                    <td><input name="upme_zia_web_hook_edit_id" type="hidden" id="upme_zia_web_hook_edit_id" value="<?php echo $upme_zia_web_hook_edit_id; ?>" class="regular-text">
                        <input name="upme_zia_web_hook_edit_submit" type="submit" id="upme_zia_web_hook_edit_submit" value="<?php echo __('Update','upme_zia'); ?>" class="button button-primary"></td>
            
                <?php } ?>
            </tr>
        </tbody>
</form>
</div>

<?php }else if(isset($_GET) && $_GET['page'] == 'upme-zia-web-hook-settings' && $_GET['action'] == 'upme_zia_view_list' ){ ?> 
        <a href="<?php echo admin_url( 'admin.php?page=upme-zia-web-hook-settings' ); ?>" class='button button-primary'><?php _e('Add UPME-Zapier Web Hook','upme_zia'); ?></a>

      <?php upme_zia_web_hooks_list_page();
      } 
?>